/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/* bodyBackground,footerBackground,footerHeadingColor,footerTextcolor,fontFamily 
 headerCol, foregroundCol, headingCol, DescriptionCol, fieldDescriptionCol, borderCol,
 selectCol, fieldBackgroundCol, buttonCol, buttonHoverCol, textColor*/
//companyName companyDescription facebookPage twitterPage googalPlusPage address yourMail contactNumber yourSite headerLogoImage selectBorderDarkColor fieldBorderDarkColor headreImage
//                                                1                           ,   2      ,    3     ,     4    , 5 ,     6    ,    7      ,    8      ,    9   ,    10    ,    11    ,    12    ,    13    ,    14    ,    15    ,    16    , 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28
var defaults = new Array(base_url + "app_data/view/images/temp/template-1.jpg", "#ececec", "#424242", "#6a6a6a", "", "#353d4e", "#ffffff", "#010101", "#908d8d", "#000000", "#e1e1e1", "#f1f1f1", "#ffffff", "#dd4b39", "#fe6a48", "#ffffff", "", "", "", "", "", "", "", "", "", "", "", "");
var colorOptionDefault = new Array("#353d4e", "#ffffff", "#010101", "#908d8d", "#e1e1e1", "#f1f1f1", "#ffffff", "#000000", "#dd4b39", "#fe6a48", "#ffffff", "#424242", "#6a6a6a", "#ececec");
var backGroundImage = base_url + "app_data/view/images/temp/template-1.jpg", backGroundImageName = '';
var clickFlage = false;
$(function() {
    $("#footer_option_content").find("input[type='text']").keyup(hideAll);
    if (typeof c_str != 'undefined')
    {
        defaults = c_str;
        if ((defaults[25] == base_url + "app_data/view/images/form-header-logo.png") || defaults[25] == "")
        {
            $("#headerLogoLink").css({display: "block"});
        } else
        {
            $("#headerLogoLink").css({display: "none"});
        }
        colorOptionDefault = p_str;
        madeTempletChange(defaults[0], defaults[1], defaults[2], defaults[3], defaults[4]);
        changeTemplet(defaults[5], defaults[6], defaults[7], defaults[8], defaults[9], defaults[10], defaults[11], defaults[12], defaults[13], defaults[14], defaults[15]);
        var dtemp = 18;
        for (var i = 0; i < 3; i++)
        {
            if (defaults[dtemp] != "")
            {
                if (!linkContain(defaults[dtemp]))
                {
                    defaults[dtemp] = "https://" + defaults[dtemp];
                }
            }
            dtemp++;
        }
        if (defaults[24] != "")
        {
            if (!linkContain(defaults[24]))
            {
                defaults[24] = "https://" + defaults[24];
                $("#yourSite").attr('href', defaults[24]);
            } else
            {
                $("#yourSite").attr('href', defaults[24]);
            }
        }

        dtemp = 16;
        var textChange = new Array("#footerHeading", "#footerDescription", "#footerFacebookPage", "#footertWitterPage", "#footerGoogleplusLink", "#footerAddress", "#footerYouremail", "#footerContactNumber", "#footerYoursite");
        var iconArray = new Array("#companyName", "#footerSubHeading", "#facebookLi", "#twitterLi", "#googleLi", "#locationIcon", "#mailIcon", "#numberIcon", "#siteIcon");
        var elemArray = new Array("#companyName", "#footerSubHeading", "#formSocialFacebook", "#formSocialTwitter", "#formSocialGoogle", "#footerLocation", "#contactMail", "#contactNumber", "#yourSite");
        var iCount = 0;
        var globalHideFlage = true, globalcFlage = true, globalaFlage = true;
        $(textChange).each(function(i, item) {
            $(item).val(defaults[dtemp]);
            if (i < 9)
            {
                if (defaults[dtemp] == "")
                {
                    $(iconArray[iCount]).css({display: "none"});
                } else {
                    globalHideFlage = false;
                    $(iconArray[iCount]).css({display: "inline-block"});
                }
                iCount++;
            }
            if (i < 5)
            {
                if (defaults[dtemp].trim() != "")
                {
                    globalcFlage = false;
                }
            } else
            {
                if (defaults[dtemp].trim() != "")
                {
                    globalaFlage = false;
                }
            }
            if (i > 1 && i < 5)
            {
                $(elemArray[i]).attr("href", defaults[dtemp++]);
            } else {
                $(elemArray[i]).html(defaults[dtemp++]);
            }
        });
        if (globalaFlage || globalcFlage)
        {
            $('.dotted-border').css({display: "none"});
        }
        if (globalHideFlage)
        {
            $("#formFooter").css({display: "none"});
            $("#footer_option_content").find("input[type='text']").keydown(function() {
                $("#formFooter").css({display: "block"});
                $("#footer_option_content").find("input[type='text']").unbind("keydown");
            });
        } else
        {
            $("#formFooter").css({display: "block"});
        }
    }
    opt_show('header_option_content', 'arrow1', 'option_nav1');
    $(".fg-outlined").click(function() {
        $(".current").removeClass('current');
        $(this).addClass('current');
    });
    $("#templateChooser span").click(function() {
        var template = $(this).attr('data-color')
        switch (template)
        {
            case"tplBlack":
                {
                    changeTemplet("#fb5927", "#000000", "#ffffff", "#ffffff", "#ffffff", "#5f5f5f", "#828282", "#969696", "#fb5927", "#f67952", "#ffffff");
                    break;
                }
            case"tplDarkCyan":
                {
                    changeTemplet("#2bc1f2", "#ffffff", "#000000", "#000000", "#000000", "#98d0f1", "#93dff8", "#ffffff", "#2bc1f2", "#42d0ff", "#ffffff");
                    break;
                }
            case"tplBrown":
                {
                    changeTemplet("#ea9111", "#fcfff3", "#000000", "#000000", "#000000", "#fcc897", "#f8e094", "#ffffff", "#ea9111", "#ffb54b", "#ffffff");
                    break;
                }
            case"tplGreen":
                {
                    changeTemplet("#afc118", "#ffffff", "#000000", "#000000", "#000000", "#92ce90", "#99e3e2", "#ffffff", "#a9bb12", "#cae012", "#ffffff");
                    break;
                }
            case"tplLightGreen":
                {
                    changeTemplet("#3b6e9a", "#fcf7e1", "#000000", "#000000", "#000000", "#bbc6a9", "#c3ddee", "#ffffff", "#3b6e9a", "#3a6e9a", "#ffffff");
                    break;
                }
            case"tplMidiumSpringGreen":
                {
                    changeTemplet("#1376af", "#edfdf6", "#000000", "#000000", "#000000", "#92dcbd", "#87f0b5", "#ffffff", "#1376af", "#1c96dc", "#ffffff");
                    break;
                }
            case"tplPaleGreen":
                {
                    changeTemplet("#87c576", "#faf3ed", "#000000", "#000000", "#000000", "#bfdcb0", "#f4e0a9", "#ffffff", "#6baf59", "#71d956", "#ffffff");
                    break;
                }
            case"tplTomato":
                {
                    changeTemplet("#ea4f44", "#ffffff", "#000000", "#000000", "#000000", "#ff9b94", "#56c3f0", "#ffffff", "#ea4f44", "#ff2313", "#ffffff");
                    break;
                }
            case"tplPurple":
                {
                    changeTemplet("#9d3e87", "#ffffff", "#000000", "#000000", "#000000", "#f5c0e9", "#cecbf5", "#fcf4fa", "#9d3e87", "#e142bc", "#ffffff");
                    break;
                }
            case"tplYellow":
                {
                    changeTemplet("#fb5927", "#fbefbf", "#000000", "#000000", "#000000", "#ecd98e", "#fdba90", "#ffffff", "#fb5927", "#ff7c53", "#ffffff");
                    break;
                }
        }
    });
    $('.color-chooser').each(function(i, item) {
        $(item).find('span:first').css({"background-color": colorOptionDefault[i]});
        $(item).ColorPicker({
            color: colorOptionDefault[i],
            onShow: function(colpkr) {
                $(colpkr).fadeIn(500);
                return false;
            },
            onHide: function(colpkr) {
                $(colpkr).fadeOut(500);
                return false;
            },
            onChange: function(hsb, hex, rgb, t) {
                var id = $(this).attr('data-ref');
                $(this).data('ref').find('.color-chooser-bg').css({"background-color": "#" + hex});
                if (id)
                {
                    changeColor(id, hex);
                }
            }
        });
    })

    $('.background-image-chooser').mouseleave(function() {
        madeTempletChange(backGroundImage, defaults[1], defaults[2], defaults[3], defaults[4]);
        changeTemplet(defaults[5], defaults[6], defaults[7], defaults[8], defaults[9], defaults[10], defaults[11], defaults[12], defaults[13], defaults[14], defaults[15]);
    }).find('img').mouseover(function() {
        var imgSrc = $(this).attr('data-src');
        var template = $(this).attr('data-template');
        selectTemplet(imgSrc, template, false);
    }).click(function() {
        var imgSrc = $(this).attr('data-src');
        var template = $(this).attr('data-template');
        selectTemplet(imgSrc, template, true);
        $('.right_check').css({display: "none"});
        $(this).parent().find('.right_check').css({display: "block"});
    });
    $('#footerHeading').keyup(function() {
        $("#companyName").css({display: "block"});
        $('#companyName').html($(this).val());
        defaults[16] = $(this).val();
    });
    $('#footerDescription').keyup(function() {
        $("#footerSubHeading").css({display: "block"});
        $('#footerSubHeading').html($(this).val());
        defaults[17] = $(this).val();
    });

    $('#footerFacebookPage').keyup(function() {
        var linkVal = $(this).val();
        if (linkVal != "")
        {
            $("#facebookLi").css({display: "inline-block"});
        } else
        {
            $("#facebookLi").css({display: "none"});
        }
        $('#formSocialFacebook').attr('href', linkVal);
        defaults[18] = linkVal;
    });
    $('#footertWitterPage').keyup(function() {
        var linkVal = $(this).val();
        if (linkVal != "")
        {
            $("#twitterLi").css({display: "inline-block"});
        }
        else
        {
            $("#twitterLi").css({display: "none"});
        }
        $('#formSocialTwitter').attr('href', linkVal);
        defaults[19] = linkVal;
    });
    $('#footerGoogleplusLink').keyup(function() {
        var linkVal = $(this).val();
        $('#formSocialGoogle').attr('href', linkVal);
        if (linkVal != "")
        {
            $("#googleLi").css({display: "inline-block"});
        } else
        {
            $("#googleLi").css({display: "none"});
        }
        defaults[20] = linkVal;
    });

    $('#footerAddress').keyup(function() {
        var linkVal = $(this).val();
        if (linkVal != "")
        {
            $('#footerLocation').html(linkVal);
            $("#locationIcon").css({display: "inline-block"});
        } else
        {
            $("#locationIcon").css({display: "none"});
        }
        defaults[21] = linkVal;
    });
    $('#footerYouremail').keyup(function() {
        var linkVal = $(this).val();
        if (linkVal != "")
        {
            $('#contactMail').html(linkVal);
            $("#mailIcon").css({display: "inline-block"});
        } else
        {
            $('#contactMail').html("");
            $("#mailIcon").css({display: "none"});
        }
        defaults[22] = linkVal;
    });
    $('#footerContactNumber').keyup(function() {
        var linkVal = $(this).val();
        if (linkVal != "")
        {
            $('#contactNumber').html(linkVal);
            $("#numberIcon").css({display: "inline-block"});
        } else
        {
            $("#numberIcon").css({display: "none"});
        }
        defaults[23] = linkVal;
    });
    $('#footerYoursite').keyup(function() {
        var linkVal = $(this).val();
        $('#yourSite').html(linkVal).attr('href', "https://" + linkVal);
        if (linkVal != "")
        {
            $('#yourSite').html(linkVal);
            $("#siteIcon").css({display: "inline-block"});
        } else
        {
            $("#siteIcon").css({display: "none"});
        }
        defaults[24] = linkVal;
    });
});
function changeTemplet(headerCol, foregroundCol, headingCol, DescriptionCol, fieldDescriptionCol, borderCol, selectCol, fieldBackgroundCol, buttonCol, buttonHoverCol, textColor)
{
    $("#formHeader").css({"background-color": headerCol});
    $(".form-content-wrap").css({"background-color": foregroundCol});
    $(".form-content").css({"background-color": foregroundCol});
    $("#form_setting_para_1").css({"color": headingCol});
    $("#form_setting_para_2").css({"color": DescriptionCol});
    $('.fg-outlined').css({"border-color": borderCol});
    var styleHtml = "#unorder_list_container input[type=text]::-webkit-input-placeholder,#unorder_list_container textarea::-webkit-input-placeholder" +
            "{color:" + fieldDescriptionCol + ";}#unorder_list_container input[type=text]::-moz-placeholder,#unorder_list_container textarea::-moz-placeholder{" +
            "color:" + fieldDescriptionCol + ";}";
    $('#placeHolderColor').html(styleHtml);
     $(".paracolor h3,.paracolor").css({color: fieldDescriptionCol});
    $("#unorder_list_container input[type=text]").css({"border": "1px solid " + darken(borderCol, '10'), color: fieldDescriptionCol});
    $("#unorder_list_container textarea").css({"border": "1px solid " + darken(borderCol, '10'), color: fieldDescriptionCol});
    $('#unorder_list_container label').css({color: fieldDescriptionCol});
    $('#unorder_list_container span').css({color: fieldDescriptionCol});
    $('#unorder_list_container select').css({color: fieldDescriptionCol});  
    $(".fg-outlined").css({"background": fieldBackgroundCol});
    $("#save").css({"background-color": buttonCol});
    $("#save").css({"color": textColor});
    var color = new Array(headerCol, foregroundCol, headingCol, DescriptionCol, fieldDescriptionCol, borderCol, selectCol, fieldBackgroundCol, buttonCol, buttonHoverCol, textColor);
    var elem = new Array("headerBackgroundCol", "foregroundCol", "formHeadingCol", "fieldDescriptionCol", "fieldTextCol", "fieldBorderCol", "fieldSelectCol", "fieldBackgroundCol", "buttonCol", "buttonHoverCol", "buttonTextCol");
    $(elem).each(function(i, item) {
//        $("#" + item).css({"background-color": color[i]}).parent().ColorPickerSetColor(color[i]);
    });
    $('#buttonHover').html('#form_wraper_div #unorder_list_container #save:hover {background-color:' + buttonHoverCol + '!important;}');
    var dcolor = darken(selectCol, '10')
    var selectStr = ".current{border-color:" + selectCol + "!important;}.current textarea{border-color:" + dcolor + "!important;}" +
            ".current input[type=text]{border-color:" + dcolor + "!important;}";
    $('#fieldSelect').html(selectStr);
    defaults[26] = dcolor;
    defaults[27] = darken(borderCol, '10');
    defaults[5] = headerCol;
    defaults[6] = foregroundCol;
    defaults[7] = headingCol;
    defaults[8] = DescriptionCol;
    defaults[9] = fieldDescriptionCol;
    defaults[10] = borderCol;
    defaults[11] = selectCol;
    defaults[12] = fieldBackgroundCol;
    defaults[13] = buttonCol;
    defaults[14] = buttonHoverCol;
    defaults[15] = textColor;
}


function changeColor(ref, color)
{
    color = "#" + color;
    switch (ref)
    {
        case"formHeader":
            {
                $("#" + ref).css({"background-color": color});
                defaults[5] = color;
                break;
            }
        case"companyName":
            {
                $("#" + ref).css({"color": color});
                defaults[2] = color;
                break;
            }
        case"footerSubHeading":
            {
                $("#footerSubHeading").css({"color": color});
                $("#footerLocation").css({"color": color});
                $("#formSocial a").css({"color": color});
                $("#formContact span").css({"color": color});
                $("#yourSite").css({"color": color});
                defaults[3] = color;
                break;
            }

        case"form_setting_para_1":
            {
                $("#" + ref).css({"color": color});
                defaults[7] = color;
                break;
            }
        case"form_setting_para_2":
            {
                $("#" + ref).css({"color": color});
                defaults[8] = color;
                break;
            }
        case"form-content":
            {
                $("." + ref).css({"background-color": color});
                $(".form-content-wrap").css({"background-color": "#" + color});
                defaults[6] = color;
                break;
            }
        case "saveButtonColor":
            {
                $("#save").css({"background-color": color});
                defaults[13] = color;
                break;
            }
        case "saveButtonHover":
            {
                $('#buttonHover').html('#form_wraper_div #unorder_list_container #save:hover {background-color:' + color + '!important;}');
                defaults[14] = color;
                break;
            }
        case "saveButtonText":
            {
                $("#save").css({"color": color});
                defaults[15] = color;
                break;
            }
        case "fieldBackgroundColor":
            {
                $(".fg-outlined").css({"background-color": color});
                defaults[12] = color;
                break;
            }
        case "fieldTextColor":
            {
                var styleHtml = "#unorder_list_container input::-webkit-input-placeholder,#unorder_list_container textarea::-webkit-input-placeholder" +
                        "{color:" + color + ";}#unorder_list_container input::-moz-placeholder,#unorder_list_container textarea::-moz-placeholder{" +
                        "color:" + color + ";}";
                $('#placeHolderColor').html(styleHtml);
                $("#unorder_list_container input[type=text]").css({color: color});
                $("#unorder_list_container textarea").css({color: color});
                $('#unorder_list_container label').css({color: color});
                $('#unorder_list_container span').css({color: color});
                $('#unorder_list_container select').css({color: color});
                $('#unorder_list_container h3').css({color: color});
                $('#unorder_list_container p').css({color: color});
                $(".paracolor h3,.paracolor").css({color: color});
                defaults[9] = color;
                break;
            }
        case "fieldBorder":
            {
                var dcolor = darken(color, '10');
                $('.fg-outlined').css({"border-color": color});
                $("#unorder_list_container input[type=text]").css({"border": "1px solid " + dcolor});
                $("#unorder_list_container textarea").css({"border": "1px solid " + dcolor});
                defaults[10] = color;
                defaults[27] = darken(dcolor, '10');
                break;
            }
        case "selectBorder":
            {
                var dcolor = darken(color, '10');
                var selectStr = ".current{border-color:" + color + "!important;}.current textarea{border-color:" + dcolor + "!important;}" +
                        ".current input[type=text]{border-color:" + dcolor + "!important;}";
                $('#fieldSelect').html(selectStr);
                defaults[11] = color;
                defaults[26] = dcolor;
                break;
            }
        case "footerBackgroundColor":
            {
                $("#formFooter").css({"background-color": color});
                defaults[1] = color;
                break;
            }
        default:
            {

            }
    }
}
function changeFontFamily(val)
{
if(val!='Select Font'){
    $('#form_wraper_div').find('*').css({"font-family": val});
    $('#formFooter').find('*').not('i').css({"font-family": val});
    defaults[4] = val;
}
}

function file_rename(fname, bh) {
    var f = $("#form_wraper_div form:first").attr('id');
    var nfname = fname.split(' ').join('_');
    var url = nfname.lastIndexOf('.');
    var d = new Date();
    var y = d.getFullYear();
    var m = d.getMonth() + 1;
    var new_url = y + "" + m + "_" + bh + "_" + f + nfname.substring(url);
    return new_url;
}
var loadFlage = true,saveFlage=false, uploadCount = 0, totalUpload = 0;
function changeBackground(input)
{
    reader_flag=true;
    totalUpload++;
    if (input.files && input.files[0]) {
        var file = input.files[0], reader;
        if (window.FileReader)
        {
            reader = new FileReader();
            reader.readAsDataURL(file);
        }
        else
        {
             file = jQuery("#"+input.id).prop("files")[0];
             reader_flag=false;
        }
       if(reader_flag==true){
        reader.onload = function(e) {
            $('body').css({"background-image": "url('" + e.target.result + "')"});
            var ba = "b";
            var new_name = file_rename(file.name,ba);
            defaults[0] = base_url + "upload/bg_img/" + new_name;
            backGroundImageName = base_url + "upload/bg_img/" + new_name;
            backGroundImage = e.target.result;
        };
       }
        var formdata = new FormData();
        formdata.append("images", file);
        formdata.append("formId", $("#form_wraper_div form:first").attr('id'));
        formdata.append("imageType", "b");
        // reader.readAsDataURL(file);
       fileUpload(formdata,'background');
    }
}
function changeHeaderLogo(input)
{
    reader_flag=true;
    totalUpload++;
    if (input.files && input.files[0]) {
        loadFlage = false;
        var ba = "h";
        var file = input.files[0], reader;
        var new_name = file_rename(file.name, ba);
        defaults[25] = base_url + "upload/bg_img/" + new_name;
        if (window.FileReader)
        {
            reader = new FileReader();
            reader.readAsDataURL(file);
        }
        else
        {
            file = jQuery("#"+input.id).prop("files")[0];
            reader_flag=false;
        }
       if(reader_flag==true){
        reader.onload = function(e) {
        $('#logoImage').attr('src', "" + e.target.result);
        $("#headerLogoLink").css({display: "none"});
        };
        }
        var formdata = new FormData();
        formdata.append("images", file);
        formdata.append("formId", $("#form_wraper_div form:first").attr('id'));
        formdata.append("imageType", "h");
        fileUpload(formdata,'header');
    }
}
var d = new Date();
d.setTime(d.getTime() + (1 * 24 * 60 * 60 * 1000));
var expires =d.toGMTString();
document.cookie = "no_u=up; expires="+expires;
function fileUpload(formdata,g_chk)
{
    document.cookie = "no_u=no_up; expires="+expires;
    upload_prog = 'upload_progress';
    jQuery.ajax({
        url: base_url + "color_change/ajax_file_upload",
        type: "POST",
        data: formdata,
        processData: false,
        contentType: false,
        complete: function(res) {
            
            uploadCount++;
            if (totalUpload == uploadCount)
            {       
                loadFlage=true;
                $("#loading2").css({display: "none"});          
                $("#infMessage").val("SAVE CHANGES");
        document.cookie="no_u=up; expires="+expires;
         var upload_prog = getCookie('no_u');
            }
            if(g_chk=='background') {
            document.body.style.setProperty( 'background-image','url('+res.responseText+'?time=' + new Date().getTime()+')','important');
            defaults[0]=jQuery.trim(res.responseText);
            }
           else if(g_chk=='header') {
            $('#logoImage').attr('src',jQuery.trim(res.responseText) + '?time=' + new Date().getTime());
            $("#headerLogoLink").css({display: "none"});   
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            alert("function error" + errorThrown);
        }
    });
}

function temp()
{
    this.viewJson = "";
}
function getCookie(cname)
{
var name = cname + "=";
var ca = document.cookie.split(';');
for(var i=0; i<ca.length; i++) 
  {
  var c = ca[i].trim();
  if (c.indexOf(name)==0) return c.substring(name.length,c.length);
  }
return "";
}
function saveChanges()
{
   var upload_prog = getCookie('no_u');
if(upload_prog=='up'){
  var uriid = document.getElementById('uri_value').getAttribute('uri');
 document.cookie = "no_u=; expires=Thu, 01 Jan 1970 00:00:00 GMT";
    $(".save-changes").css({display: "none"});
    $("#saveChangesLoading").css({display: "block"});
    var view_val = jQuery.toJSON(new optionArray());
    jQuery.ajax({
        type: "POST",
        url: base_url + "color_change/view_json",
        data: { view_js: view_val,
               uri_id:uriid    },
        async: false,
        success: function(data) {
            if (loadFlage)
            {
                $("#loading2").css({display: "none"});
                $("#infMessage").val("Saved");
                setTimeout(function() {
                    $("#infMessage").val("SAVE CHANGES");
                    window.location.assign(base_url + "your_form?f="+uriid);
                }, 2000);
            }else
                {
                    saveFlage=true;
                }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            //alert("error data inserted successfully");
        }
    });
}
else{   
    $(".save-changes").css({display: "none"});
    $("#saveChangesLoading").css({display: "block"});
    
     setTimeout(function() {
                    saveChanges();
                }, 3000);
    

}
}


function madeTempletChange(bodyBackground, footerBackground, footerHeadingColor, footerTextcolor, fontFamily)
{
    $('body').css({"background-image": "url('" + bodyBackground + "')"});
    backGroundImage = bodyBackground;
    $("#formFooter").css({"background-color": footerBackground});
    $("#companyName").css({"color": "#" + footerHeadingColor});
    $("#footerSubHeading").css({"color": footerTextcolor});
    $("#footerLocation").css({"color": footerTextcolor});
    $("#formSocial a").css({"color": footerTextcolor});
    $("#formContact span").css({"color": footerTextcolor});
    $("#yourSite").css({"color": footerTextcolor});
    changeFontFamily(fontFamily);
    var tempArray = new Array('footerHeadingCol', 'footerSubHeadCol', 'footerBackgroundCol');
    var tempColArray = new Array(footerHeadingColor, footerTextcolor, footerBackground);
    $(tempArray).each(function(i, item) {
        $("#" + item).css({"background-color": tempColArray[i]}).parent().ColorPickerSetColor(tempColArray[i]);
    });
    if (bodyBackground.length > 100)
    {
        defaults[0] = backGroundImageName;
    } else
    {
        defaults[0] = bodyBackground;
    }
    defaults[1] = footerBackground;
    defaults[2] = footerHeadingColor;
    defaults[3] = footerTextcolor;
    defaults[4] = fontFamily;
}

function optionArray()
{
    this.fi = $("#form_wraper_div form:first").attr('id');
    this.background = defaults[0];
    this.headerLogoImage = defaults[25];
    this.field_outer_color = defaults[10];
    this.field_inner_color = defaults[27];
    this.select_outer_color = defaults[11];
    this.select_inner_color = defaults[26];
    this.header_color = defaults[5];
    this.foreground_color = defaults[6];
    this.head_text_color = defaults[7];
    this.head_desc_color = defaults[8];
    this.field_background = defaults[12];
    this.field_label_color = defaults[9];
    this.button_color = defaults[13];
    this.button_hover_color = defaults[14];
    this.button_text_color = defaults[15];
    this.footerHeadingColor = defaults[2];
    this.footerTextColor = defaults[3];
    this.footerBackGroundColor = defaults[1];
    this.companyName = defaults[16];
    this.companyDescription = defaults[17];
    this.facebookPage = defaults[18];
    this.twitterPage = defaults[19];
    this.googalPlusPage = defaults[20];
    this.address = defaults[21];
    this.yourMail = defaults[22];
    this.contactNumber = defaults[23];
    this.font_family = defaults[4];
    this.yourSite = defaults[24];
}//companyName companyDescription facebookPage twitterPage googalPlusPage address yourMail contactNumber yourSite

function selectTemplet(imgSrc, template, type)
{
    switch (template)
    {
        case"template7":
            {
                if (type) {
                    madeTempletChange(imgSrc, "#fff0e3", "#424242", "#6a6a6a", "Source Sans Pro");
                    changeTemplet("#54a139", "#faf3ed", "#010101", "#908d8d", "#000000", "#bfdcb0", "#f4e0a9", "#ffffff", "#6baf59", "#000000", "#ffffff");
                } else
                {
                    HoverChange(imgSrc, "#fff0e3", "#424242", "#6a6a6a", "Source Sans Pro", "#54a139", "#faf3ed", "#010101", "#908d8d", "#000000", "#bfdcb0", "#f4e0a9", "#ffffff", "#6baf59", "#000000", "#ffffff");
                }
                break;
            }

        case"template3":
            {
                // bodyBackground,footerBackground,footerHeadingColor,footerTextcolor,fontFamily 
                if (type) {
                    madeTempletChange(imgSrc, "#ffe0cc", "#2a2a2a", "#2a2a2a", "Source Sans Pro");
                    changeTemplet("#774728", "#ffffff", "#010101", "#908d8d", "#000000", "#dccbba", "#f7e6b2", "#ffffff", "#987653", "#000000", "#ffffff");
                } else
                {
                    HoverChange(imgSrc, "#ffe0cc", "#2a2a2a", "#2a2a2a", "Source Sans Pro", "#774728", "#ffffff", "#010101", "#908d8d", "#000000", "#dccbba", "#f7e6b2", "#ffffff", "#987653", "#000000", "#ffffff");
                }
                break;
            }

        case"template2":
            {
                if (type) {
                    madeTempletChange(imgSrc, "#f3f3f3", "#352917", "#000000", "Source Sans Pro");
                    changeTemplet("#e83238", "#ffffff", "#3c3c3c", "#525252", "#000000", "#d8d8d8", "#a9e0f7", "#ffffff", "#e83238", "#000000", "#ffffff");
                } else
                {
                    HoverChange(imgSrc, "#f3f3f3", "#352917", "#000000", "Source Sans Pro", "#e83238", "#ffffff", "#3c3c3c", "#525252", "#000000", "#d8d8d8", "#a9e0f7", "#ffffff", "#e83238", "#000000", "#ffffff");
                }
                break;
            }
        case"template12":
            {
                if (type) {
                    madeTempletChange(imgSrc, "#e7f1ff", "#1f56a0", "#000000", "Source Sans Pro");
                    changeTemplet("#3d80db", "#ffffff", "#3c3c3c", "#525252", "#000000", "#d8d8d8", "#a9e0f7", "#ffffff", "#3d80db", "#000000", "#ffffff");
                } else
                {
                    HoverChange(imgSrc, "#e7f1ff", "#1f56a0", "#000000", "Source Sans Pro", "#3d80db", "#ffffff", "#3c3c3c", "#525252", "#000000", "#d8d8d8", "#a9e0f7", "#ffffff", "#3d80db", "#000000", "#ffffff");
                }
                break;
            }

        case"template10":
            {
                if (type) {
                    madeTempletChange(imgSrc, "#f3f3f3", "#242424", "#000000", "Source Sans Pro");
                    changeTemplet("#18b499", "#ffffff", "#159780", "#525252", "#000000", "#9bddc1", "#a9e0f7", "#ffffff", "#18b499", "#000000", "#ffffff");
                } else
                {
                    HoverChange(imgSrc, "#f3f3f3", "#242424", "#000000", "Source Sans Pro", "#18b499", "#ffffff", "#159780", "#525252", "#000000", "#9bddc1", "#a9e0f7", "#ffffff", "#18b499", "#000000", "#ffffff");
                }
                break;
            }
        case"template6":
            {
                if (type)
                {
                    madeTempletChange(imgSrc, "#e0f4ff", "#242424", "#000000", "Source Sans Pro");
                    changeTemplet("#2072a1", "#ffffff", "#000000", "#525252", "#000000", "#9ed2f0", "#f8dbad", "#ffffff", "#2072a1", "#000000", "#ffffff");
                } else
                {
                    HoverChange(imgSrc, "#e0f4ff", "#242424", "#000000", "Source Sans Pro", "#2072a1", "#ffffff", "#000000", "#525252", "#000000", "#9ed2f0", "#f8dbad", "#ffffff", "#2072a1", "#000000", "#ffffff")
                }
                break;
            }
        case"template9":
            {
                if (type)
                {
                    madeTempletChange(imgSrc, "#f7f7f7", "#424242", "#6a6a6a", "Source Sans Pro");
                    changeTemplet("#ef4362", "#ffffff", "#010101", "#908d8d", "#000000", "#feced7", "#f3d3be", "#ffffff", "#ef4362", "#000000", "#ffffff");
                } else
                {
                    HoverChange(imgSrc, "#f7f7f7", "#424242", "#6a6a6a", "Source Sans Pro", "#ef4362", "#ffffff", "#010101", "#908d8d", "#000000", "#feced7", "#f3d3be", "#ffffff", "#ef4362", "#000000", "#ffffff");
                }
                break;
            }
        case"template8":
            {
                if (type)
                {
                    madeTempletChange(imgSrc, "#ffe7db", "#424242", "#6a6a6a", "Source Sans Pro");
                    changeTemplet("#f0423e", "#fff3ed", "#010101", "#908d8d", "#000000", "#ffccc8", "#f3cdb5", "#ffffff", "#f0423e", "#000000", "#ffffff");
                } else
                {
                    HoverChange(imgSrc, "#ffe7db", "#424242", "#6a6a6a", "Source Sans Pro", "#f0423e", "#fff3ed", "#010101", "#908d8d", "#000000", "#ffccc8", "#f3cdb5", "#ffffff", "#f0423e", "#000000", "#ffffff")
                }
                break;
            }
        case"template15":
            {
                if (type)
                {
                    madeTempletChange(imgSrc, "#ffefe8", "#424242", "#6a6a6a", "Source Sans Pro");
                    changeTemplet("#ef672d", "#ffffff", "#010101", "#908d8d", "#000000", "#feccb6", "#dcdcdc", "#ffffff", "#ef672d", "#000000", "#ffffff");
                } else
                {
                    HoverChange(imgSrc, "#ffefe8", "#424242", "#6a6a6a", "Source Sans Pro", "#ef672d", "#ffffff", "#010101", "#908d8d", "#000000", "#feccb6", "#dcdcdc", "#ffffff", "#ef672d", "#000000", "#ffffff");
                }
                break;
            }
        case"template4":
            {
                if (type)
                {
                    madeTempletChange(imgSrc, "#f1f1f1", "#424242", "#6a6a6a", "Source Sans Pro");
                    changeTemplet("#2d5986", "#ffffff", "#010101", "#908d8d", "#000000", "#dae9ff", "#dcdcdc", "#ffffff", "#2d5986", "#000000", "#ffffff");
                } else
                {
                    HoverChange(imgSrc, "#f1f1f1", "#424242", "#6a6a6a", "Source Sans Pro", "#2d5986", "#ffffff", "#010101", "#908d8d", "#000000", "#dae9ff", "#dcdcdc", "#ffffff", "#2d5986", "#000000", "#ffffff");
                }
                break;
            }
        case"template11":
            {
                if (type)
                {
                    madeTempletChange(imgSrc, "#e6e9da", "#424242", "#6a6a6a", "Source Sans Pro");
                    changeTemplet("#76870f", "#fdfff4", "#010101", "#908d8d", "#000000", "#dbe0a4", "#dbdcd7", "#ffffff", "#76870f", "#000000", "#ffffff");
                } else
                {
                    HoverChange(imgSrc, "#e6e9da", "#424242", "#6a6a6a", "Source Sans Pro", "#76870f", "#fdfff4", "#010101", "#908d8d", "#000000", "#dbe0a4", "#dbdcd7", "#ffffff", "#76870f", "#000000", "#ffffff")
                }
                break;
            }
        case"template1":
            {
                if (type)
                {
                    madeTempletChange(imgSrc, "#e9eef8", "#424242", "#6a6a6a", "Source Sans Pro");
                    changeTemplet("#393e4a", "#ffffff", "#010101", "#908d8d", "#000000", "#d1d9e1", "#c3daff", "#ffffff", "#393e4a", "#000000", "#ffffff");
                }
                else
                {
                    HoverChange(imgSrc, "#e9eef8", "#424242", "#6a6a6a", "Source Sans Pro", "#393e4a", "#ffffff", "#010101", "#908d8d", "#000000", "#d1d9e1", "#c3daff", "#ffffff", "#393e4a", "#000000", "#ffffff");
                }
                break;
            }
        case"template13":
            {
                if (type)
                {
                    madeTempletChange(imgSrc, "#f7f7f7", "#424242", "#6a6a6a", "Source Sans Pro");
                    changeTemplet("#94561f", "#ffffff", "#010101", "#908d8d", "#000000", "#f4d7bc", "#ffdede", "#ffffff", "#94561f", "#000000", "#ffffff");
                } else
                {
                    HoverChange(imgSrc, "#f7f7f7", "#424242", "#6a6a6a", "Source Sans Pro", "#94561f", "#ffffff", "#010101", "#908d8d", "#000000", "#f4d7bc", "#ffdede", "#ffffff", "#94561f", "#000000", "#ffffff");
                }
                break;
            }
        case"template14":
            {
                if (type)
                {
                    madeTempletChange(imgSrc, "#f1f1f1", "#424242", "#6a6a6a", "Source Sans Pro");
                    changeTemplet("#1d7bb8", "#ffffff", "#010101", "#908d8d", "#000000", "#98d0f1", "#93dff8", "#ffffff", "#1d7bb8", "#000000", "#ffffff");
                } else
                {
                    HoverChange(imgSrc, "#f1f1f1", "#424242", "#6a6a6a", "Source Sans Pro", "#1d7bb8", "#ffffff", "#010101", "#908d8d", "#000000", "#98d0f1", "#93dff8", "#ffffff", "#1d7bb8", "#000000", "#ffffff");
                }
                break;
            }
        case"template5":
            {
                if (type)
                {
                    madeTempletChange(imgSrc, "#eff1e1", "#424242", "#6a6a6a", "Source Sans Pro");
                    changeTemplet("#579446", "#ffffff", "#010101", "#908d8d", "#000000", "#dee4b3", "#ece9e6", "#ffffff", "#6b761b", "#000000", "#ffffff");
                } else
                {
                    HoverChange(imgSrc, "#eff1e1", "#424242", "#6a6a6a", "Source Sans Pro", "#579446", "#ffffff", "#010101", "#908d8d", "#000000", "#dee4b3", "#ece9e6", "#ffffff", "#6b761b", "#000000", "#ffffff");
                }
                break;
            }
    }
}

function HoverChange(bodyBackground, footerBackground, footerHeadingColor, footerTextcolor, fontFamily, headerCol, foregroundCol, headingCol, DescriptionCol, fieldDescriptionCol, borderCol, selectCol, fieldBackgroundCol, buttonCol, buttonHoverCol, textColor)
{
    $('body').css({"background-image": "url('" + bodyBackground + "')"});
    $("#formFooter").css({"background-color": footerBackground});
    $("#companyName").css({"color": "#" + footerHeadingColor});
    $("#footerSubHeading").css({"color": footerTextcolor});
    $("#footerLocation").css({"color": footerTextcolor});
    $("#formSocial a").css({"color": footerTextcolor});
    $("#formContact span").css({"color": footerTextcolor});
    $("#yourSite").css({"color": footerTextcolor});
    $('#form_wraper_div').find('*').css({"font-family": fontFamily});
    $('#formFooter').find('*').not('i').css({"font-family": fontFamily});
    var tempArray = new Array('footerHeadingCol', 'footerSubHeadCol', 'footerBackgroundCol');
    var tempColArray = new Array(footerHeadingColor, footerTextcolor, footerBackground);
    $(tempArray).each(function(i, item) {
        $("#" + item).ColorPickerSetColor(tempColArray[i]).find('span:first').css({"background": tempColArray[i]});
    });
    $("#formHeader").css({"background-color": headerCol});
       $(".paracolor h3,.paracolor").css({color: fieldDescriptionCol});
    $(".form-content-wrap").css({"background-color": foregroundCol});
    $(".form-content").css({"background-color": foregroundCol});
    $("#form_setting_para_1").css({"color": headingCol});
    $("#form_setting_para_2").css({"color": DescriptionCol});
    $('.fg-outlined').css({"border-color": borderCol});
    var styleHtml = "#unorder_list_container input::-webkit-input-placeholder,#unorder_list_container textarea::-webkit-input-placeholder" +
            "{color:" + fieldDescriptionCol + ";}#unorder_list_container input[type=text]::-moz-placeholder,#unorder_list_container textarea::-moz-placeholder{" +
            "color:" + fieldDescriptionCol + ";}";
    $('#placeHolderColor').html(styleHtml);
    $("#unorder_list_container input[type=text]").css({"border": "1px solid " + darken(borderCol, '10'), color: fieldDescriptionCol});
    $("#unorder_list_container textarea").css({"border": "1px solid " + darken(borderCol, '10'), color: fieldDescriptionCol});
    $('#unorder_list_container label').css({color: fieldDescriptionCol});
    $('#unorder_list_container span').css({color: fieldDescriptionCol});
    $('#unorder_list_container select').css({color: fieldDescriptionCol});
    $(".fg-outlined").css({"background": fieldBackgroundCol});
    $("#save").css({"background-color": buttonCol});
    $("#save").css({"color": textColor});
    var color = new Array(headerCol, foregroundCol, headingCol, DescriptionCol, fieldDescriptionCol, borderCol, selectCol, fieldBackgroundCol, buttonCol, buttonHoverCol, textColor);
    var elem = new Array("headerBackgroundCol", "foregroundCol", "formHeadingCol", "fieldDescriptionCol", "fieldTextCol", "fieldBorderCol", "fieldSelectCol", "fieldBackgroundCol", "buttonCol", "buttonHoverCol", "buttonTextCol");
    $(elem).each(function(i, item) {
        $("#" + item).css({"background": color[i]}).parent().ColorPickerSetColor(color[i]);
    });
    $('#buttonHover').html('#form_wraper_div #unorder_list_container #save:hover {background-color:' + buttonHoverCol + '!important;}');
    var dcolor = darken(selectCol, '10')
    var selectStr = ".current{border-color:" + selectCol + "!important;}.current textarea{border-color:" + dcolor + "!important;}" +
            ".current input[type=text]{border-color:" + dcolor + "!important;}";
    $('#fieldSelect').html(selectStr);
}

function linkContain(val)
{
    if (val.search("http") >= 0)
    {
        return true;
    } else
    {
        return false;
    }
}

function hideAll()
{
    var footerFlage = true, contactFlage = true, compnayFlage = true;
    $("#footer_option_content").find("input[type='text']").each(function(i) {
        if (i < 5)
        {
            if ($(this).val().trim() != "")
            {
                footerFlage = false;
                compnayFlage = false;
            }
        } else
        {
            if ($(this).val().trim() != "")
            {
                footerFlage = false;
                contactFlage = false;
            }
        }
    });
    if (footerFlage)
    {
        $("#formFooter").css({display: "none"});
    }
    else
    {
        $("#formFooter").css({display: "block"});
    }
    if (compnayFlage)
    {
        $('.dotted-border').css({display: "none"});
    } else
    {
        $('.dotted-border').css({display: "block"});
    }

    if (contactFlage)
    {
        $('.dotted-border').css({display: "none"});
    }
}

function deleteLogoImage()
{
    $("#headerLogoLink").css({display: "block"});
    $('#logoImage').attr('src', base_url + "app_data/view/images/form-header-logo.png");
    defaults[25] = base_url + "app_data/view/images/form-header-logo.png";
}