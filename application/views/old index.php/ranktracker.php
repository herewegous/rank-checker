<?php
//code to show the value of userid and username which is passed when page is called
$numproject = 0;
//echo $userid; 
$b = base_url();
//echo $b;
//echo $username;
if ($this->session->userdata('username')) {
   foreach ($uprofile->result() as $row) {
    $name = $row->name;
}
}
else {
    header('location:');
    header('Location:' . base_url() . 'tracker/login');
}


?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Rank Tracker</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="http://netdna.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/global.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/fg-main-style.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/animate.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/animate.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/pagestyle.css" />
        <script type="text/javascript" src="<?php echo base_url(); ?>js/bootstrap.min_1.js" ></script>
        <script type="text/javascript" src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>js/canvasjs.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>js/script1.js"></script>
        <script type="text/javascript" src="http://www.amcharts.com/lib/3/amcharts.js"></script>
        <script type="text/javascript" src="http://www.amcharts.com/lib/3/serial.js"></script>
        <script type="text/javascript" src="http://www.amcharts.com/lib/3/themes/light.js"></script>


        <script>
 //==============================function to genrate graph for the keyword===========================================
//===================================================================================================================
            function graph(keyid) {
                //      alert('graph called');
                //        alert(keyid);
                $.ajax({
                    type: 'POST',
                    url: '<?php echo base_url() . "tracker/graphdata" ?>',
                    dataType: 'json',
                    data: {"keyid": keyid},
                    success: function (data)
                    {
                        for (var m = 0; m < data.length; m++)
                        {
                            var i = data[m].ranking;
                            //           alert(i);
                            var date = data[m].date;
                            //       alert(date);
                        }

                        var resultArray = i.split(',').map(function (i) {
                            return Number(i);
                        });
//alert(resultArray[0]);
                        var datearr = date.split(',')
//alert(datearr[0]);
//alert(datearr[1]);
                        var chartData = [];
                        var firstDate = new Date();
                        firstDate.setDate(firstDate.getDate() - 5);
                        // alert(1);
                        //  alert('length of srray'+resultArray.length);
                        for (var z = 0; z < resultArray.length; z++)
                        {
                            //     alert(2);
                            var newDate = new Date(firstDate);
                            newDate.setDate(newDate.getDate() + z);
                            chartData.push({
                                date: datearr[z],
                                visits: resultArray[z]
                            });
                        }
                        // alert('visit called');
                        for (var z = 0; z < resultArray.length; z++)
                        {
                            // alert(chartData[z].visits);
                            //alert(chartData[z].date);
                        }
                        var chart = AmCharts.makeChart("chartdiv", {
                            "type": "serial",
                            "theme": "light",
                            "marginRight": 80,
                            "autoMarginOffset": 20,
                            "marginTop": 7,
                            "dataProvider": chartData,
                            "valueAxes": [{
                                    "axisAlpha": 0.2,
                                    "dashLength": 1,
                                    "position": "left"
                                }],
                            "mouseWheelZoomEnabled": true,
                            "graphs": [{
                                    "id": "g1",
                                    "balloonText": "[[category]]<br/><b><span style='font-size:14px;'>value: [[value]]</span></b>",
                                    "bullet": "round",
                                    "bulletBorderAlpha": 1,
                                    "bulletColor": "#FFFFFF",
                                    "hideBulletsCount": 50,
                                    "title": "red line",
                                    "valueField": "visits",
                                    "useLineColorForBulletBorder": true
                                }],
                            "chartScrollbar": {
                                "autoGridCount": true,
                                "graph": "g1",
                                "scrollbarHeight": 40
                            },
                            "chartCursor": {
                            },
                            "categoryField": "date",
                            "categoryAxis": {
                                "parseDates": true,
                                "axisColor": "#DADADA",
                                "dashLength": 1,
                                "minorGridEnabled": true
                            },
                            "export": {
                                "enabled": true
                            }
                        });
                        chart.dataDateFormat = "YYYY-MM-DD";
                        chart.addListener("rendered", zoomChart);
                        zoomChart();


                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert('Error Message: ' + textStatus);
                        alert('HTTP Error: ' + errorThrown);
                    }
                });


            }
// this method is called when chart is first inited as we listen for "rendered" event
            function zoomChart() {
                // different zoom methods can be used - zoomToIndexes, zoomToDates, zoomToCategoryValues
                chart.zoomToIndexes(chartData.length - 40, chartData.length - 1);
            }


// generate some random data, quite different range
            function generateChartData() {
                var chartData = [];
                var firstDate = new Date();
                firstDate.setDate(firstDate.getDate() - 5);

//y=2015;
//m=09;
//d=07;

                for (var i = 0; i < 1000; i++) {
                    // we create date objects here. In your data, you can have date strings
                    // and then set format of your dates using chart.dataDateFormat property,
                    // however when possible, use date objects, as this will speed up chart rendering.

                    var newDate = new Date(firstDate);
                    // var newDate = new Date(y,m,d);
                    newDate.setDate(newDate.getDate() + i);

                    var visits = Math.round(Math.random() * (40 + i / 5)) + 20 + i;

                    chartData.push({
                        date: newDate,
                        visits: visits
                    });
                }
                return chartData;
            }

        </script>


        <script>
          
            function urld(mailt)
            {
                alert(mailt);
            }
//=========== this function will show all  url with there alexa rank only for the project currently inserted without reloading page===========
//=============================================================================================================================================
            function abc(prnm) {

                $('ul a.formgetcall').removeClass('active');
                //      $('ul a.formgetcall').removeClass('active');
                //  $('ul li a').removeClass('active');
                $(this).addClass('active');
                $('li#' + prnm + 'list a').addClass('active');
                $('.orange').css('display', 'none');
                //      var pro = $(this).text();
                //      var pr = $(this).text();
                var pro = prnm;
                var pr = prnm;
//alert('value of pr is:'+pro);
                $("span#addurlbutton").text(pro);

                $(".content123").empty();

                var uid = document.getElementById("userid").value;

                $('#deletprobut').html('<img src="<?php echo base_url() ?>images/cross button image.png" style="margin-left:10px;display:inline;width:25px;height:25px;float:right;margin-right:10px;margin-top:6px;" onclick="deletfunction(' + '\'' + pr + '\'' + ',' + uid + ')">');
                $('#hid1').val(pro);// this will write the value for url popup box
                
            //=============================below code used to show ajax image while loading content of project============================================       
           //===========================================================================================================================================       
            $('#urlheading').hide();
            $('#projdetailloding').show(); 
        $('#nourlmsg').css('display', 'none');
           //============================= code to show ajax image while loading content of project ends============================================       
           //=========================================================================================================================================== 

    function waitfunction(){ 
        
      $.ajax({
                    type: 'POST',
                    url: '<?php echo base_url() . "tracker/fetchwebcontent" ?>',
                    dataType: 'json',
                    //data : data,
                    data: {"id": pr, "usid": uid},
                    success: function (data)
                    {
 $('#projdetailloding').hide(); 
                        //       console.log(data);

                        var q = data.length;
                        //     alert(q);

                        if (q < 2) {
                            $('#nourlmsg').css('display', 'block');
                            $('#urlheading').css('display', 'none');
                        }
                        else {
                            $('#nourlmsg').css('display', 'none');
                            //$('#urlheading').css('display','table-row');
                            $('#urlheading').show();
                        }

                        for (var m = 0; m < data.length; m++)
                        {
                            var q = data[m].project_links;
                            var i = data[m].page_rank;
                            var ar = data[m].alexa_rank;
                            var pid = data[m].project_id;
                            //below code is used to stop showing that entry doesn't contain url this will genrated when a new project is created
                            if (q.length == 0) {
                            }
                            else {
                                $(".content123").append('<tr id="' + pid + 'proid" class="' + pid + '"><td onclick="newnew(' + pid + ');"><a>' + q + '</a></td><td>' + i + '</td><td>' + ar + '</td><td><div  class="fg-row last"> <button id="btn' + pid + '" type="button" class="fg-btn green medium inline" data-toggle="modal" data-target=".demo-keyword" onclick="newfunction(' + pid + ',\'' + q + '\')" value="' + pid + '">add keyword</button></div></td><td><img  src="<?php echo $b; ?>images/cross button image.png" style="display:inline;width:25px;height:25px;float:right;margin-right:20px;margin-top:6px;" alt="delet" onclick="deleturlfunction(' + pid + ')"></td></tr> <tr id="botlinemngr" style="display:none"></tr>    <tr style="display: none;background-color:#0E7AC4;color:white;" id="' + pid + '"><td id="' + pid + 'keynum">Keyword</td><td>Ranking</td><td>Country</td><td>Statistics</td><td></td></tr>');  //here we used /' at the time of passing parameter to sent q as a string because it will not get sent normaly
                            }
                        }

                        $("#reg-form").fadeOut(500).hide(function ()
                        {
                            $(".result").fadeIn(500).show(function ()
                            {
                                $(".result").html(data);
                            });
                        });
                    }
                });
    };
   window.setTimeout( waitfunction, 700 ); // 5 seconds          
    
  
            }
        </script>
        <script>
//=========this code will show the all information(websites and there alexa rank and page rank) stored in the project================
//===================================================================================================================================
            $(document).ready(function () {

                $("a.formgetcall").on("click", function () {
                      $('#wrongkeymessage').css('display','none');
                //    alert('formget calles');
                    //      alert('formget called');
                    $('ul a.formgetcall').removeClass('active');
                    //  $('ul li a').removeClass('active');
                    $(this).addClass('active');

                    var pro = $(this).text();
                    var pr = $(this).text();

                    $("span#addurlbutton").text(pro);

                    $(".content123").empty();

                    var uid = document.getElementById("userid").value;
                    $('#deletprobut').html('<img src="<?php echo base_url() ?>images/cross button image.png" style="margin-left:10px;display:inline;width:25px;height:25px;float:right;margin-right:10px;margin-top:6px;" onclick="deletfunction(' + '\'' + pr + '\'' + ',' + uid + ')">');

                    // <img src="<?php echo base_url() ?>images/cross button image.png" style="margin-left:10px;display:inline;width:15px;height:15px;float:right;margin-right:10px;margin-top:6px;" onclick="deletfunction()">
                    //$('#dfg'.val)
                    $('#hid1').val(pro);// this will write the value for url popup box
                    
           //=============================below code used to show ajax image while loading content of project============================================       
           //===========================================================================================================================================       
            $('#urlheading').hide();
            $('#projdetailloding').show();
             $('#nourlmsg').css('display', 'none');
             
            function waitfunction(){
   //  alert('after 5 secan called');
      $.ajax({
                        type: 'POST',
                        url: '<?php echo base_url() . "tracker/fetchwebcontent" ?>',
                        dataType: 'json',
                        //data : data,
                        data: {"id": pr, "usid": uid},
                        success: function (data)
                        {
                  $('#projdetailloding').hide();
                            //       console.log(data);

                            var q = data.length;
                            //     alert(q);

                            if (q < 2) {
                                $('#nourlmsg').css('display', 'block');
                                $('#urlheading').css('display', 'none');
                            }
                            else {
                                $('#nourlmsg').css('display', 'none');
                                //$('#urlheading').css('display','table-row');
                                $('#urlheading').show();
                          
                               
                              
                            }

                            for (var m = 0; m < data.length; m++)
                            {
                                var q = data[m].project_links;
                                var i = data[m].page_rank;
                                var ar = data[m].alexa_rank;
                                var pid = data[m].project_id;
                                //below code is used to stop showing that entry doesn't contain url this will genrated when a new project is created
                                if (q.length == 0) {
                                }
                                else {
                                    $(".content123").append('<tr id="' + pid + 'proid" class="' + pid + '"><td onclick="newnew(' + pid + ');"><a>' + q + '</a></td><td>' + i + '</td><td>' + ar + '</td><td><div  class="fg-row last"> <button id="btn' + pid + '" type="button" class="fg-btn green medium inline" data-toggle="modal" data-target=".demo-keyword" onclick="newfunction(' + pid + ',\'' + q + '\')" value="' + pid + '">add keyword</button></div></td><td><img src="<?php echo $b; ?>images/cross button image.png" style="display:inline;width:25px;height:25px;float:right;margin-right:20px;margin-top:6px;" alt="delet" onclick="deleturlfunction(' + pid + ')"></td></tr> <tr id="botlinemngr" style="display:none"></tr>    <tr style="display: none;background-color:#0E7AC4;color:white;" id="' + pid + '"><td id="' + pid + 'keynum">Keyword</td><td>Ranking</td><td>Country</td><td>Statistics</td><td></td></tr>');  //here we used /' at the time of passing parameter to sent q as a string because it will not get sent normaly
                                }
                            }

                            $("#reg-form").fadeOut(500).hide(function ()
                            {
                                $(".result").fadeIn(500).show(function ()
                                {
                                    $(".result").html(data);
                                });
                            });
                        }
                    });
                    
   };
   window.setTimeout( waitfunction, 700 ); // 5 seconds
  

    // $("#projdetailloding").show().delay(5000);

       
                });

                //code to set the id of project  in popup .demo keyword to add a keyword at project and url location

                $('#btn').click(function () {
                    var val = $(this).attr('value');
                    alert(val);
                    alert("Hello");
                });
                
                //code for suggestion starts
                
                //code for 
            });


        </script>

        <script>
            $(document).ready(function () {
                $(".but").click(function () {
                    $(".keyrank2").toggle(500);

                });
            });
            
  //====================================function to clear popup box===========================================================
//============================================================================================================================
            function clearurlpopup(){
                
                $('#wrongentrymessage').css('display','none');
                $('#nurl').val('');
                  $('#addurlloding').css('display','none');
                       $('#wrongkeymessage').css('display', 'none');
            }
            
            
         
//==================this function add the url currently inserted and add it in the list of url using ajax===================
//==========================================================================================================================
            function submiturl123(evt) {
                //   alert('submiturl callrd');

                evt.preventDefault();

                var naurl = $('#nurl').val();
                var proname = $('#hid1').val();
                var useid2 = $('#userid2').val();
                var usename2 = $('#username4').val();
                
               var naurl = naurl.trim();
          
                 
                 
           //          $(".content123").append("<tr class='dynamiccontimg'><td></td><td></td><td><img src='<?php echo $b; ?>images/loding.GIF'></td><td></td><td></td></tr>");
//popupcodeurl

         //  $('#popupcodeurl').modal('hide');
           $('#addurlloding').css('display','inline');

            //    document.getElementById("addurlbutt").click();
                $.ajax({
                    type: 'POST',
                    url: '<?php echo base_url() . "tracker/addurl" ?>',
                    dataType: 'json',
                    //data : data,"nurl":nurl,
                    data: {"hid": proname, "nurl": naurl, "userid2": useid2, "username2": usename2},
                    success: function (data)
                    {
                         var rt=0;
                        //   alert('after called');
                         if(data == "notvalid")
                        {
                 //          $('#popupcodeurl').modal('show');
                          $('#addurlloding').css('display','none');
                        //   $('.dynamiccontimg').css('display', 'none');
                              $('#wrongentrymessage').html('Invalid Url Entered');
                            $('#wrongentrymessage').css('display','block');
                          //     document.getElementById("addurlbutt").click();
                          //      document.getElementById("addurlbutt").click();
                            
                            rt++;
                        }
        
                       if (data == "ext")
                        {
                           // alert("Url Already Exist");
                           // document.getElementById("addurlbutt").click();
                            $('#addurlloding').css('display','none');
                        //    $('.dynamiccontimg').css('display', 'none');
                              $('#wrongentrymessage').html('Url Already Exist');
                            $('#wrongentrymessage').css('display','block');
              //             $('#popupcodeurl').modal('show');
                              rt++;
                        }
                      
                      if(rt==0)
                        {
                              $('#popupcodeurl').modal('hide');
                               $('#addurlloding').css('dislpay','none');
                  //        $('.dynamiccontimg').css('display', 'none');
                            //===========this code will change the number of url in dektop
                            var number = $("#numbur").html();
                            var e = parseInt(number) + 1;
                            $("#numbur").text(e);
                            //==============above code to change the number of url ends 

                            $('#nourlmsg').css('display', 'none');
                            $('#urlheading').show();
                            for (var m = 0; m < data.length; m++)
                            {
                                var q = data[m].project_links;
                                var i = data[m].page_rank;
                                var ar = data[m].alexa_rank;
                                var pid = data[m].project_id;

                            }
                            //  alert(q+i+ar+pid);
                            //   alert("not exist");
                            $(".content123").append('<tr id="' + pid + 'proid" class="' + pid + '"><td onclick="newnew(' + pid + ');"><a>' + q + '</a></td><td>' + i + '</td><td>' + ar + '</td><td><div  class="fg-row last"> <button id="btn' + pid + '" type="button" class="fg-btn green medium inline" data-toggle="modal" data-target=".demo-keyword" onclick="newfunction(' + pid + ',\'' + q + '\')" value="' + pid + '">add keyword</button></div></td><td><img src="<?php echo $b; ?>images/cross button image.png" style="display:inline;width:25px;height:25px;float:right;margin-right:20px;margin-top:6px;" alt="delet" onclick="deleturlfunction(' + pid + ')"></td></tr> <tr id="botlinemngr" style="display:none"></tr>    <tr style="display: none;background-color:#0E7AC4;color:white;" id="' + pid + '"><td id="' + pid + 'keynum">Keyword</td><td>Ranking</td><td>Country</td><td>Statistics</td><td></td></tr>');

                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $('.dynamiccontimg').css('display', 'none');
                        alert('Error Message: ' + textStatus);
                        alert('HTTP Error: ' + errorThrown);
                    }


                });
                    
        

                //   $(".content123").append("<tr class='dynamiccontimg'><td><img src='<?php echo $b; ?>images/ajax-loader.gif'></td><td><img src='<?php echo $b; ?>images/ajax-loader.gif'></td><td><img src='<?php echo $b; ?>images/ajax-loader.gif'></td><td></td><td></td></tr>");
            
            }
//==================this function fetch and add the current keyword and add rank of it in the list using ajax===================
//==============================================================================================================================
            function submitketword(evt) {
                
                evt.preventDefault();
         //              alert('called keyword');
                var keyword = $('#nkeyword').val();
                var projid = $('#hidproid').val();
                var url = $('#hidprourl').val();
                var userid = $('#userid1').val();
                var username = $('#username3').val();
                var countryname=$('#counrtyname').val();
                 var citycode=$('#multipleSelect').val();
           //        alert(countryname);
           //     alert(citycode);
                //     alert(keyword + projid + url + userid + username);
                
           var keyword = keyword.trim();               //remove extraa space in at starting and ending of a string
                  keyword = keyword.replace(/ +(?= )/g,'');  //remove more than one space in a string
           //        $x = preg_replace('/\s*,\s*/', ',', $x);
                   keyword = keyword.replace(/\s*,\s*/g, ',');
        if (keyword == '') {
            alert('keyword must be enterd');
        }
        else{
            
         document.getElementById("btn" + projid).click();
            $('#popupcodekey').modal('hide');
                // alert(projid);
                //               $("<tr class='dynamiccontkeyimg'><td><img src='<?php echo $b; ?>images/ajax-loader.gif'></td><td><img src='<?php echo $b; ?>images/ajax-loader.gif'></td><td><img src='<?php echo $b; ?>images/ajax-loader.gif'></td><img src='<?php echo $b; ?>images/ajax-loader.gif'><td>-</td><td></td></tr>").insertAfter($('#' + projid));
                $("<tr class='dynamiccontkeyimg'><td></td><td></td><td><img src='<?php echo $b; ?>images/loding.GIF'></td><td></td><td></td></tr>").insertAfter($('#' + projid));
                $.ajax({
                    type: 'POST',
                    url: '<?php echo base_url() . "tracker/addkeyword" ?>',
                    dataType: 'json',
                    //data : data,
                    data: {"nkeyword": keyword, "hiproid": projid, "hiprourl": url, "userid1": userid, "username3": username, "countryname": countryname, "citycode": citycode},
                    success: function (data)
                    {
                        //      alert("after called");
                           //   alert(data.length);
                           
                            if (data == "keyext")
                        {
                           //  document.getElementById("btn" + projid).click();
                   //            $('#popupcodekey').modal('show');
                          //  alert('keyword alresdy exxist');
                               $('#wrongkeymessage').css('display', 'block');
                               $('.dynamiccontkeyimg').css('display', 'none');
                          $("html, body").animate({ scrollTop: 0 }, "slow");
      }
                        else{
                           
                        $('.dynamiccontkeyimg').css('display', 'none');

                        //==============htis code is to write the number of keyboad in dashboard============
                        var keywordnum = $("#numbkey").html();
                        var e1 = parseInt(keywordnum) + data.length;
                        $("#numbkey").text(e1);
                        //==========code to write number of keyboars ends============
                        // var keynumproj=$( "#"+projid+"keynum" ).html();
                        //   var e2 = parseInt(keynumproj)+1;
                        //  $("#numbkey").text(e2);
                        $("#" + projid + "keynum").text('keywords');
                        //   $("#" + projid).toggle(500); 
                        //===========this code is manage the toggling of keywords when added
                        newnew(projid);
                        $("#" + projid).show(500);    //thi                   
                        $("." + projid + "abc").show(500);
                        $("#botlinemngr").show(500);
                        //============code to manage toggling ends=============================
                        //   $("#" + projid).show(500);

                        //       alert(q+i+keyid+j);
                        //   alert(projid);
                           for (var m = 0; m < data.length; m++)
                        {
                //        alert('outer loop called');
                           var q=data[m][0].keyword;
                         var i= data[m][0].ranking;
                          var cntry = data[m][0].country;
                         var keyid=data[m][0].keyword_id;
                          var pieces = i.split(/[\s,]+/);
                        var j = pieces[pieces.length - 1]
                      //   alert(q+j+keyid);
        
                        
                        $("#" + keyid).remove();  // this applied to remove same name keyword already exist

                        $('<tr id="' + keyid + '" class="' + projid + 'abc"><td>' + q + '</td><td>' + j + '</td><td>'+cntry+'</td><td data-toggle="modal" data-target=".demo-graph" id="' + projid + 'graph" style="padding-bottom:55px;height:50px;" onclick="graph(' + keyid + ');">-</td><td><img src="<?php echo $b; ?>images/cross button image.png" style="display:inline;width:25px;height:25px;float:right;margin-right:20px;margin-top:6px;" alt="delet" onclick="deletkeywordfunction(' + keyid + ')"></td></tr>').insertAfter($("#" + projid));
                        //   $("#"+keyid).css("display", "none");

                        var temp = new Array();
                        temp = i.split(",");
                        for (var p = 0; p < temp.length; p++)
                        {
                            temp[p] = parseInt(temp[p]);
                        }
                        console.log(temp);


                        var dps = new Array();

                        for (i = 0; i < temp.length; i++)
                        {
                            dps.push({x: i, y: temp[i]});
                        }
                        //	var dps = [{x: 1, y: 12}, {x: 2, y: 15}, {x: 3, y: 18}, {x: 4, y: 12}, {x: 5, y: 17},{x: 6, y: 10}, {x: 7, y: 13}, {x: 8, y: 18}, {x: 9, y: 20}, {x: 10, y: 17}];   //dataPoints. 

                        var chart = new CanvasJS.Chart(projid + "graph", {
                            height: 55,
                            width: 220,
                            title: {
                                text: ""
                            },
                            axisX: {
                                interlacedColor: "#F0F8FF"
                                        //  margin: 50,
                            },
                            axisY: {
                                margin: -5
                            },
                            data: [{
                                    type: "line",
                                    lineThickness: 1,
                                    dataPoints: dps
                                }]
                        });

                        chart.render();
                    }
                    }
                    }
                    ,
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert('Error Message: ' + textStatus);
                        alert('HTTP Error: ' + errorThrown);
                        $('.dynamiccontkeyimg').css('display', 'none');
                    }
                });
                }
            }
           
 //========================================== function to toggle add project =======================================================
//===================================================================================================================================
            
              //this code will hide and show the add project form
            function adproject()
            {
              $('#projectempty').css('display','none');
              $('#projectexist').css('display','none');
              
                $('#prnam').val("");
                var x = document.getElementById('projectform')
                if (x.style.display == 'block')
                    x.style.display = 'none';
                else
                    x.style.display = 'block';
                //   $('#projectform').slideToggle(500);
            }
            
//=================this function is used to insert project and it also prevent blank entry project in database =======================
////==================================================================================================================================


            function checkempty(event) {
           $('.alertmessage').css('display','none');
                event.preventDefault();
                   var prnam = $('#prnam').val();
              //   var prnam1 = $('#prnam').val();
                    var userid = $('#userid').val();
                    var username = $('#username').val();
                        var prnam = prnam.trim();
                         prnam = prnam.replace(/ +(?= )/g,'');
                if (prnam == '') {
                    //    alert('Project name can not be empty');
                    $('#projectempty').css('display', 'block');
                    $('#projectexist').css('display', 'none');
                    evt.preventDefault();
                }
                else {
                    $('.orange').css('display', 'none');


                 
                //                  var prnam = prnam1.trim();
                 //                 alert(prnam);
                    $.ajax({
                        type: 'POST',
                        url: '<?php echo base_url() . "tracker/addproject" ?>',
                        dataType: 'json',
                        //data : data,
                        data: {"prnam": prnam, "userid": userid, "username": username},
                        success: function (data)
                        {
                            if (data == "exist")
                            {
                                //        alert("Project Already Exist");
                                $('#projectexist').css('display', 'block');
                                $('#projectempty').css('display', 'none');
                            }
                            else
                            {
                                var number = $("#numbproj").html();
                                var e = parseInt(number) + 1;
                                //  alert(e);
                                $("#numbproj").text(e);

                                                   //    alert(data);
                                $('#dynamicurlcontainer').css('display', 'block');
                                $('#projectform').css('display', 'none');
                                $('ul a.formgetcall').removeClass('active');
                                $('ul').removeClass('active');
                                $(".all-options").append(' <li id="' + data + 'list" class="active"><a href="#mailget" class="formgetcall" onClick="abc(' + "'" + data + "'" + ');" data-toggle="tab" >' + data + '</a></li>');

                                //   location.reload();
                                //   '+"'"+data+"'"+'
                                //jQuery.ready();
                                $('#' + data + 'list a').addClass('active');

                                // alert(data + 'list');
                                //$("#foo").trigger("click");
                                $('li#' + data + 'list a').trigger('click');
                                //   document.getElementById("#" + data + "list a").click();
                                //jQuery.ready();
                            }

                        },
                        error: function (data) {
                            alert("error");
                        }
                    });


                    evt.preventDefault();
                }
            }
            
 //========================================== Function To delet project ============================================================
//===================================================================================================================================

            function deletfunction(proname, userid) {
                //  alert(proname);

                $('#' + proname + 'list').addClass('zoomOutDown animated', '5000');
                $('#' + proname + 'list').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
                    $(this).remove();
                });

                $.ajax({
                    type: 'POST',
                    url: '<?php echo base_url() . "tracker/deletprojectname" ?>',
                    dataType: 'json',
                    //data : data,
                    data: {"pronam": proname, "userid": userid},
                    success: function (data)
                    {
                        //      alert("after called");
                        location.reload();
                    //    func1();
                    },
                    error: function (data) {
                        //get the status code
                        alert("error occured while removing project");

                    }
                });
            }
 //========================================== Function To delet url ============================================================
//===================================================================================================================================

            function deleturlfunction(proid) {
                //  alert(proid);

                $.ajax({
                    type: 'POST',
                    url: '<?php echo base_url() . "tracker/deleturl" ?>',
                    dataType: 'json',
                    //data : data,
                    data: {"proid": proid},
                    success: function (data)
                    {
                        //======this coe reduce 1 number from url=========
                        var number = $("#numbur").html();
                            var e = parseInt(number) - 1;
                            $("#numbur").text(e);
                            //======above coe reduce 1 number from url=========
                        //   alert("after called");
                        $("#" + proid + "proid").remove();
                        $("#"+proid).remove();
                         $("." + proid + "abc").remove();
                       $('#botlinemngr').remove();
                       $('.dynamiccontkeyimg').remove();

      

var tbody = $("tbody#content12");

if (tbody.children().length == 0) {
   // alert('gotten');
   $('#urlheading').css('display','none')
   $('#nourlmsg').css('display','block')
}
else{
   // alert('not gotten');
}
         
       

    },
                            
                    error: function (data) {
                        alert("error occured while removing project");
                    }
                });
            }

 //========================================== Function is use to delet keyword from url =============================================
//===================================================================================================================================
           
            function deletkeywordfunction(keyid) {
                //   alert(keyid);

                $.ajax({
                    type: 'POST',
                    url: '<?php echo base_url() . "tracker/deletkeyword" ?>',
                    dataType: 'json',
                    //data : data,
                    data: {"keyid": keyid},
                    success: function (data)
                    {
                        //==============this code is to reduce the number of keyboad in dashboard============
                        var keywordnum = $("#numbkey").html();
                        var e1 = parseInt(keywordnum) - 1;
                        $("#numbkey").text(e1);
                        //==========code to reduce number of keyboars ends============
                        
                        //        alert("after called");
                        $("#" + keyid).remove();

                        //   $("#"+keyid).css("display", "none");
                    },
                    error: function (data) {
                        alert("error occured while removing project");
                    }
                });
            }

//=============this function is used to show keywords with their ranking and provide toggle facility to the keyword ranking container=============
//================================================================================================================================================
            function newnew(s) {
           
                //  alert(s);       this alert tells the projectid in which user currently clicked
                //   alert('newnew is called');
                $("#" + s).toggle(500);    //thi                   
                $("." + s + "abc").toggle(500);
                $("#botlinemngr").toggle(500);
                $.ajax({
                    type: 'POST',
                    url: '<?php echo base_url() . "tracker/fetchkeywordranking" ?>',
                    dataType: 'json',
                    //data : data,
                    data: {"prid": s},
                    success: function (data)
                    {
                        console.log(data);
                        var length = Object.keys(data).length;
                        // alert(length);// length contains the size of returned element
                        var q = data.length;
                        $("#" + s + "keynum").html('Keywords ( ' + q + ' )');
                        //alert(q);
                        //this code will check that keyrank should be inserted or not
                        if ($("." + s + "abc")[0]) {
//  alert("class exist");
                        }
                        else {
                            for (var m = 0; m < data.length; m++)
                            {
                                var q = data[m].keyword;
                                var i = data[m].ranking;
                                var cntry = data[m].country;
                                var keyid = data[m].keyword_id;
                                var pieces = i.split(/[\s,]+/);
                                var j = pieces[pieces.length - 1]
                                $('<tr id="' + keyid + '" class="' + s + 'abc"><td>' + q + '</td><td>' + j + '</td><td>'+cntry+'</td><td data-toggle="modal" data-target=".demo-graph"  id="' + s + 'graph" style="padding-bottom:55px;height:50px;" onclick="graph(' + keyid + ');">-</td><td><img src="<?php echo $b; ?>images/cross button image.png" style="display:inline;width:25px;height:25px;float:right;margin-right:20px;margin-top:6px;" alt="delet" onclick="deletkeywordfunction(' + keyid + ')"></td></tr>').insertAfter($("#" + s));
//alert("append is done");

//code to add the line graph in place of (-)
//var str = "1,2,3,4,5,6";
                                var temp = new Array();
                                temp = i.split(",");
                                for (var p = 0; p < temp.length; p++)
                                {
                                    temp[p] = parseInt(temp[p]);
                                }
                                console.log(temp);


                                var dps = new Array();

                                for (i = 0; i < temp.length; i++)
                                {
                                    dps.push({x: i, y: temp[i]});
                                }
//	var dps = [{x: 1, y: 12}, {x: 2, y: 15}, {x: 3, y: 18}, {x: 4, y: 12}, {x: 5, y: 17},{x: 6, y: 10}, {x: 7, y: 13}, {x: 8, y: 18}, {x: 9, y: 20}, {x: 10, y: 17}];   //dataPoints. 

                                var chart = new CanvasJS.Chart(s + "graph", {
                                    height: 55,
                                    width: 220,
                                    title: {
                                        text: ""
                                    },
                                    axisX: {
                                        interlacedColor: "#F0F8FF"
                                                //  margin: 50,
                                    },
                                    axisY: {
                                        margin: -5
                                    },
                                    data: [{
                                            type: "line",
                                            lineThickness: 1,
                                            dataPoints: dps
                                        }]
                                });

                                chart.render();

                            }
                        }
                        $("#reg-form").fadeOut(500).hide(function ()
                        {
                            $(".result").fadeIn(500).show(function ()
                            {
                                $(".result").html(data);
                            });
                        });

                    },
                    error: function (data) {
                        //get the status code
                        alert("error occured");

                    }
                });
            }
            
 //========================================== Function to open select box for advance search ========================================
//===================================================================================================================================
          
       function advancesearch(){
        $("#optv").val('');
        $("#optv").text('');
           $('a.chosen-single span').text('Select Your City');
             var valt=$('#multipleSelect').text();
             var valt1=$('#multipleSelect').val();
    //         alert(valt);
    //         alert(valt1);
         $("#advancesearch").toggle(500);
     //  alert('advance search called');
       }
 
        </script>           
  <style>
  </style>

    </head>
    <body>
        <div id="main-wrapper" class="container-fluid">

            <!-- Header Section -->
            <div class="row">
                <div class="main-header">  
                    <div class="col-xs-3 col-sm-3 col-md-3">
                        <a href="#" class="logo"><img src="<?php echo base_url(); ?>images/logo.png" alt="Formget Fugo"></a>
                    </div>
                    <div class="col-xs-9 col-sm-9 col-md-9">
                        <div>
                            <div class="user-nav">
                                <span class="wel-name">Welcome <?php echo $name; ?></span>
                                <div id="user-img">
                                <!--	<img src="<?php //echo base_url();       ?>images/user-img.png" alt="Profile Picture">
                                        <span class="arrow-down"></span>-->
                                    <div id="fg-dropdown">
                                        <div class="fg-dropdown-inner">
                                            <ul>
                                                <li><a href="dashboard.html" class="first">Dashboard</a></li>
                                                <li><a href="#">Account Settings</a></li>
                                                <li><a href="#">Become Affiliate</a></li>
                                                <li><a href="#" class="last">Extensions</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="fg-clear"></div>
                    <ul class="main-tab">
                        <li><a href="<?php echo base_url() . "tracker/dashboard"?>">Home</a></li>
                        <li><a href="<?php echo base_url() . 'tracker/accountfetch'?>">Account Settings</a></li>
                        <li id="nomargin"><a href="<?php echo base_url() . "tracker/logout" ?>">Logout</a></li>
                    </ul>
                    <div class="fg-clear"></div>
                </div>
            </div>

            <!-- Middle Section -->
            <div class="row  main-container blue-bg">
                <div class="col-md-2 nopadding">

                    <div id="sidebar">
                        <center><input type="submit" class="fg-btn green medium inline" value="add project" onclick="adproject();"></center><br />
                        <!-- Add New Project at Dashboard -->
                        <form id="projectform" action="<?php echo base_url() . "tracker/addproject" ?>">


                            <div class="fg-box first last">
                                <p class="fg-box-header rl-pad">Project Editor</p>
                                <div class="fg-inner-box rl-pad">
                                    <div class="fg-row first">
                                        <label class="block fg-label">Project Name</label>
                                        <input id="prnam" type="text" class="fg-input fg-fw text last" name="pname">
                                        <input id="userid" type="hidden"  name="userid" value="<?php echo $userid; ?>">
                                        <input id="username" type="hidden"  name="username" value="<?php //echo $username;     ?>">
                                        <p class="fg-help">Enter Project Name</p>
                                        <p id="projectexist" class="alertmessage"> Project Already Exist</p>
                                        <p id="projectempty" class="alertmessage"> Project Name  Can Not Be Empty</p>
                                    </div>
                                    <div class="fg-row last">
                                        <input id="check" type="submit" class="fg-btn green small inline" onclick="checkempty(event);" value="submit">
                                    </div>
                                </div>
                            </div>
                        </form>
                        
                        <?php
                        if ($projectname->result() !== '') {
                            echo '<a href="' . base_url() . 'tracker/dashboard/' . $userid . '"><h3 class="projectname12">Projects</h3></a>';
                        }
                        ?>
                        <ul class="all-options" style="margin-top:10px;">

                            <!-- code for making list of projects-->
                            <!-- we have passed the $project['projectname'] when  this page was called so we can use it -->
   <!--===================================list of project name============================================================================-->
 <!--=====================================================================================================================================-->
                            <?php
                            $prn = 1;
                            foreach ($projectname->result() as $row) {

                                if ($prn == 1) {
                                    $cpronm = $row->project_name;
                                    echo '<li id="' . $row->project_name . 'list"><a href="#mailget" class="formgetcall active" data-toggle="tab">' . $row->project_name . '</a></li>';
                                } else {
                                    echo '<li id="' . $row->project_name . 'list">'
                                    . '<a href="#mailget" class="formgetcall"  data-toggle="tab">' . $row->project_name . '</a>'
                                    . '</li>';
                                }
                                $numproject++;
                                $prn++;
                            }
                            ?>
                                                <!--  <li><a  href="#home" class="" data-toggle="tab"><i class="icon-arrow-right5"></i>Dashboard</a></li><hr>
                                                                  <li><a href="#mailget" data-toggle="tab"><i class="icon-arrow-right5"></i>mailget</a></li><hr>-->

                        </ul>							
                    </div>

                </div>

                <div class="col-md-10 nopadding ">

                    <div id="right-section-wrapper">

                        <div class="tab-content">

                           <div class="tab-pane active" id="mailget">
 <!--======================This code will show the statistics of projects in the of user account at the header=========================-->    
 <!--==================================================================================================================================-->

                                <div class="row all-stats" style="margin-bottom:40px;">
                                    <div class="col-md-4">
                                        <div class="message-stat new-messages">
                                            <span id="numbproj" class="stat sand"><?php echo $numproject; ?></span>
                                            <span class="block-title">Projects</span>
                                        </div>  
                                    </div>
                                    <?php
                                    $urls1 = 0;
                                    foreach ($urls->result() as $row) {
                                        $urls1++;
                                    }
                                    ?>
                                    <div class="col-md-4"> 
                                        <div class="message-stat on-hold">
                                            <!--this substrction is done due to remove one extra url for every project which will get added automatically-->
                                            <span id="numbur" class="stat"><?php echo $urls1 - $numproject; ?></span>
                                            <span class="block-title">Url</span>
                                        </div>
                                    </div>
                                    <?php
                                    $keyword2 = 0;
                                    foreach ($keyword1->result() as $row) {
                                        $keyword2++;
                                    }
                                    ?>
                                    <div class="col-md-4"> 
                                        <div class="message-stat">
                                            <span id="numbkey" class="stat"><?php echo $keyword2; ?></span>
                                            <span class="block-title">Keywords</span>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                </div> 

                                 <div id="wrongkeymessage" style="display:none">keyword already exist</div>
                                 
  <!--===================================below code is written to prevent displaying of div when there is no project============================-->
   <!--=========================================================================================================================================-->
                                <?php if ($prn > 1) { ?>
                                    <div id="dynamicurlcontainer" class="fg-box first last" style="display:block;" >
                                    <?php } else { ?>
                                        <div id="dynamicurlcontainer" class="fg-box first last" style="display:none;" >
                                        <?php } ?>
                                            <div class="fg-box-header rl-pad"><div class="row"><div class="col-md-10"><span id="addurlbutton"></span></div><div class="col-md-2"><button id="addurlbutt" type="button" style="" class="fg-btn blue medium inline" data-toggle="modal" data-target=".demo-popup" onclick="clearurlpopup();">add url</button><span id="deletprobut"></span></div></div></div>
                                        <div class="fg-inner-box rl-pad">
                                            <div class="fg-row last">
                                                <center><div id="projdetailloding"><img src="<?php echo base_url() ?>images/loding.GIF"></div></center>
                                                <table class="fg-table fg-fw">
                                                    <thead id="urlheading" style="">
                                                        <tr>
                                                            <th>Website</th>
                                                            <th>PageRank</th>
                                                            <th>Alexa Rank</th>
                                                            <th>
                                                                <!--     <button id="addurlbutt" type="button" class="fg-btn blue medium inline" data-toggle="modal" data-target=".demo-popup">add url</button>-->
                                                            </th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="content12" class="content123">
                                                        <!-- content dynamically comes here -->
                                                    <div id="nourlmsg" class="fg-callout blue">
                                                        <h2>Add Url First</h2>
                                                        <p>To Add Url Click On The Add Ur Button </p>

                                                    </div>


                                                    </tbody>
                                                </table>

                                            </div>
                                        </div>
                                    </div>

                                    <?php if ($prn < 2) { ?>
                                        <div class="fg-callout orange">
                                            <h2>First Add A Project</h2>
                                            <p><strong>Add Project:</strong> Click On The Add Project Button To Add a new Project</p>

                                        </div>
                                    <?php } ?>
                                </div>
                                <div id="clickme"></div>
                                <!-- dynamic content end-->

                            </div><!-- right-section-wrapper End -->
                        </div>
                    </div>
                </div>

                <!-- Footer Section -->
                <div class="row">
                    <div class="main-footer">
                        <div class="link-footer">
                            <ul>
                                <li><a href="#"></a></li>
                                <li><a href="#"></a></li>
                                <li><a href="#"></a></li> 
                                <li><a href="#"></a></li>
                                <li><a href="#"></a></li>
                                <li><a href="#"> </a></li>
                                <li><a href="#"></a></li>
                                <li><a href="#"></a></li>
                                <li><a href="#"></a></li>
                            </ul>
                        </div>
                        <div class="copyright-footer">
                            <p>2014 &copy; FormGet.com All rights reserved.</p>
                        </div>
                    </div>
                </div>
                  
  <!--========================================================Popup coding for url==============================================================-->
   <!--=========================================================================================================================================-->
                
                <!--popup coding for url-->
                <div id="popupcodeurl" class="modal fade demo-popup" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel-1" aria-hidden="true" >
                   <!--data-backdrop="static" data-keyboard="false"-->
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> <h3 class="modal-title">Add Url Here</h3>
                            </div>
                            <div class="modal-body">



 <div id="wrongentrymessage" style=""></div>
                                <div id="popupstyle" class="fg-box first">
                                    

                                    <p class="fg-box-header rl-pad">Your Url</p>
                                    <div class="fg-inner-box rl-pad">
                                       <form action='<?php echo base_url() . "tracker/addurl"; ?>' method="post"><!--<?php //echo base_url() . "tracker/addurl"; ?>-->
                                            <div class="fg-row last">
                                                <label class="block fg-label">Enter the url </label>
                                                 <input id="nurl" type="text" class="fg-input text fg-fw" name="nurl" autocomplete="off">
                                                <input type="hidden" id="hid1" class="fg-input text fg-fw" name="hid">
                                                <input id="userid2" type="hidden"  name="userid2" value="<?php echo $userid; ?>">
                                                <input id="username4" type="hidden"  name="username2" value="<?php //echo $username;     ?>">
                                                <p class="fg-help">eg:-http://www.example.com</p>
                                            </div>
                                            <input type="submit" class="fg-upload" id="submiturl" onclick="submiturl123(event);">
                                         
                                            <img id="addurlloding"  src='<?php echo base_url(); ?>images/loding.GIF'>
                                        </form>
                                    </div>

                                </div>




                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal-->

                <!-- popup coding for url ends-->

                
 <!--========================================================popup coding for keyword=========================================================-->
 <!--=========================================================================================================================================-->
             <div id="popupcodekey" class="modal fade demo-keyword" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel-1" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> <h3 class="modal-title">Add keyword Here</h3>
                            </div>
                            <div class="modal-body">
                                <div class="fg-inner-box rl-pad">
                                    <form action="<?php echo base_url() . "tracker/addkeyword" ?>" method="post">
                                        <div class="fg-row">
                                            <label class="block fg-label">Keyword</label>
                                            <input id="nkeyword" type="text" class="fg-input text fg-fw" name="nkeyword" autocomplete="off">
                                            <input id="hidproid" type="hidden" class="fg-input text fg-fw" name="hiproid">
                                            <input id="hidprourl" type="hidden" class="fg-input text fg-fw" name="hiprourl">
                                            <input id="userid1" type="hidden"  name="userid1" value="<?php echo $userid; ?>">
                                            <input id="username3" type="hidden"  name="username3" value="<?php //echo $username;     ?>">
                                            <p class="fg-help">enter a keyword</p>
                                        </div>
                                        <div class="fg-row">
                                            <label class="block fg-label">Country</label>
                                            <select id="counrtyname" class="fg-select fg-fw" name="countryname">
                                               <?php
                                                   foreach ($countryname->result() as $row) {
                                                  // echo $row->country_name;
                                                     echo '<option value="'.$row->country_code.'">'.$row->country_name.'</option>';
                                                   }
                                                 ?>
                                                <!--<option>USA</option>
                                                <option>India</option>-->
                                            </select>
                                            <p class="fg-help">select a country to search</p>
                                        </div>
                                          <div class="fg-row">
                                  <input id="advsearch" type="checkbox" onclick="advancesearch();">
                                  <label>Click Here For Advance Search</label>
                                     </div>
                                  <!--input with dropdown-->
<!-- /input-group -->
                                  
                                      <!--input with dropdown ends-->
                                      <div id="advancesearch" class="fg-row">
                            <label class="block fg-label">Select Your City</label>
                            <select id="multipleSelect" class="fg-select fg-fw chosen-select" name="citycode"  data-placeholder="Search your form..." style="display: none;">
                                <option id="optv" value="">myform</option>
                            </select>
<div class="chosen-container chosen-container-single chosen-with-drop" style="width: 100%;" title="" id="multipleSelect_chosen"><!--chosen-with-drop-->
<a class="chosen-single" tabindex="-1">
<span>Select Your City</span>
<div><b></b>
</div>
</a>
<div class="chosen-drop">
<div class="chosen-search">
<!--<input type="text" autocomplete="off">-->
<input type="text" id="keyword" tabindex="0" autocomplete="off">
</div>
                      <!--  <div id="ajax_response" style="position: absolute; left: 22.8%; width: 174px; height: 150px; overflow:scroll">-->
                                <div id="ajax_response" style=" max-height: 150px; overflow: scroll;">
      
      
  </div>
<!--<ul class="chosen-results">
<li class="active-result result-selected" style="" data-option-array-index="0">myform</li>
<li class="active-result" style="" data-option-array-index="1">myform</li>
</ul>-->
</div>
</div>
 </div>
                                      

                                      <input type="submit" class="fg-upload" id="submitkeyword" onclick="submitketword(event);">
                                    </form>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal-->
               </div> <!--popup code for keywords ends here-->

 <!--========================================================Popup coding for graph=========================================================-->
 <!--=========================================================================================================================================-->

                <div id="graphpopup" class="modal fade demo-graph" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel-1" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> <h3 class="modal-title">Statistics Of Keywords</h3>
                            </div>
                            <div class="modal-body">

                                <div id="chartdiv"></div>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal-->
          </div><!-- Main wrapper closing -->
                                                 
            <script>
                $(document).ready(function () {
                    $(".butn").click(function () {
                        alert("hidden called");
                        //     $(".keyrank").toggle(500);

                    });
                });

                //========== this function loads the details of first project while we reload the function==============
                //=====================================================================================================
                function func1() {
        
                    var pr = '<?php echo $cpronm; ?>';

                    var pro = '<?php echo $cpronm; ?>';
                 
                 
                    $('#hid1').val(pro);
                                        $(".content123").empty();

                    var uid = document.getElementById("userid").value;

                    //    $('#hid1').val(pro);
                    //    alert(pr+uid);
                    $("span#addurlbutton").text(pro);
                    $('#deletprobut').html('<img src="<?php echo base_url() ?>images/cross button image.png" style="margin-left:10px;display:inline;width:25px;height:25px;float:right;margin-right:10px;margin-top:6px;" onclick="deletfunction(' + '\'' + pr + '\'' + ',' + uid + ')">');
                    
                               //=============================below code used to show ajax image while loading content of project============================================       
           //===========================================================================================================================================       
            $('#urlheading').hide();
            $('#projdetailloding').show();
           $('#nourlmsg').css('display', 'none');
             
//============================= code to show ajax image while loading content of project ends============================================       
           //=========================================================================================================================================== 
                    function waitfunction(){ 
                        
                        $.ajax({
                        type: 'POST',
                        url: '<?php echo base_url() . "tracker/fetchwebcontent" ?>',
                        dataType: 'json',
                        //data : data,
                        data: {"id": pr, "usid": uid},
                        success: function (data)
                        {
                             $('#projdetailloding').hide();
                            //alert('after called');
                            console.log(data);

                            var q = data.length;// q is the length of number of result 
                            if (q > 1) {
                                $('#nourlmsg').css('display', 'none');
                                //   $('#urlheading').css('display','block');
                                  $('#urlheading').show();
                                    
                            }
                            else
                            {
                                $('#urlheading').css('display', 'none');
                                 $('#nourlmsg').css('display', 'block');
                            }

                            for (var m = 0; m < data.length; m++)
                            {
                                var q = data[m].project_links;
                                var i = data[m].page_rank;
                                var ar = data[m].alexa_rank;
                                var pid = data[m].project_id;
                                //below code is used to stop showing that entry doesn't contain url this will genrated when a new project is created
                                if (q.length == 0) {
                                }
                                else {
                                    $(".content123").append('<tr id="' + pid + 'proid" class="' + pid + '"><td onclick="newnew(' + pid + ');"><a>' + q + '</a></td><td>' + i + '</td><td>' + ar + '</td><td><div  class="fg-row last"> <button id="btn' + pid + '" type="button" class="fg-btn green medium inline" data-toggle="modal" data-target=".demo-keyword" onclick="newfunction(' + pid + ',\'' + q + '\')" value="' + pid + '">add keyword</button></div></td><td><img src="<?php echo base_url() ?>images/cross button image.png" style="display:inline;width:25px;height:25px;float:right;margin-right:20px;margin-top:6px;" alt="delet" onclick="deleturlfunction(' + pid + ')"></td></tr> <tr id="botlinemngr" style="display:none"></tr>    <tr style="display: none;background-color:#0E7AC4;color:white;" id="' + pid + '"><td id="' + pid + 'keynum">Keyword</td><td>Ranking</td><td>Country</td><td>Statistics</td><td></td></tr>');  //here we used /' at the time of passing parameter to sent q as a string because it will not get sent normaly
                                }
                            }

                            $("#reg-form").fadeOut(500).hide(function ()
                            {
                                $(".result").fadeIn(500).show(function ()
                                {
                                    $(".result").html(data);
                                });
                            });

                        }
                    });
                    };
   window.setTimeout( waitfunction, 700 ); // 5 seconds

                    
                }
                window.onload = func1;
            </script>     
            <script>
                $(document).ready(function () {
                    $('#btn').click(function () {
                        var val = $(this).attr('value');
                        alert(val);
                        alert("Hello");
                    });
                });
            </script>
            <script>
                function newfunction(a, b) {
                        $('#wrongkeymessage').css('display', 'none');
                    $('#nkeyword').val('');
                    $('#hidproid').val(a);
                    $('#hidprourl').val(b);
         //  the below code is used to maintain the functionality of select 
                   $('a.chosen-single span').text('Select Your City');
                     $("#optv").text('');
                      $("#optv").val('');
                      $( "#multipleSelect_chosen" ).removeClass( "chosen-with-drop" );
                       $("#advancesearch").hide();
                      // $("#advancesearch").uncheck();
                       $("#advsearch").prop( "checked", false );
          //  the code used to maintain the functionality of select ends
                }
                
 $( ".chosen-single" ).click(function() {
// $('#multipleSelect_chosen').addClass('');
 var ret=$( "#multipleSelect_chosen" ).hasClass( "chosen-with-drop" );
 //alert(ret);
 if(ret == 1)
 {
    // alert('true')
     $( "#multipleSelect_chosen" ).removeClass( "chosen-with-drop" );


 }
 else{
    // alert('false');
     $('#multipleSelect_chosen').addClass('chosen-with-drop');
      $( "#keyword" ).focus();
 }
});


            </script></body>
</html>