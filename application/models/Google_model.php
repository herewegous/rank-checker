<?php

class Google_Model extends CI_Model {

    public function adduser($name, $password, $emailid, $country) {

        //    if ($this->db->simple_query("INSERT INTO `user` (`name`,`username`,`password`, `emailid`, `country`) VALUES ('$name','$uname','$password', '$emailid', '$country');")) {
        if ($this->db->simple_query("INSERT INTO `user` (`name`,`password`, `emailid`, `country`) VALUES ('$name', '$password', '$emailid', '$country');")) {
            /*  $query2=$this->db->query("INSERT INTO `key_rank` (`project_id`, `ranking`) VALUES ('$proid', '$rank');"); */
            $b = "done";
        } else {
            $b = "fail";
        }
        //    $query = $this->db->insert('user', $data);
        //    echo $query;
        // echo $this->db->_error_message(); 
//echo $this->db->_error_number();
        return $b;
    }

    public function userlogin($username, $password) {
    $this->db->where('emailid', $username);
        $this->db->where('password', $password);
  $this->db->select('user_id,name');
        $query = $this->db->get('user');

        return $query;
    }

    public function fetchusname($userid) {
        $this->db->where('user_id', $userid);
        $this->db->select('name');
        $query = $this->db->get('user');
        return $query;
    }

    public function accountfetch($uid) {
        $this->db->where('user_id', $uid);
        $query = $this->db->get('user');
        return $query;
    }

    public function accountupdate($data, $userid) {
        $this->db->where('user_id', $userid);
        $this->db->update('user', $data);
        return;
    }

    public function forgotpassword1($emailaddress) {
        $this->db->where('emailid', $emailaddress);
        $this->db->select('password');
        $query = $this->db->get('user');
        return $query;
    }

    public function project_check($userid) {

        $this->db->where('user_id', $userid);
        $this->db->select('project_name');
        $query = $this->db->get('url_rank');
        return $query;
    }

    public function urlcheck($prona, $userid2, $url) {
        $this->db->where('project_name', $prona);
        $this->db->where('user_id', $userid2);
        //  $this->db->where('project_links',$url);
        $this->db->select('project_links');
        $query = $this->db->get('url_rank');
        return $query;
    }

    public function project_insert($data) {
        $this->db->insert('url_rank', $data);
    }

    public function ccaddedurl($userid2, $url, $prona) {
        $this->db->where('user_id', $userid2);
        $this->db->where('project_links', $url);
        $this->db->where('project_name', $prona);
        $query = $this->db->get('url_rank');
        return $query;
    }

    //=============This funcyion will fetch the projectname projects from database================================
    //=====================================================================================================
    public function project_names($userid) {
        $this->db->where('user_id', $userid);
        $this->db->distinct();
        $this->db->select('project_name');
        $query = $this->db->get('url_rank');

        // $query = $this->db->get('url_rank');

        return $query;
    }

    public function urls($userid) {
        $this->db->where('user_id', $userid);
        $this->db->select('project_links');
        $query = $this->db->get('url_rank');
        return $query;
    }

    public function keyword1($userid) {
        $this->db->where('user_id', $userid);
        $this->db->select('keyword');
        $query = $this->db->get('key_rank');
        return $query;
    }

    public function project_details($pron, $userid) {
        $query2 = $this->db->query("select * from url_rank where project_name='$pron' and user_id = '$userid'");
        return $query2;
    }

    public function keywordranking($proid, $rank, $keyword, $userid, $rankcheck, $countryname, $citycode) {
        //check that keyword for that perticular project and link already exist or not 
        $this->db->where('project_id', $proid);
        $this->db->where('keyword', $keyword);
        $this->db->select('keyword_id');
        $query = $this->db->get('key_rank');
        if ($query->result()) {
            $this->db->where('project_id', $proid);
            $this->db->where('keyword', $keyword);
            $this->db->select('keyword_id,ranking,date');
            $query = $this->db->get('key_rank');
            $r1 = $query->result();
//this will add latest ranking seprated by comma in the past lanking it\
            //   print_r($r1);
            $ran = $r1[0]->ranking;
            $key_id = $r1[0]->keyword_id;
            $ran = $ran . "," . $rank;
            $date = $r1[0]->date;
            $date = $date . "," . date('Y-m-d');
            // echo $ran;
            $data = array(
                'ranking' => $ran,
                'date' => $date,
                'mailcheck' => $rankcheck
            );


            $this->db->where('keyword_id', $key_id);
            $this->db->update('key_rank', $data);
        } else {
            $d = date('Y-m-d');
            //if keyword not exist then run it 
            if ($this->db->simple_query("INSERT INTO `key_rank` (`user_id`,`project_id`,`keyword`, `ranking`, `date`, `country`, `city`) VALUES ('$userid','$proid','$keyword', '$rank', '$d', '$countryname', '$citycode');")) {
                /*  $query2=$this->db->query("INSERT INTO `key_rank` (`project_id`, `ranking`) VALUES ('$proid', '$rank');"); */
                //   echo "it's done";
            }
            // echo "not available";
        }


        //otherwise run it
        /*     else
          {
          //this code fetch the old rank and add new rank separated by comma
          $this->db->where('project_id',$proid);
          $this->db->select('ranking');
          $query = $this->db->get('key_rank');
          $r1=$query->result();
          //this will add
          $ran=$r1[0]->ranking;
          //$ran=$ran.",".$rank;

          } */
    }

    public function ccaddedkeyword($projectid, $keyword, $userid) {
        $this->db->where('user_id', $userid);
        $this->db->where('project_id', $projectid);
        $this->db->where('keyword', $keyword);
        $query = $this->db->get('key_rank');
        return $query;
    }

    public function fetchrank($prid) {
        $this->db->where('project_id', $prid);
        $this->db->select('keyword,ranking,keyword_id,country');
        $query = $this->db->get('key_rank');
        return $query;
    }

    public function deletprojectname($proname, $userid) {
        $this->db->query("DELETE FROM key_rank WHERE project_ID IN (SELECT project_ID FROM url_rank where project_name='" . $proname . "' AND user_id='" . $userid . "') ;");
        $query = $this->db->query("DELETE FROM url_rank WHERE project_name='" . $proname . "' AND user_id='" . $userid . "' ;");
        return $query;
    }

    public function deleturl($proid) {
        // $this->db->query("DELETE FROM key_rank WHERE project_ID IN (SELECT project_ID FROM url_rank where project_name='".$proname."') ;");
        $query = $this->db->query("DELETE FROM url_rank WHERE project_id='" . $proid . "' ;");
        $query = $this->db->query("DELETE FROM key_rank WHERE project_id='" . $proid . "' ;");
        return $query;
    }

    public function deletkeyword($keyid) {
        // $this->db->query("DELETE FROM key_rank WHERE project_ID IN (SELECT project_ID FROM url_rank where project_name='".$proname."') ;");
        $query = $this->db->query("DELETE FROM key_rank WHERE keyword_id='" . $keyid . "' ;");
        return $query;
    }

    public function pnameandurl($userid) {

        $query = $this->db->query("SELECT project_links,project_id,project_name FROM url_rank WHERE user_id = '" . $userid . "' ;");
        return $query;
    }

    public function pidandkey($userid) {
        $query2 = $this->db->query("SELECT project_id,keyword FROM key_rank WHERE user_id ='" . $userid . "' ;");
        return $query2;
    }

    public function fetchemail() {
        $query3 = $this->db->query("SELECT emailid FROM user ;");
        return $query3;
    }

    public function graphdata($keyid1) {
        $this->db->where('keyword_id', $keyid1);
        $this->db->select('ranking,date');
        $query = $this->db->get('key_rank');
        return $query;
    }

    public function autoupdate() {
        $data = $this->db->get('key_rank');
        return $data;
    }

    //we fetch url here for auto update function to find out ranking of keyword for that url
    public function fetchurl($projid) {
        $this->db->where('project_id', $projid);
        $this->db->select('project_links');
        $url = $this->db->get('url_rank');
        return $url;
    }
    
    public function fetchkeyword($projectid){
         $this->db->where('project_id', $projectid);
        $this->db->select('keyword');
        $keyword = $this->db->get('key_rank');
        return $keyword;
        
    }

    public function fetchranking($projectid, $keyword) {
        $this->db->where('project_id', $projectid);
        $this->db->where('keyword', $keyword);
        $this->db->select('ranking');
        $rank = $this->db->get('key_rank');
        return $rank;
    }

    public function fetchemailproj($userid) {
        $this->db->where('user_id', $userid);
        $this->db->select('emailid');
        $email = $this->db->get('user');
        return $email;
    }

    public function fetchprojdetail($projectid) {
        $this->db->where('project_id', $projectid);
        $this->db->select('project_name,project_links');
        $query = $this->db->get('url_rank');
        return $query;
    }
    
    public function countryfetch(){
        $this->db->select('country_name,country_code');
        $query = $this->db->get('countryname');
        return $query;
    }
public function fetchsuggestion($keyword,$countryname){
  $this->db->select('*');
    $this->db->where('pcode', $countryname); 
       $this->db->like('pname', $keyword);
    //   $this->db->select('pbaseencode');
      $res = $this->db->get('citiesdata');
      return $res;
    
}
    
}
?>

