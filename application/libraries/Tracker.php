<?php

ini_set('max_execution_time', 3000);
require_once(APPPATH . 'libraries/pagerankgoogle.php');
include 'pagerankgoogle.php';
defined('BASEPATH') OR exit('No direct script access allowed');

class Tracker extends CI_Controller {

    public function __construct() {
        parent::__construct();
        // $this->load->model('form_model');
        $this->load->model('google_model');
    }

    public function index() {
        $this->dashboard();
    }

    public function keywordranking() {
        $this->load->library('curl');
//$this->curl->create('http://www.formget.com/');
        $this->curl->create("https://www.googleapis.com/customsearch/v1?key=AIzaSyAObAfpva8PtO5rNs5Iffs8ywylXHa34HA&cx=012557125435830414214:wuek1hiu0n8&q=dog");
        $this->curl->option('returntransfer', 1);
        $this->curl->option('SSL_VERIFYPEER', false); // For ssl site
//$this->curl->option('SSLVERSION', 3); // end ssl
        $data = $this->curl->execute();

        $rez = json_decode($data);
        $b = $rez->items[0]->link;
      //  echo $b . "<br>";
    }

//rank function strated from here
    public function login() {
        // $email['emailadd'] = $this->uri->segment(3);
        $indicate['sent'] = $this->uri->segment(3);
       //  $this->load->view('login', $indicate);
		 $this->load->view('index2', $indicate);
    }

    public function adduser() {
        $emailid = $this->input->post('email');
        $name = $this->input->post('name');
        // $uname = $this->input->post('uname');
        $password = $this->input->post('password');
        $country = $this->input->post('country');

        $data = array(
            'name' => $name,
            //        'username' => $uname,
            'password' => $password,
            'emailid' => $emailid,
            'country' => $country
        );

        //  $this->google_model->adduser($data);
        //  $data=$this->google_model->adduser($name,$uname,$password,$emailid,$country);
        $data = $this->google_model->adduser($name, $password, $emailid, $country);

        echo json_encode($data);
        //   echo $data;
    }

    public function userlogin() {

        // $pron=$this->input->post('id');
        $username = $this->input->post('id');
        $password = $this->input->post('pre');

        $data1 = $this->google_model->userlogin($username, $password);

        //   $data1=$this->google_model->project_details($pron);

        $rst = $data1->result_array();
        echo json_encode($rst);
    }

    public function hello123() {
        $projects['projectname'] = $this->google_model->project_names();

        //  echo "hiiiiiiiiiiiii";
        $this->load->view('ranktracker', $projects);
    }

    public function setsession() {
        // this function is used to establish the session of users
       // $userid = $this->uri->segment(3);
         $userid = $this->input->post('uid');
        //   $username = $this->uri->segment(4);
        $query = $this->google_model->fetchusname($userid);

        foreach ($query->result() as $row) {
            $username = $row->name;
        }

        $this->load->library('session');
        $this->load->library('encrypt');

        $newdata = array(
            'username' => $username,
             'userid' => $userid
        );

        $this->session->set_userdata($newdata);
        $session_id = $this->session->userdata('__ci_last_regenerate');
      //  echo $session_id;
        $username = $this->session->userdata('username');
    //    echo $username;
        $ret = $this->session->all_userdata();
   //     print_r($ret);
        //code to unset the session
//$this->session->unset_userdata('username');
   //     redirect('tracker/dashboard/' . $userid);
        //  $this->dashboard($userid,$username);
            $msg='hi';
        echo json_encode($msg);
    }

    //function to load home page
    public function dashboard() {

  //      $username = $this->uri->segment(4);
        //   $username = str_replace('%20', ' ', $username);
  //      $userid = $this->uri->segment(3);
//echo $userid;
//echo $username;
         $userid=$this->session->userdata('userid');
        $projects['projectname'] = $this->google_model->project_names($userid);
        $projects['urls'] = $this->google_model->urls($userid);
        $projects['keyword1'] = $this->google_model->keyword1($userid);

        $projects['pnameandurl'] = $this->google_model->pnameandurl($userid);
        $projects['pidandkey'] = $this->google_model->pidandkey($userid);
        $projects['uprofile'] = $this->google_model->accountfetch($userid);

        $projects['userid'] = $userid;
        //  $projects['username'] = $username;
//  $this->load->view('showbooks',$booksname);
        $this->load->view('ranktracker', $projects);
    }

    public function logout() {
        $this->session->unset_userdata('username');
        redirect('tracker/login');
    }

    public function accountfetch() {
       // $useid = $this->uri->segment(3);
        // $username = $this->uri->segment(4);
           $useid=$this->session->userdata('userid');
        // $username = str_replace('%20', ' ', $username);
        //  $userprofile['username'] = $username;
        $userprofile['userid'] = $useid;
        $userprofile['uprofile'] = $this->google_model->accountfetch($useid);
        $this->load->view('profile1', $userprofile);
    }

    public function accountupdate() {
      //  $userid = $this->uri->segment(3);
      //  $name = $this->input->post('name');
             $userid=$this->session->userdata('userid');
        $data = array(
            'name' => $this->input->post('name'),
            // 'username' => $this->input->post('uname'),
            'password' => $this->input->post('upass'),
            //    'emailid' => $this->input->post('emailid'),
            'country' => $this->input->post('country')
        );
        $this->google_model->accountupdate($data, $userid);
        redirect('tracker/accountfetch');
    }

    public function forgotpassword() {
        $this->load->view('forgotpassword');
    }

    public function checkemailaddress() {
        $data = $this->google_model->forgotpassword1($keyid);
    }

    public function forgotpassword1() {

        //   $emailaddress = $_POST['emailaddress'];
        $emailaddress = $this->input->post('emailaddress');
        //  echo $emailaddress;
        $query = $this->google_model->forgotpassword1($emailaddress);
        $sent = 0;
        foreach ($query->result() as $row) {
            $p = $row->password;
            $sent++;
        }


        if ($sent == 0) {
            $msg = 'notexist';
            //  echo $msg;
            echo json_encode($msg);
        } else {
            $address = $emailaddress;
            $this->load->library('email');
            $this->email->set_newline("\r\n");

            $this->email->clear(TRUE);
// Sender email address
            $this->email->from('formget.dev@gmail.com', 'Rank Tracker');
// Receiver email address
            $this->email->to($address);
// Subject of email
            $this->email->subject('Password Of Rank Tracker');
// Message in email

            $this->email->message("<b>Rank Tracker Account Details</b> <br><br>"
                    . "your password for rank tracker account :" . $p);

            if ($this->email->send()) {
//$data['message_display'] = 'Email Successfully Send !';

           //     echo 'message has been sent successfully';
            } else {
                $er = $this->email->print_debugger();
           //     echo $er;
             //   echo 'message has not been sent';
            }

            /*    require_once(APPPATH . 'libraries/PHPMailer-master/PHPMailerAutoload.php');
              $mail = new PHPMailer();
              $address = $emailaddress;
              $body = "<b>Rank Tracker Account Details</b> <br><br>"
              . "your password for rank tracker account :".$p;
              $mail->IsSMTP(); // telling the class to use SMTP
              $mail->Host = "smtp.gmail.com"; // SMTP server
              $mail->SMTPDebug = 1;                     // enables SMTP debug information (for testing)
              // 1 = errors and messages
              // 2 = messages only
              $mail->SMTPAuth = true;                  // enable SMTP authentication
              $mail->SMTPSecure = "ssl";                 // sets the prefix to the servier
              $mail->Host = "smtp.gmail.com";      // sets GMAIL as the SMTP server
              $mail->Port = 465;                    // set the SMTP port for the GMAIL server
              $mail->Username = "formget.dev@gmail.com";     // GMAIL username
              $mail->Password = "formgetmb12345";            // GMAIL password
              $mail->From = 'formget.dev@gmail.com';
              $mail->FromName = 'Formget RankTraker';
              $mail->Subject = "Password Details";
              $mail->AltBody = "To view the message, please use an HTML compatible email viewer!";     // optional, comment out and test
              $mail->MsgHTML($body);
              $mail->AddAddress($address);
              if (!$mail->Send()) {
              //     echo "Mailer Error: " . $mail->ErrorInfo;
              } else {
              //     echo "Message sent!";
              } */
        }
        //       redirect('tracker/login/'. base64_encode($emailaddress).'/sent');
    }

    public function pnameandurl($userid) {
        $query = $this->db->query("SELECT project_links,project_id,project_name FROM url_rank WHERE user_id = '" . $userid . "' ;");
        return $query;
    }

    //this function will fetch the projectid and keyword from the key_rank for perticular user
    public function pidandkey($userid) {
        $query2 = $this->db->query("SELECT project_id,keyword FROM key_rank WHERE user_id ='" . $userid . "' ;");
        return $query2;
    }

    //function to add project for apecific user   
    public function addproject() {
        $project = $this->input->post('prnam');
        $userid = $this->input->post('userid');
        //   $username = $this->input->post('username');
        //     $project = $_GET['pname'];
        //      $userid = $_GET['userid'];
        //       $username = $_GET['username'];
        $e = 0;
        $data1 = $this->google_model->project_check($userid);
        foreach ($data1->result() as $row) {
            $p = $row->project_name;
           
		   if (!strcasecmp($project, $p)) {
                //  echo "same";
             $e++;
            }
            //  else 
            //    echo "not exist";
        }

        $data = array(
            'project_name' => $project,
            'user_id' => $userid
        );
        if ($e == 0) {
            $this->google_model->project_insert($data);
            $msg1 = $project;
            echo json_encode($msg1);
        } else {
            $msg = "exist";
            echo json_encode($msg);
        }
        // below code will redirect you to main page where you added a project
    }

    public function fetchkeywordranking() {
        $projectid = $this->input->post('prid');
        //   $this->db->where('project_id', $projectid); 
        //  $this->db->select('keyword, ranking');
        // $query = $this->db->get('key_rank');

        $query = $this->google_model->fetchrank($projectid);
        $rst1 = $query->result_array();
        echo json_encode($rst1);
    }

// this fetches the whole entries of perticular project for perticular project
    // this function returns the json encode string of the array recived from the database 
    public function fetchwebcontent() {
        //   $pron=$_POST['proname'];
        $pron = $this->input->post('id');
        $userid = $this->input->post('usid');
        //$pron="anuj";
        $data = array(
            'a' => '5',
            'b' => '6',
            'c' => '7'
        );

        // echo $pron;
        //echo $data;
        // echo json_encode($data);

        $data1 = $this->google_model->project_details($pron, $userid);

        $rst = $data1->result_array();
        echo json_encode($rst);
        //         $rowcount = $data1->num_rows();
        //         echo json_encode($data);
        //  $rst1=$data1->result();
        // print_r($rst);
        //    foreach ($data1->result_array() as $row)
//{
        //        print_r($row);
        // echo $row->body;
//}
    }

    // this function will add the url in the perticular project after finding alexa rank and page rank
    public function addurl() {
require_once(APPPATH . 'libraries/pagerankgoogle.php');
        $prona = $this->input->post('hid');
        $url = $this->input->post('nurl');
        //    $url=strstr($url, 'www');
        //    $url = 'http://' . $url;
        $userid2 = $this->input->post('userid2');
        //  $username = $this->input->post('username2');
       
	        $churl=$url;
       $disallowed = array('https://', 'http://','www.');
        foreach ($disallowed as $d) {
            if (strpos($churl, $d) === 0) {
                $churl = str_replace($d, '', $churl);
            }
        }
		        $curl = curl_init();
curl_setopt_array( $curl, array(
    CURLOPT_HEADER => true,
    CURLOPT_NOBODY => true,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_URL => $churl ) );
$headers1 = explode( "\n", curl_exec( $curl ) );
$length= sizeof($headers1);

if ($length>1) {
//echo 'inner called';
if($headers1[0] == 'HTTP/1.1 404 Not Found') {
  $nv= 'notvalid';
echo json_encode($nv);
}
else {
    $check = 0;
        $disallowed = array('https://', 'http://','www.');
        foreach ($disallowed as $d) {
            if (strpos($url, $d) === 0) {
                $url = str_replace($d, '', $url);
            }
        }
        $url1 = rtrim($url, "/");
        //    echo $url1."<br>";
        //    echo $url."<br>";
        $urlcheck = $this->google_model->urlcheck($prona, $userid2, $url);


        foreach ($urlcheck->result() as $row) {
            $prlinks = $row->project_links;
            $disallowed = array('https://', 'http://','www.');
            foreach ($disallowed as $d) {
                if (strpos($prlinks, $d) === 0) {
                    $prlinks = str_replace($d, '', $prlinks);
                }
            }
            $prlinks = rtrim($prlinks, "/");
            //echo $prlinks;
            if (!strcasecmp($url1, $prlinks)) {
                //  echo "same";
                $check = 1;
            }
        }

        
        if ($check == 1) {
            $as = "ext";
            echo json_encode($as);
        } else {

            $xml = simplexml_load_file('http://data.alexa.com/data?cli=10&dat=snbamz&url=' . $url);
            $rank = isset($xml->SD[1]->POPULARITY) ? $xml->SD[1]->POPULARITY->attributes()->TEXT : 0;
           // $page_rank = new PageRank('https://www.facebook.com');
            $page_rank = new PageRank($url);
            $gpr = $page_rank->pr;
            $data = array(
                'project_name' => $prona,
                'project_links' => $url,
                'alexa_rank' => $rank,
                'page_rank' => $gpr,
                'user_id' => $userid2
            );
            $this->google_model->project_insert($data);


            $retm = $this->google_model->ccaddedurl($userid2, $url, $prona);

            $rst2 = $retm->result_array();
            echo json_encode($rst2);
        }
		}
}
else{
$nv = 'notvalid';
    echo json_encode($nv);
}
    }

    // this function works to find out the ranking of the website on a perticular keyword.....
    public function addkeyword() {
        //fetch ranking ehere keyword and projectid
$rst5='';
      //  $keyword = $_POST['nkeyword'];
      

        $keywordc = $_POST['nkeyword'];
        $keywordcc = explode(',', $keywordc);
       // print_r($keyword);
         $url1 = $_POST['hiprourl'];
    
         $disallowed = array('https://', 'http://','www.');
        foreach ($disallowed as $d) {
            if (strpos($url1, $d) === 0) {
                $url1 = str_replace($d, '', $url1);
            }
        }
        $url1 = rtrim($url1, "/");
        //  echo "modified url" . $url, "<br><br>";
        $b = $url1;
      
        $projectid = $_POST['hiproid'];
        $userid = $_POST['userid1'];
        $max = sizeof($keywordcc);
        for($vn=0;$vn<$max;$vn++){

            $keyword = $keywordcc[$vn];
			
			 $keyword = $keywordcc[$vn];
            if(strlen(preg_replace('/\s+/u','',$keyword)) == 0)
            {
                //echo 'variable not set';
            }
            else{
        
 $keyword1 = str_replace(' ', '+', $keyword);
     
$rank = $this->google_model->fetchranking($projectid, $keyword);
        foreach ($rank->result() as $row) {
            $rankdiff = $row->ranking;
            $rank1 = (int) substr($rankdiff, strrpos($rankdiff, ",") + 1);
        }


//=====code to get previous rank
        
        $rank = 0;
      
        //======code of simple ajax api-===========

        ob_start();

        $c = 0;
        for ($i = 1; $i < 55; $i + 8) {

            $referer = 'http://localhost/search_api_php/';
            $url = 'http://ajax.googleapis.com/ajax/services/search/web?v=1.0&rsz=8&cr=countryUS&start=' . $i . '&q=' . $keyword1;
            $ch = curl_init();
//	echo $url.'<br>';
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      
            curl_setopt($ch, CURLOPT_REFERER, $referer);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            $body = curl_exec($ch);
        
            $rez = json_decode($body);
            //  echo "results are:".$rez->responseData->results;
//print_r($rez);
            //  foreach ($rez->responseData->results as $key => $value) {
            if (empty($rez->responseData->results)) {
                //  echo "array not exist";
            } else {
                //   echo "array exist";
                for ($x = 0; $x < 8; $x++) {
                    $c = $c + 1;

                    if ($i == 1 && $x == 0) {
  //echo $b.$rez->responseData->results[0]->visibleUrl;
                    $visibleurl=$rez->responseData->results[0]->visibleUrl;
                    $disallowed = array('https://', 'http://','www.');
                    foreach ($disallowed as $d) {
                        if (strpos($visibleurl, $d) === 0) {
                            $visibleurl = str_replace($d, '', $visibleurl);
                        }
                    }
                    $visibleurl = rtrim($visibleurl, "/");
					
  
                        if (!strcmp($b, $visibleurl)) {
                            //                    echo '<b><i>strings are equal</b></i><br><br>';
                            //                  echo $b . $rez->responseData->results[0]->visibleUrl;
		//					echo $b.$visibleurl;
                           $rank = 1;
                            break 2;
                        }
                        //  }
                    }
//	if($rez->responseData->results[$x]->url){
      if (empty($rez->responseData->results[$x]->url)) {
                       // echo 'value not exist';
                    }
                    else{
	  
                    $ur = $rez->responseData->results[$x]->url;
                    $disallowed = array('https://', 'http://','www.');
                    foreach ($disallowed as $d) {
                        if (strpos($ur, $d) === 0) {
                            $ur = str_replace($d, '', $ur);
                        }
                    }
                    $ur = rtrim($ur, "/");
                    //echo "available";
                    //echo ($c).$rez->responseData->results[$x]->url ."<br></br>";
        //             	echo ($c).$ur.$b ."<br></br>";
//$b= $rez->responseData->results[$x]->url;
                    if (!strcmp($b, $ur)) {
//echo '<b><i>strings are equal</b></i><br><br>';
//echo $b . $rez->responseData->results[0]->visibleUrl;
                        // echo $c;
          //          echo 'ajax api called';
                        $rank = $c;
                        break 2;
                    }
					}

                    //}
                    //else{
//	echo "not available";
                    //  }
                    //print_r($rez);
                }
            }
            //    echo $key.' - '.count($value);
//}


            $i+=8;
        }
        //header("Location: http://www.formget.com");
        //======code of simple ajax api ends-===========
        //=========find rank using dom parser===============
//echo 'keyword is'.$keyword1.'<br><br>';
        if ($rank == 0) {
            require_once(APPPATH . 'libraries/simple_html_dom.php');
            //   require_once('simple_html_dom.php');
            $url = 'http://www.google.com/search?q=' . $keyword1 . '&start=0&num=100&cr=countryUS&gws_rd=ssl';
         //   echo $url."<br>";
//online+form+builder
            $html = file_get_html($url);

            $linkObjs = $html->find('h3.r a');
            $cont = 1;
            foreach ($linkObjs as $linkObj) {
                $title = trim($linkObj->plaintext);
                $link = trim($linkObj->href);

                // if it is not a direct link but url reference found inside it, then extract
                if (!preg_match('/^https?/', $link) && preg_match('/q=(.+)&amp;sa=/U', $link, $matches) && preg_match('/^https?/', $matches[1])) {
                    $link = $matches[1];
                } else if (!preg_match('/^https?/', $link)) { // skip if it is not a valid link
                    continue;
                }

                $disallowed = array('https://', 'http://','www.');
                foreach ($disallowed as $d) {
                    if (strpos($link, $d) === 0) {
                        $link = str_replace($d, '', $link);
                    }
                }
                $link = rtrim($link, "/");
//echo $link."<br>";

                if (!strcmp($b, $link)) {
                    //                  echo $b;
                    //                echo '<b><i>strings are equal</b></i><br><br>';
  //                  echo 'google search called';
                    $rank = $cont;
                }

                // echo $cont . $link .$b. '</p>';    
                $cont++;
            }
        }
        //====================rank using dom parser ends=============
        //===============this code fetch tha data from google paid api=============

        if ($rank == 0) {
            $this->load->library('curl');
            $this->curl->create("https://www.googleapis.com/customsearch/v1?key=AIzaSyAObAfpva8PtO5rNs5Iffs8ywylXHa34HA&cx=012557125435830414214:wuek1hiu0n8&cr=countryUS&q=" . $keyword1);
            $this->curl->option('returntransfer', 1);
            $this->curl->option('SSL_VERIFYPEER', false); // For ssl site

            $data = $this->curl->execute();
            $rez = json_decode($data);
            $cont = 1;
            $rank = 0;

            if (isset($rez->items[0])) {
                // echo "existccccc";
                for ($i = 0; $i < 10; $i++) {
				
				 if (empty($rez->items[$i]->link)) {
                        
          //              echo '3 rd api failed';
                    }
                    else{
				
                    $b1 = $rez->items[$i]->link;

                    $disallowed = array('https://', 'http://','www.');
                    foreach ($disallowed as $d) {
                        if (strpos($b1, $d) === 0) {
                            $b1 = str_replace($d, '', $b1);
                        }
                    }
                    $b1 = rtrim($b1, "/");

                    if ($b1 == $b) {
                        // echo "rank is" . $cont;
                        $rank = $cont;
                    }
                    $cont++;
					}
                }
            } else {
                //  echo "notexist";
            }
        }
        //================paid api code ends===========


        if ($rank == 0) {
            //  $rank = '-';
        }

        $rankcheck = 0;
        //=======this code will check that rank is increases or decrease=========

        if (isset($rank1)) {
            if ($rank == $rank1) {
                //  echo 'rank is same';
                $rankcheck = 0;
            } else if ($rank < $rank1) {
                //    echo 'ranking is down';
                $rankcheck = 1;
            } else {
                //    echo 'rank is up';
                $rankcheck = 2;
            }
        }

                    //=======this code to check that rank is increases or decrease ends=========
       //    echo $rank . "<br>";
       //    echo $keyword . "<br>";
       //    echo $userid . "<br>";  
    
        $this->google_model->keywordranking($projectid, $rank, $keyword, $userid, $rankcheck);
        
      $retm2[] = $this->google_model->ccaddedkeyword($projectid, $keyword, $userid);
       //  $rst3 = $retm2->result_array();
       //  $rst5+= json_encode($rst3);
       //  echo 'hirtrt';
       //    echo $rst5;
	   }
         }
     
         $max1 = sizeof($retm2);
      for($ret=0; $ret<$max1; $ret++)
         {
        $retm6[] = $retm2[$ret]->result_array();
         }
        //echo json_encode($rst3);
         echo  json_encode($retm6);
// redirect('http://localhost/rankproject/index.php/tracker/dashboard/' . $userid . '/' . $username3);
    }

    //function to delet a project for specific project for specific user
    public function deletprojectname() {
        $projectname = $this->input->post('pronam');
        $userid = $this->input->post('userid');

        $this->google_model->deletprojectname($projectname, $userid);
        $data = "hiiiiiiiiiiiiii";

        echo json_encode($data);
    }

    // function to delet a url from a project 
    public function deleturl() {
        $projectid3 = $this->input->post('proid');
        $this->google_model->deleturl($projectid3);
        $data = "hiiiiiiiiiiiiii";

        echo json_encode($data);
    }

    //function to delet a specific keyword from database
    public function deletkeyword() {
        $keyid = $this->input->post('keyid');
        $this->google_model->deletkeyword($keyid);
        $data = "hiiiiiiiiiiiiii";
        echo json_encode($data);
    }

    public function fetchemail() {
        // $email = $this->input->post('email');
        $data2 = $this->google_model->fetchemail();
        $rst = $data2->result_array();
        //  $rst="hiiiiii";
        echo json_encode($rst);
    }

    public function graphdata() {
        $keyid = $this->input->post('keyid');
        $data = $this->google_model->graphdata($keyid);
        $data1 = $data->result_array();
        // $data1='hiiiiiiiii';  

        echo json_encode($data1);
    }

    public function autoupdate() {

        $data = $this->google_model->autoupdate();
            foreach ($data->result() as $row) {
            $keyword = $row->keyword;
            $keyword1 = str_replace(' ', '+', $keyword);
            $userid = $row->user_id;
            $projectid = $row->project_id;
            $rankdiff = $row->ranking;
            $rank1 = (int) substr($rankdiff, strrpos($rankdiff, ",") + 1);  //this is used to find out the difference between current ranking and previous ranking
            $url1 = $this->google_model->fetchurl($projectid);
            $url2 = $url1->result();
            $url = $url2[0]->project_links;
            //           echo $keyword.$url."<br>";
            $rank = 0;
            $disallowed = array('https://', 'http://','www.');
            foreach ($disallowed as $d) {
                if (strpos($url, $d) === 0) {
                    $url = str_replace($d, '', $url);
                }
            }
            $url = rtrim($url, "/");
            //  echo "modified url" . $url, "<br><br>";
            $b = $url;

            //======code of simple ajax api-===========

            ob_start();

            $c = 0;
            for ($i = 1; $i < 55; $i + 8) {

                $referer = 'http://localhost/search_api_php/';
                $url = 'http://ajax.googleapis.com/ajax/services/search/web?v=1.0&cr=countryUS&rsz=8&start=' . $i . '&q=' . $keyword1;
                $ch = curl_init();
//	echo $url.'<br>';
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                //curl_setopt($ch, CURLOPT_HEADER, 0);
                // note that the referer *must* be set
                curl_setopt($ch, CURLOPT_REFERER, $referer);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                $body = curl_exec($ch);
                $rez = json_decode($body);
                //  echo "results are:".$rez->responseData->results;
//print_r($rez);
                //  foreach ($rez->responseData->results as $key => $value) {
                if (empty($rez->responseData->results)) {
                    //  echo "array not exist";
                } else {
                    //   echo "array exist";
                    for ($x = 0; $x < 8; $x++) {
                        $c = $c + 1;

                        if ($i == 1 && $x == 0) {
						$visibleurl=$rez->responseData->results[0]->visibleUrl;
						 $disallowed = array('https://', 'http://','www.');
                        foreach ($disallowed as $d) {
                            if (strpos($visibleurl, $d) === 0) {
                                $visibleurl = str_replace($d, '', $visibleurl);
                            }
                        }
                        $visibleurl = rtrim($visibleurl, "/");

                            if (!strcmp($b, $visibleurl)) {
                                //                    echo '<b><i>strings are equal</b></i><br><br>';
                                //                  echo $b . $rez->responseData->results[0]->visibleUrl;
                                $rank = 1;
                                            // echo 'google ajax api runs';
                                break 2;
                            }
                            //  }
                        }
//	if($rez->responseData->results[$x]->url){
                        $ur = $rez->responseData->results[$x]->url;
                        $disallowed = array('https://', 'http://','www.');
                        foreach ($disallowed as $d) {
                            if (strpos($ur, $d) === 0) {
                                $ur = str_replace($d, '', $ur);
                            }
                        }
                        $ur = rtrim($ur, "/");
                        //echo "available";
                        //echo ($c).$rez->responseData->results[$x]->url ."<br></br>";
                        // 	echo ($c).$ur.$b ."<br></br>";
//$b= $rez->responseData->results[$x]->url;
                        if (!strcmp($b, $ur)) {
//echo '<b><i>strings are equal</b></i><br><br>';
//echo $b . $rez->responseData->results[0]->visibleUrl;
                            // echo $c;  
                              echo 'simple ajax api code runs';
                            $rank = $c;

                            break 2;
                        }
                    }
                }



                $i+=8;
            }
            //header("Location: http://www.formget.com");
            //======code of simple ajax api ends-===========
            //=========find rank using dom parser===============

            if ($rank == 0) {
                require_once(APPPATH . 'libraries/simple_html_dom.php');
                //   require_once('simple_html_dom.php');
                $url = 'http://www.google.com/search?q=' . $keyword1 . '&start=0&num=100&gws_rd=ssl&cr=countryUS';
//online+form+builder
                $html = file_get_html($url);

                $linkObjs = $html->find('h3.r a');
                $cont = 1;
                foreach ($linkObjs as $linkObj) {
                    $title = trim($linkObj->plaintext);
                    $link = trim($linkObj->href);

                    // if it is not a direct link but url reference found inside it, then extract
                    if (!preg_match('/^https?/', $link) && preg_match('/q=(.+)&amp;sa=/U', $link, $matches) && preg_match('/^https?/', $matches[1])) {
                        $link = $matches[1];
                    } else if (!preg_match('/^https?/', $link)) { // skip if it is not a valid link
                        continue;
                    }

                    $disallowed = array('https://', 'http://','www.');
                    foreach ($disallowed as $d) {
                        if (strpos($link, $d) === 0) {
                            $link = str_replace($d, '', $link);
                        }
                    }
                    $link = rtrim($link, "/");
//echo $link."<br>";

                    if (!strcmp($b, $link)) {

                        $rank = $cont;
                            //         echo 'dom structure code runs';
                    }

                    // echo $cont . $link .$b. '</p>';    
                    $cont++;
                }
            }
            //====================rank using dom parser ends=============
            //===============this code fetch tha data from google paid api=============

            if ($rank == 0) {
                $this->load->library('curl');
                $this->curl->create("https://www.googleapis.com/customsearch/v1?key=AIzaSyAObAfpva8PtO5rNs5Iffs8ywylXHa34HA&cx=012557125435830414214:wuek1hiu0n8&q=" . $keyword1);
                $this->curl->option('returntransfer', 1);
                $this->curl->option('SSL_VERIFYPEER', false); // For ssl site

                $data = $this->curl->execute();
                $rez = json_decode($data);
                $cont = 1;
                $rank = 0;

                if (isset($rez->items[0])) {
                    // echo "existccccc";
                    for ($i = 0; $i < 10; $i++) {
                        $b1 = $rez->items[$i]->link;

                        $disallowed = array('https://', 'http://','www.');
                        foreach ($disallowed as $d) {
                            if (strpos($b1, $d) === 0) {
                                $b1 = str_replace($d, '', $b1);
                            }
                        }
                        $b1 = rtrim($b1, "/");

                        if ($b1 == $b) {
                            // echo "rank is" . $cont;
                            $rank = $cont;
                                    //     echo 'google paid api fetching data';
                        }
                        $cont++;
                    }
                } else {
                    //  echo "notexist";
                }
            }
            //================paid api code ends===========

            if ($rank == 0) {
                //  $rank = '-';
            }

            $rankcheck = 0;
//      echo "last rank of keyword is".$rank1;

            if (isset($rank1)) {
                if ($rank == $rank1) {
//            echo 'rank is same';
                    $rankcheck = 0;
                } else if ($rank < $rank1) {
//            echo 'ranking is down';
                    $rankcheck = 1;
                } else {
//            echo 'rank is up';
                    $rankcheck = 2;
                }
            }
            //  echo gettype($rank1);
//        echo $rankcheck;
//     echo "<br>".$keyword;
//     echo $rank;
            // echo $userid;
            $this->google_model->keywordranking($projectid, $rank, $keyword, $userid, $rankcheck);
        }


//fetch one entry and get the value of project_id find the url for correspondin proj id
        //now we get the value of url and keyword find the ranking and update the value
    }

    public function autosendmails() {
        $data = $this->google_model->autoupdate();
        foreach ($data->result() as $row) {
            $mailcheck = $row->mailcheck;
            $keyword = $row->keyword;
            $userid = $row->user_id;
            $projectid = $row->project_id;
            //echo $mailcheck.$keyword.$userid."<br /><br />";
            if ($mailcheck == 1 || $mailcheck == 2) {
                //  echo " email called";

                $email = $this->google_model->fetchemailproj($userid);
                $projectdetails = $this->google_model->fetchprojdetail($projectid);
                foreach ($email->result() as $row) {
                    $address = $row->emailid;
                    //    echo $address;
                }
                foreach ($projectdetails->result() as $row) {
                    $l = $row->project_links . "<br>";
                    $pn = $row->project_name . "<br>";
                }


        $config['protocol'] = 'smtp';
$config['smtp_host'] = 'smtp.gmail.com';
$config['smtp_port'] = 465;
$config['smtp_user'] = "formget.dev@gmail.com";
$config['smtp_pass'] = "formgetmb12345";
// Load email library and passing configured values to email library
                 $this->load->library('email', $config);
                $this->load->library('email');
                $this->email->set_newline("\r\n");

                $this->email->clear(TRUE);
// Sender email address
                $this->email->from('formget.dev@gmail.com', 'Rank Tracker');
// Receiver email address
                $this->email->to($address);
// Subject of email
                $this->email->subject('Current Ranking Status Of Your Keyword');
				  $this->email->set_mailtype("html");
// Message in email
                $this->email->message('<html>
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Single Extention </title>
    </head>
    <body style="font-size: 45px;font-family: proxima_nova_rgregular, sans-serif;font-size: 16px;line-height: 28px;">
        <!-- Middle Section -->
        <div align="center" style="border:1px,red,solid;">
        <div align="center" id="padd" style="width:815px;border-style: solid;border-color: #D5D5D5;padding:30px;background-color:#F2F2F2;" >
			<div style=""><img src="https://ci5.googleusercontent.com/proxy/uQRkR4RIRhQ7LPTHiI4BHEaGun2LR1WJRBOVFkRovNdPC55ysLASrCmwaf-aSXsv4M4B51-4IFjQmIvRCFPHxI4PLAvgL_oWkaeZJFZwEHrprw8P-UiZIfpFFy9u18SxCT0cLwFOJ4ffmjJScFPgClc=s0-d-e1-ft#http://www.formget.com/mailget/upload_files/1431944509-1601297505-Formget_welcome_email_png"/></div>
          <div  id="division1" style="width:585px;border-style: solid;border-color: #D5D5D5;background-color:#ffffff;" align="center">
		   <h2>Keyword Ranking Report</h2>
           <div class="extension-desc" align="center">
		    <p style="margin:0 15px;">Your Keyword:-' . $keyword . '</p>
            <p style="margin:0 15px;">Associated With Url:-' . $l . '</p>
			<p style="margin:0 15px;">And Project:-' . $pn . '</p>
		  has been changed its ranking open your account to watch changes
			   	<p><b>For login click <a href="http://aorank.com/rank-checker">here</a></b></p>
           </div>
	</div>
</div>
</div>
    </body>
</html>');

//'Your "' . $keyword . '" keyword of "' . $pn . '"project and "' . $l . '" link has changed its ranking '

                if ($this->email->send()) {
//    echo 'message has been sent successfully';
                } else {
                    //   $er=$this->email->print_debugger();
                    //   echo $er;
                    //   echo 'message has not been sent';
                }



                /*        require_once(APPPATH . 'libraries/PHPMailer-master/PHPMailerAutoload.php');
                  $mail = new PHPMailer();
                  // $address='anujtiwari1375@gmail.com';
                  $body = "<b>Your Keyword Statistics</b> <br><br>"
                  . "your keywords gone up or down".$keyword;
                  $mail->IsSMTP(); // telling the class to use SMTP
                  $mail->Host = "smtp.gmail.com"; // SMTP server
                  $mail->SMTPDebug = 1;                     // enables SMTP debug information (for testing)
                  // 1 = errors and messages
                  // 2 = messages only
                  $mail->SMTPAuth = true;                  // enable SMTP authentication
                  $mail->SMTPSecure = "ssl";                 // sets the prefix to the servier
                  $mail->Host = "smtp.gmail.com";      // sets GMAIL as the SMTP server
                  $mail->Port = 465;                    // set the SMTP port for the GMAIL server
                  $mail->Username = "formget.dev@gmail.com";     // GMAIL username
                  $mail->Password = "formgetmb12345";            // GMAIL password
                  $mail->From = 'formget.dev@gmail.com';
                  $mail->FromName = 'Formget RankTraker';
                  $mail->Subject = "Password Details";
                  $mail->AltBody = "To view the message, please use an HTML compatible email viewer!";     // optional, comment out and test
                  $mail->MsgHTML($body);
                  $mail->AddAddress($address);
                  if (!$mail->Send()) {
                  //  echo "Mailer Error: " . $mail->ErrorInfo;
                  } else {
                  //  echo "Message sent!";
                  } */
            }
        }
    }

}
