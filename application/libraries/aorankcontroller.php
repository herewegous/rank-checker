<?php

error_reporting(0);

class Aorankcontroller extends CI_Controller {

// storing curl file in a variable 
    private $curl_html_page = '';
    // Converting to HTML Dom form Curl
    private $html = '';
    // Fetching title form dom
    private $title = '';
    // Fetching Description form dom
    private $meta_description = '';
    // Fetching Keywords form dom
    private $meta_keywords = '';
    // Fetching body content form dom
    private $body_content = '';
    // Fetching title form dom
    // img_ alt_ text//
    private $img_alt = ' ';
    private $stop_words = '';
    //heading
    private $heading_h1 = '';
    private $heading_h2 = '';
    private $heading_h3 = '';
    private $heading_h4 = '';
    private $heading_h5 = '';
    private $heading_h6 = '';
    private $link = array();
    private $time_start = 0;
    private $time_end = 0;
    private $time = 0;

    public function __construct() {
        parent::__construct();
        $this->load->library("pr");
        $this->load->library("google_backlinks");
        $this->load->library("redirect_url");
        $this->load->library("alexa");
        $this->load->library("domainage");
        $this->load->library("whois");
        $this->load->library('dom_parser');
        $this->load->library('geoplugin.php');
        $this->load->library('html_dom.php');
        $this->load->library("curl");
        $this->load->model('aorankmodel');
        $this->time_start = microtime(true);
    }

    public function __destruct() {
        $this->time_end = microtime(true);
        $this->time = $this->time_end - $this->time_start;
        //echo "Loaded in $this->time seconds\n";
    }

    public function index() {
        $result = $this->aorankmodel->fetch_links_search();
        $numrows = $result[0];
        $data = $result[1];
        $x = 0;
        $filter_links = array();
        foreach ($data as $web_data) {


            $screen_image = $web_data['aorank_ajax_screen_img'];
            if ($screen_image != '') {

                $img_result = $this->getstatus($screen_image);

                if (strpos($img_result, '200') !== false) {


                    if ($x < 5) {

                        $filter_links[$x] = $web_data;
                        $x++;
                    } else {
                        break;
                    }
                }
                /* else{
                  echo "<h3>".$screen_image."</h3><br>";
                  } */
            }
        }

        $linksdata['data_present'] = $numrows;
        $linksdata['data'] = $filter_links;

        $this->load->view('search', $linksdata);
    }

    public function curl($url) {

        $this->curl->create($url);
        $this->curl->option('buffersize', 10);
        $this->curl->option('useragent', 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.8) Gecko/20100722 Firefox/3.6.8                (.NET CLR 3.5.30729)');
        $this->curl->option('returntransfer', 1);
        $this->curl->option('followlocation', 1);
        $this->curl->option('HEADER', true);
        $this->curl->option('ENCODING', 'gzip');
        $this->curl->option('connecttimeout', 600);
        $this->curl->option('SSL_VERIFYPEER', false);
        $this->curl->option('SSL_VERIFYHOST', false);
//        $this->curl->option('CURLOPT_RETURNTRANSFER', true);
//        $this->curl->option('CURLOPT_FOLLOWLOCATION', true);
        // $this->curl->option(CURLOPT_ENCODING ,'gzip');
        $this->curl->option('header', true);
        $descarga = utf8_encode($this->curl->execute());
        $this->curl->close();
        return $descarga;
    }

    public function analytics() {
	
	
        $url = '';
        if ($this->input->post('url')) {
            $url = $this->input->post('url');
        } else {
            // $url = $this->uri->segment(4);
            // $url = str_replace('_',' ',$url);
            // $url = str_replace(array('%2F','%5C'),array('%252F','%255C'), urldecode($url));
            //  $url = str_replace(array('%2F','%5C'), array('%252F','%255C'), urlencode($url));
            // echo $url;


            $server_uri = $_SERVER['REQUEST_URI'];

            // echo $server_uri."<br><br>";

            $tokens = explode("http", $server_uri);
            //  echo $tokens[0]."<br>";

            $url = str_replace($tokens[0], "", $server_uri);
            // echo $url;
        }

        function get_domain($input) {
            $pieces = parse_url($input);

            if (isset($pieces['host'])) {
                return $pieces['host'];
            } else {
                return $pieces['path'];
            }
        }

        function get_scheme($input) {
            $pieces = parse_url($input);

            if (isset($pieces['scheme'])) {
                return $pieces['scheme'];
            } else {
                return $pieces['path'];
            }
        }

        $url1 = get_domain($url);
        // url validation
        if (strpos($url1, 'www.') !== false) {
            $url1 = str_replace("www.", "", "$url1");
            $url1 = "http://www." . $url1;
        } else {
            $url1 = "http://www." . $url1;
        }
        $scheme = get_scheme($url1);
        $host = get_domain($url1);

        $url1 = $scheme . "://" . $host;


        // url validation



        $url = rtrim($url, "/"); //cut / from last index of url
        // url validation
        if (strpos($url, 'www.') !== false) {
            
        } else {
            $url = "www." . $url;
        }
        if (strpos($url, 'http://') !== false) {
            
        } else {
            $url = "http://" . $url;
        }
        if (strpos($url, 'www.http://') !== false) {
            $url = str_replace("www.http://", "", "$url");
            $url = "http://www." . $url;
        }
        if (strpos($url, 'http://https://www.') !== false) {
            $url = str_replace("http://https://www.", "", "$url");
            $url = "https://www." . $url;
        }

        if (strpos($url, 'http://www.https://') !== false) {
            $url = str_replace("http://www.https://", "", "$url");
            $url = "https://www." . $url;
        }



        /*
         * Calling Curl library to load page html
         */
        $this->curl_html_page = $this->curl($url);
        $this->html = $this->dom_parser->str_get_html($this->curl_html_page);
        //  var_dump( $this->html );
        $input_url = trim($url, '/');

// If scheme not included, prepend it
        if (!preg_match('#^http(s)?://#', $input_url)) {
            $input_url = 'http://' . $input_url;
        }

        $urlParts = parse_url($input_url);

// remove www
        $Domain = preg_replace('/^www\./', '', $urlParts['host']);


        $input_url1 = trim($url1, '/');

// If scheme not included, prepend it
        if (!preg_match('#^http(s)?://#', $input_url1)) {
            $input_url1 = 'http://' . $input_url1;
        }
        $urlParts = parse_url($input_url1);
// remove www
        $Domain1 = preg_replace('/^www\./', '', $urlParts['host']);



        //  echo "url==".$url."<br><hr>";
        //echo "Domain==".$Domain."<br><hr>";
        //echo "url1==".$url1."<br><hr>";
        //echo "Domain1==".$Domain1."<br><hr>";
        //url page exist or not ..

        if (!empty($this->html)) {
            // if url page exist then  page exist in database or not    

            $fetch_data = $this->aorankmodel->aorank_fetch_data($url);
            $url_exist = count($fetch_data);
            $rescan = $this->input->post('rescan');
            if (!($url_exist == 1) || ($rescan == "rescan")) {

                // if url page not in database or rescan then ..  
                //   Insert   and update  logic  

                $url;
// 0 (url)  string form

                $data['body'] = $this->get_body_content($this->html);
                $data['get_url'] = $this->get_url($url); //display http://www.formget.com
                $data['screen'] = $Domain; //display formget.com
                $data['whois_info'] = $Domain1; //display  formget.com 
                $data['domainage'] = $url1;  ///display  https://www.formget.com
                $data['page_ip'] = $this->get_page_ip($Domain1); //array  ip,city,country ..      
                $ip = $data['page_ip']['no_of_ip'];
                $city = $data['page_ip']['city'];
                $country = $data['page_ip']['Country'];
                $ip_city_country = "$ip###$city###$country";
                $data['title'] = $this->gettitle($this->html); //array title ,title count , title tag relevancy 1(title data ) array format
                $title_json = json_encode($data['title']);   //.   1(title data ) json format
                $data['description'] = $this->get_meta_description($this->html); // description and length...  2 (description and length) array format
                $description_json = json_encode($data['description']);  // 2 (description and length) json format
                $data['meta_keywords'] = $this->get_meta_keywords($this->html); // meta keywords...    3( meta keywords ) array format
                $meta_keywords_json = json_encode($data['meta_keywords']); // 3( meta keywords ) json format
                $data['meta_robots'] = $this->meta_robots($this->html); // ????  
                $title_return = implode(' ', $this->title); //display title of page .
                $data['get_related_link'] = array($title_return, $url); // contains title and url .. 
                $data['unique_title'] = $title_return;   //contains title  
                $data['heading'] = $this->get_heading($this->html); // contains heading count ...   5 (heading count)
                $data['heading_innertext'] = $this->get_heading_details($this->html); //heading innertext array .. 5(heading count inner text arry) 
                $heading_data_array[0] = $data['heading'];
                $heading_data_array[1] = $data['heading_innertext'];
                $heading_data_json = json_encode($heading_data_array); //  5 heading data json format 
                $data['Keywords_cloud'] = $this->get_keyword_cloud($this->html);   // 6 focused keyword array.. 6(focused keyword) array format
                $focused_keyword_json = json_encode($data['Keywords_cloud']);   // 6 focused keyword array.. 6(focused keyword)  json format
                $data['keywords_consistency'] = $this->get_keyword($this->html);  // 7 keyword consistency.. array format 
                $keywords_consistency_json = json_encode($data['keywords_consistency']);  // 7 keyword consistency.. json format 
                $data['words'] = $this->get_no_words($this->html); // total words , unique words ..{content length}   4(total words,unique words) array format
                $words_json = json_encode($data['words']); //  4 content length (total words,unique words) json format
                $data['image'] = $this->getimage($this->html); // 8 image data .. array format
                $image_data_json = json_encode($data['image']); // 8 image  data .. json format
                $data['get_missing_alt'] = $this->get_missing_alt($this->html);
                $data['get_text_ratio'] = $this->get_text_ratio($this->curl_html_page); // 9 text html ratio string form 
                $data['index_page'] = $this->index_page($url); //10 page index in google ..string form
                $data['google_preview'] = $this->google_preview($url, $this->html); //11 google preview data in array 
                $google_preview_json = json_encode($data['google_preview']); // 11 googlel preview data in json .
                $data['blinks'] = $this->back_links_juice($url); //12 page links data in array format 
                $blinks_page_links = json_encode($data['blinks']); //12 page links data in json format
                $data['link_status'] = $this->link;   // 29   related to page links..
                $link_status = json_encode($data['link_status']); //29 link status json ..
                $data['GoogleBL'] = $this->GoogleBL($url); // 13 page backlinks string . 
                $data['robots'] = $url1;
                $data['xmlsitemap'] = $url1;
                $data['url_rewrite'] = $this->url_rewrite($url); // 15 url rewrite data in true /  false
                $data['url_underscores'] = $this->url_underscores($url); // 16 url underscore ... string
                $data['url_char_count'] = $this->url_char_count($url); // 17 domain length .. number
                $data['favicon'] = $this->get_favicon($this->html, $Domain1); //18 favicon ...
                $data['checkurl'] = $this->checkurl($url); // 19 check 404 page in website ..
                $data['conversion_forms'] = $this->conversion_forms($this->html); //20 conversion forms .. string 
                $data['headers'] = $this->headers($this->curl_html_page);  // 21 pagesize header
                $data['load_time'] = $this->load_time($url);   // 21 pagesize loadtime.
                $page_size[0] = $data['headers'];
                $page_size[1] = $data['load_time'];
                $page_size_json = json_encode($page_size); // 21 pagesize data convert into json format.
                $data['language'] = $this->language($url, $this->html);  // 22 language .. declared
                $data['lang'] = $this->get_lang($url, $this->html);  //22 language .. detected
                $language_data[0] = $data['language'];
                $language_data[1] = $data['lang'];
                $language_data_json = json_encode($language_data);  // 22 language data converted in json ..
                $data['technology'] = $this->technology($url1);
                $data['tech'] = $this->wordpress($this->html); //23 technology
                $data['google_analytic'] = $this->google_analytic($this->curl_html_page); //23  technology
                $data['diff_analytic'] = $this->diff_analytic($this->html); //23  technology
                $technology_data[0] = $data['tech'];
                $technology_data[1] = $data['google_analytic'];
                $technology_data[2] = $data['diff_analytic'];
                $technology_data_json = json_encode($technology_data); // 23 technology json data..
                $data['speed_tips'] = $this->speed_tips($url, $this->html); //24 speed tips data ..
                $speed_tips_json = json_encode($data['speed_tips']); //24 speed tips data
                $data['w3c_validator'] = $url;
                $data['doctype'] = $this->doctype($this->curl_html_page); // 25 doctype... string
                $data['encoding'] = $this->encoding($this->html); // 26 encoding ...
                $data['dep'] = $this->dep($this->html); // 27 depricated html as array ...
                $depricated_html_json = json_encode($data['dep']); // 27 depricated in json ....
                $data['social_count'] = $this->social_count($url); // 28 social count data ...
                $social_count_json = json_encode($data['social_count']); // 28 social count data json format .
                $data['pagerank'] = $url1;
                $data['get_alexa_rank'] = $url1;
                $data['country2'] = $Domain1;
                if ($rescan == "rescan") {
                    $id = $this->aorankmodel->update_database($url, $title_json, $description_json, $meta_keywords_json, $words_json, $heading_data_json, $focused_keyword_json, $keywords_consistency_json, $image_data_json, $data['get_text_ratio'], $data['index_page'], $google_preview_json, $blinks_page_links, $data['GoogleBL'], $data['redirect'], $data['url_rewrite'], $data['url_underscores'], $data['url_char_count'], $data['favicon'], $data['checkurl'], $data['conversion_forms'], $page_size_json, $language_data_json, $technology_data_json, $speed_tips_json, $data['doctype'], $data['encoding'], $depricated_html_json, $social_count_json, $link_status, $ip_city_country);
                    $data['id'] = $id;
                    $this->load->view('analytics', $data);
                } else {
                    $id = $this->aorankmodel->insert_data($url, $title_json, $description_json, $meta_keywords_json, $words_json, $heading_data_json, $focused_keyword_json, $keywords_consistency_json, $image_data_json, $data['get_text_ratio'], $data['index_page'], $google_preview_json, $blinks_page_links, $data['GoogleBL'], $data['redirect'], $data['url_rewrite'], $data['url_underscores'], $data['url_char_count'], $data['favicon'], $data['checkurl'], $data['conversion_forms'], $page_size_json, $language_data_json, $technology_data_json, $speed_tips_json, $data['doctype'], $data['encoding'], $depricated_html_json, $social_count_json, $link_status, $ip_city_country);
                    $data['id'] = $id;
                    $this->load->view('analytics', $data);
                }
            } else {
                // if url in database .. 
                // fetch from database
                $url_data_array = $this->aorankmodel->get_data($url);
                $url_data = $url_data_array[0];
                $db['get_url'] = $url_data['url'];   // 0 (url)  string form
                $title_json = $url_data['aorank_title'];    // 1 title
                $db['title'] = json_decode($title_json);    // 1 title
                $description_json = $url_data['aorank_description'];  // 2 description 
                $db['description'] = json_decode($description_json);   // 2 description 
                $meta_keywords_json = $url_data['aorank_meta_keywords']; // 3 meta keywords 
                $db['meta_keywords'] = json_decode($meta_keywords_json); // 3 meta keywords 
                $words_data_json = $url_data['aorank_content_length']; // 4 words   ...
                $db['words'] = json_decode($words_data_json);    // 4 words   ...
                $heading_json = $url_data['aorank_headings'];     // 5 heading
                $heading_data = json_decode($heading_json);       // 5 heading
                $db['heading'] = $heading_data[0];                   // 5 heading
                $db['heading_innertext'] = $heading_data[1];             // 5 heading  
                $focused_keyword_json = $url_data['aorank_focused_keywords'];      // 6 focused keyword
                $db['Keywords_cloud'] = json_decode($focused_keyword_json);      // 6 focused keyword
                $keyword_consistency_json = $url_data['aorank_keyword_consistency'];  //7 keyword consistency
                $db['keywords_consistency'] = json_decode($keyword_consistency_json); //7 keyword consistency
                $images_data_json = $url_data['aorank_images']; // 8 images
                $db['image'] = json_decode($images_data_json, true); // 8 images
                $db['get_text_ratio'] = $url_data['aorank_text_ratio']; // 9 text html ratio
                $db['index_page'] = $url_data['aorank_index_page_data']; // 10 page index .. 
                $google_preview_json = $url_data['aorank_google_preview']; //11 google preview .
                $db['google_preview'] = json_decode($google_preview_json); //11 google preview ..
                $blinks_data = $url_data['aorank_in_page_links'];  //   12 page links 
                $blinks_data_json = stripslashes($blinks_data);  //   12 page links 
                $db['blinks'] = json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $blinks_data_json), true); //   12 page links
                $db['GoogleBL'] = $url_data['aorank_page_backlinks'];   //13  page back links
                $db['redirect'] = $url_data['aorank_www_redirect'];   //14
                $db['url_rewrite'] = $url_data['aorank_url_rewrite'];   // 15
                $db['url_underscores'] = $url_data['aorank_url_underscore'];   // 16
                $db['url_char_count'] = $url_data['aorank_domain_char_count']; //17
                $db['favicon'] = $url_data['aorank_favicon']; //18 
                $db['checkurl'] = $url_data['aorank_check_404_page']; // 19
                $db['conversion_forms'] = $url_data['aorank_coversion_form']; // 20
                $page_size_json = $url_data['aorank_page_size'];
                $page_size_data = json_decode($page_size_json, true);
                $db['headers'] = $page_size_data[0];  //21
                $db['load_time'] = $page_size_data[1];  //21
                $language_data_json = $url_data['aorank_language']; // 22
                $language_data = json_decode($language_data_json, true);
                $db['language'] = $language_data[0];   // 22
                $db['lang'] = $language_data[1]; // 22
                $technology_data_json = $url_data['aorank_technology'];
                $technology_data = json_decode($technology_data_json, true);
                $db['tech'] = $technology_data[0]; //23
                $db['google_analytic'] = $technology_data[1]; //23
                $db['diff_analytic'] = $technology_data[2]; //23
                $speed_tips_json = $url_data['aorank_speed_tips'];  //24 
                $db['speed_tips'] = json_decode($speed_tips_json, true);  // 24
                $db['doctype'] = $url_data['aorank_doctype']; // 25
                $db['encoding'] = $url_data['aorank_encoding']; // 26
                $depricated_json = $url_data['aorank_deprecated_html']; // 27
                $db['dep'] = json_decode($depricated_json, true); // 27
                $social_count_json = $url_data['aorank_social_count']; //28
                $db['social_count'] = json_decode($social_count_json, true); //28
                $link_status_json = $url_data['aorank_link_status']; //29
                $db['link_status'] = json_decode($link_status_json, true); //29
                $ip_city_country = $url_data['aorank_ip_city_country'];
                $ip_city_country_data = explode("###", $ip_city_country);
                $no_of_ip = $ip_city_country_data[0];
                $city = $ip_city_country_data[1];
                $country = $ip_city_country_data[2];
                $db['page_ip'] = array(
                    "no_of_ip" => "$no_of_ip",
                    "city" => "$city",
                    "Country" => "$country"
                );
// ajax data .....
                $who_is_info_json = $url_data['ajax_whois_fullinfo_json'];
                $whois_data = explode("###", $who_is_info_json);
                $db['time_info'] = $whois_data[0];
                $db['owner_info'] = $whois_data[1];
                $db['related_websites'] = $url_data['aorank_ajax_related_websites'];
                $db['screen_image'] = $url_data['aorank_ajax_screen_img'];
                $db['page_rank'] = $url_data['aornak_ajax_pagerank'];
                $db['alexa_rank'] = $url_data['aorank_ajax_alexa_rank'];
                $db['w3c_reply'] = $url_data['aorank_ajax_w3c_error_count'];
                $db['pagelink_icon_tics'] = $url_data['aorank_ajax_pagelink'];
                $robot_text_json = $url_data['aorank_ajax_robot_txt_json'];
                $robot_text_data = explode("###", "$robot_text_json");
                $robot_result = $robot_text_data[0];
                $count = $robot_text_data[1];
                $db['robot_text_data'] = array(
                    'robot_result' => $robot_result,
                    'count' => $count
                );
                $xml_data_json = $url_data['aorank_ajax_sitemap_xml'];
                $db['xml_data'] = json_decode($xml_data_json, true);
                $db['alexa_ranks'] = $url_data['aorank_ajax_alexa_rank'];
                $domain_country_json = $url_data['aorank_ajax_country'];
                $db['unique_title'] = $url_data['aorank_ajax_unique_title'];
//$domain_country_data = json_decode($domain_country_json,true);
// i am from database ...
                $this->load->view('db_analytics', $db);
            }
        } else {
            // if url page  not exist  
            $result = $this->aorankmodel->fetch_links_search();
            $numrows = $result[0];
            $data = $result[1];
            $x = 0;
            $filter_links = array();
            foreach ($data as $web_data) {
                $screen_image = $web_data['aorank_ajax_screen_img'];
                if ($screen_image != '') {
                    $img_result = $this->getstatus($screen_image);
                    if (strpos($img_result, '200') !== false) {
                        if ($x < 5) {
                            $filter_links[$x] = $web_data;
                            $x++;
                        } else {
                            break;
                        }
                    }
                }
            }
            $linksdata['data_present'] = $numrows;
            $linksdata['data'] = $filter_links;
            $linksdata['wrong_url'] = "wrong_url";
            $this->load->view('search', $linksdata);
        }
    }

////////////////////////////////////Get Related Links///////////////////////////////////////////////// 


    public function anchor_links($html) {

        $result = '';
        foreach ($html->find("a") as $element) {
            $result[] = $element->href;
        }

        return $result;
    }

    public function back_links_juice($url) {

        $pUrl = parse_url($url);
        $domain = isset($pUrl['host']) ? $pUrl['host'] : '';
        if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) {
            $find = ($regs['domain']);
        } else {
            $find = substr($url, 11);
        }
        //var_dump($find);
        $find_int = "#";
        // Load the HTML into a DOMDocument

        $doc = new DOMDocument;
        @$doc->loadHTMLFile($url);
        // Look for all the 'a' elements
        $links = $doc->getElementsByTagName('a');
        $total_link_count = 0;
        $internal_link_count = 0;
        $external_link_count = 0;
        $link_type = '';
        $link_text = '';
        $http_status = '';

        foreach ($links as $link) {

            $href = $link->getAttribute('href');
            /*             * *****************************************************  URL SANATIZATION ********************************************************** */

            if (strpos($href, "http://") === 0 || strpos($href, "https://") === 0) {
                
            } elseif (strpos($href, "//") === 0) {
                $href = "http:" . $href;
            } elseif (strpos($href, "/") !== 0 || strpos($href, "?") === 0 || strpos($href, "#") === 0) {
                $href = "http://" . $find . "/" . $href;
            } else {
                $href = "http://" . $find . "" . $href;
            }
            /*             * ************************************************* END OF URL SANATIZATION ********************************************************** */
            $total_link_count++;
            $pos = stripos($href, $find);
            if ($pos !== false) {
                if ($link->getAttribute("rel='nofollow'")) {

                    $link_type = "nofollow";
                } else if ($link->getAttribute("rel='follow'")) {

                    $link_type = "dofollow";
                } else if (!$link->getAttribute('rel')) {
                    $link_type = "dofollow";
                }
                if ($link->textContent) {
                    $link_text = $link->textContent;
                }
                $this->link[] = $href;
                $internal_link_count++;
                $total_link[] = $href . ",," . "Internal" . ",," . $link_type . ",," . $link_text . ",," . $http_status;
            } else {
                if ($link->getAttribute("rel='nofollow'")) {
                    $link_type = "nofollow";
                } else if ($link->getAttribute("rel='follow'")) {
                    $link_type = "dofollow";
                } else if ($link->getAttribute('href') && !$link->getAttribute('rel')) {
                    $link_type = "dofollow";
                }
                if ($link->textContent) {
                    $link_text = $link->textContent;
                }
                $this->link[] = $href;
                $external_link_count++;
                $total_link[] = $href . ",," . "External" . ",," . $link_type . ",," . $link_text . ",," . $http_status;
            }
        }
        foreach ($total_link as $k => $v) {
            $tot_links[] = explode(',,', $v);
        }
        $chart_details = $this->chart($total_link_count, $internal_link_count, $external_link_count);
        $data['link_detail'] = $tot_links;
        $data['chart_detail'] = $chart_details;
        return ($data);
    }

    public function meta_robots($html) {

        $result = '';
        foreach ($html->find("meta[name='robots']") as $element) {
            $result = $element->content;
        }

        return $result;
    }

//    public function  unique_title(){
//        $title = implode(' ', $this->title);
//        
//        //echo $title."<br><br>";
//        
//         $title = str_replace(' ', '+', $title);
//          
//       $url  = 'http://www.google.com/search?q=intitle:"'.$title.'"&gws_rd=ssl';
//   
//      $html = $this->dom_parser->file_get_html($url);
//
//
//
//
//        foreach ($html->find('div[id="resultStats"]') as $promo) {
//            $result = $promo->innertext;
//        }
//
//       
//        
//        $result = str_replace('results', '', $result);
//        $result = str_replace('About', '', $result);
//        $result = str_replace(' ', '', $result);
//        
//return $result;
//
// 
// 
// 
// 
//    }
//

    public function unique_title() {
        $title = $_POST['title'];
        $title = str_replace(array('|', ','), ' ', $title);
        $title = str_replace(' ', '+', $title);
        $title = preg_replace('/\++/', '+', $title);
        $url = 'http://www.google.com/search?q=intitle:"' . $title . '"&gws_rd=ssl';
        $html = $this->dom_parser->file_get_html($url);
        foreach ($html->find('div[id="resultStats"]') as $promo) {
            $result = $promo->innertext;
        }
        $result = str_replace(array('results', 'result'), '', $result);
        $result = str_replace('About', '', $result);
        $result = str_replace(' ', '', $result);
        $unique_title_result = "";
        if (isset($result)) {
            if ($result == '1' || $result == '0' || $result == NULL) {

                $unique_title_result .= " <div class=\"fg-callout nomargin green\"><h2 class=\"regular green\">Great..! Title is unique</h2><a class=\"close icon-checkmark\"></a></div>";
            } else {                
                $unique_title_result .= "<div class=\"fg-callout nomargin red\"><h2 class=\"regular red\">Title is not unique</h2><p>We found $result similar title on the web</p><a class=\"close icon-cross\"></a></div>";
            }


            $url_search_id = $_POST['id'];
            $ajaxdata = array("unique_title" => "$unique_title_result");
            $this->aorankmodel->update_data($url_search_id, $ajaxdata);

            echo $result;
          //  echo $unique_title_result;   // ajax 1....
        }
    }

    public function get_related_link_demo() {

        $keyword = $_POST['title'];
        $url = $_POST['url'];


        $main_url = parse_url($url);
        $main_host_url = $main_url['host'];
        $tag = '';
        $links = '';
        $pr = '';
        $domain = substr($url, 11);
        $word = substr($domain, 0, strpos($domain, '.'));

        $keyword = strtolower($keyword);

        $keyword = str_replace(array($url, $domain, $word, "-", "."), ' ', $keyword);
        $keyword = str_replace(' ', '+', $keyword);


        $url = 'http://www.google.com/search?q=' . $keyword . '&start=0&num=20&gws_rd=ssl';


        $html = $this->dom_parser->file_get_html($url);



        $linkObjs = $html->find('h3.r a');
        foreach ($linkObjs as $linkObj) {
            $title = trim($linkObj->plaintext);
            $link = trim($linkObj->href);

            //  if it is not a direct link but url reference found inside it, then extract
            if (!preg_match('/^https?/', $link) && preg_match('/q=(.+)&amp;sa=/U', $link, $matches) && preg_match('/^https?/', $matches[1])) {
                $link = $matches[1];
            } else if (!preg_match('/^https?/', $link)) { // skip if it is not a valid link
                continue;
            }

            $head = $this->getstatus($link);
            if (strpos($head, '200') !== false) {
                $host_url = parse_url($link);
                $link_url = ($host_url['host']);
                if ((strcmp("$main_host_url", "$link_url"))) {
                    $pr = $this->get_google_pr_demo($link);
                    $links = $link;
                } else {
                    
                }
            }
            $tag = $tag . "||" . $links . "@|@" . $pr;
        }
        $tag = ltrim($tag, '||');
        $tmp = array_filter(explode('||', $tag));
        $data = array();
        foreach ($tmp as $k => $v) {
            $data[] = array_filter(explode('@|@', $v));
        }
        $input = (array_filter($data));
        $result = array_map("unserialize", array_unique(array_map("serialize", $input)));
        $result = array_values($result);

        $data = "";

        for ($i = 0; $i < 5; $i++) {
            $pr_test = 0;
            if (isset($result[$i][1])) {
                $pr_test = $result[$i][1];
            } else {
                
            }

            if (!empty($result[$i][0])) {

                //$url=$result[$i][0];
                //$url = str_replace(' ','_',$url);
                //$url = str_replace(array('%2F','%5C'), array('%252F','%255C'), urlencode($url));

                $data .= "<tr class=\"analytics-tr\">";
                $data .="<td class=\"analytics-table-td-url\"><a class=\"related-web-a\" href=" . base_url() . "aorankcontroller/analytics/{$result[$i][0]} target=\"_blank\">{$result[$i][0]}</a></td>";
                $data .="<td class=\"analytics-table-td-page-rank\">{$pr_test}</td>";
                $data .="</tr>";
            }
        }



        $url_search_id = $_POST['id'];
        $ajaxdata = array("relatedwebsites" => "$data");
        $this->aorankmodel->update_data($url_search_id, $ajaxdata);

        echo $data;   // ajax 2....
    }

    // status code
    public function getstatus($url) {

        $this->curl->create($url);
        $this->curl->option('NOBODY', true);
        $this->curl->option('SSL_VERIFYPEER', false);
        $this->curl->option('SSL_VERIFYHOST', false);
        $this->curl->option('SSLVERSION', 3);
        $this->curl->option('URL', $url);
        $this->curl->option('HEADER', true);
        $temp = $this->curl->execute();
        $this->curl->close();
        return $temp;
    }

    public function getstatus_link() {
        $url = $_POST['src_url'];
        $c = curl_init();
        curl_setopt($c, CURLOPT_NOBODY, true);
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($c, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($c, CURLOPT_URL, $url);
        curl_exec($c);
        $status = curl_getinfo($c, CURLINFO_HTTP_CODE);
        curl_close($c);




        if ($status == 404 || $status == 0 || $status == 403 || $status == 503) {
            $http = "cross";
        } else {
            $http = "ok";
        }

        $pagelink_ajaxdata = "<img src=" . base_url() . "images/$http.png width=16px height=16px  title=\"Response Code $status\">";

        $url_search_id = $_POST['id'];
        $ajaxdata = array("pagelink_data" => "$pagelink_ajaxdata");
        $this->aorankmodel->update_data($url_search_id, $ajaxdata);




        echo "<img src=" . base_url() . "images/$http.png width=16px height=16px  title=\"Response Code $status\">"; // ajax 3..
    }

    ////////////////////////////////////url and screen img///////////////////////////////////////////////// 


    public function w3c_validator() {
        $w3c_count = 0;
        $w3c_val_url = $_POST['src_url'];
        $w3c_url = 'http://validator.w3.org/check?uri=' . $w3c_val_url;
        $str = $this->curl($w3c_url);
        $html = str_get_html($str);


        foreach ($html->find('li') as $element) {
            if ($element->class == 'error') {
                //$w3c_validate = $element->innertext . " ";
                $w3c_count++;
            }
        }
        $w3c_error_count_data = $w3c_count . " error";
        $url_search_id = $_POST['id'];
        $w3c_count_data = array("w3c_error_count" => "$w3c_error_count_data");
        $this->aorankmodel->update_data($url_search_id, $w3c_count_data);



        echo $w3c_count . " error";  // ajax 4 done..
    }

    public function get_url($url) {

        return $url;
    }

    public function screen() {
        $screen = $_POST['src_url'];
        $screen_img = "http://sitescreens.woorank.com/" . $screen . ".png";
        $screen_img_url = "http://sitescreens.woorank.com/" . $screen . ".png";


        $url_search_id = $_POST['id'];
        $ajax_screen_img = array("screen_img" => "$screen_img_url");
        $this->aorankmodel->update_data($url_search_id, $ajax_screen_img);

        echo $screen_img; // ajax 5..
    }

//    public function screen($Domain) {
//
//        $screen = "http://sitescreens.woorank.com/" . $Domain . ".png";
//
//        return $screen;
//    }
    //////////////////////////////////////////////////////////////get related//////////////////////////////////////////////////////////////
    public function get_related_link($html, $url) {
        $main_url = parse_url($url);
        $main_host_url = $main_url['host'];
        $tag = '';
        $links = '';
        $pr = '';
        $domain = substr($url, 11);
        $word = substr($domain, 0, strpos($domain, '.'));
        foreach ($html->find('title') as $element) {
            $keyword = $element->innertext;
        }
        $keyword = strtolower($keyword);

        $keyword = str_replace(array($url, $domain, $word, "-", "."), ' ', $keyword);
        $keyword = str_replace(' ', '+', $keyword);
        $url = 'http://www.google.com/search?q=' . $keyword . '&start=0&num=20&gws_rd=ssl';
        $curl_handle = curl_init();
        curl_setopt($curl_handle, CURLOPT_URL, $url);
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
        $url = curl_exec($curl_handle);
        curl_close($curl_handle);
        $url = $this->dom_parser->str_get_html($url);
        $linkObjs = $url->find('h3.r a');
        foreach ($linkObjs as $linkObj) {
            $title = trim($linkObj->plaintext);
            $link = trim($linkObj->href);

            //  if it is not a direct link but url reference found inside it, then extract
            if (!preg_match('/^https?/', $link) && preg_match('/q=(.+)&amp;sa=/U', $link, $matches) && preg_match('/^https?/', $matches[1])) {
                $link = $matches[1];
            } else if (!preg_match('/^https?/', $link)) { // skip if it is not a valid link
                continue;
            }



            $head = get_headers($link, 1);

            if (strpos($head[0], '200') !== false) {


                $host_url = parse_url($link);
                $link_url = ($host_url['host']);

                if ((strcmp("$main_host_url", "$link_url"))) {

                    $pr_temp = $this->get_google_pr($link_url);
                    if (!empty($pr_temp)) {
                        $pr = $this->get_google_pr($link_url);
                        $links = $link_url;
                    }
                } else {
                    
                }
            }

            $tag = $tag . "||" . $links . "@|@" . $pr;
        }
        $tag = ltrim($tag, '||');
        $tmp = array_filter(explode('||', $tag));


        $data = array();

        foreach ($tmp as $k => $v) {
            $data[] = array_filter(explode('@|@', $v));
        }

        $input = (array_filter($data));
        $result = array_map("unserialize", array_unique(array_map("serialize", $input)));

        $result = array_values($result);
        return ($result);
    }

    //////////////////////////////////////////////////////////////get title///////////////////////////////////////////////////////////////

    public function gettitle($html) {


        $title = ' ';
        foreach ($html->find('title') as $element) {
            $title = $element->innertext;
        } $length = strlen($title);

        // $words = explode(" ", $title);
        // var_dump($words);
        $total[] = $title;
        $this->title = $total;

        $title = strtolower($title);
        $title = $this->get_stopwords($title);
        $title1 = str_replace(array('.', ',', '=', '-', '|', '&', '@', '!', '#', '%', '^', '>', '<', ';', '8250'), ' ', $title);
        $title1 = explode(' ', $title1);
        $title1 = array_unique($title1);
        $title1 = implode(' ', $title1);
        $html_tag = $this->get_data_by_tags($this->html);
        $img_alt = $this->img_alt;
        extract($html_tag);   // extract keyword into variables.
        $heading_text = $this->heading_h1 . " " . $this->heading_h2 . " " . $this->heading_h3 . " " . $this->heading_h4 . " " . $this->heading_h5 . " " . $this->heading_h6;
        $heading = strip_tags($heading_text);
        $body = $body . $img_alt;
        $body = $body . $heading;
        $script_filtered_page = $this->filter_body_content($body);
        $arr1 = explode(' ', $title1);
        $arr2 = explode(' ', $script_filtered_page);
        $aint = array_intersect($arr2, $arr1);
        $aint = array_unique($aint);
        $unique_str = implode(' ', $aint);
        $unique_str = str_word_count($unique_str);
        $title1 = str_word_count($title1);

        $total[] = round((($unique_str / $title1) * 100), 1) . "%";
        $total[] = $length;


        $result = array_diff($arr1, $aint);
        //$result = implode(', ', $result);
        $total[] = $result;

        return($total);
    }

    public function filter_body_content($body) {

        $body = preg_replace('/<!--(.|\s)*?-->/', '', $body);
        // remove style
        $body = preg_replace("/<style\\b[^>]*>(.*?)<\\/style>/s", "", $body);
        // remove script
        $body = preg_replace("/<script\\b[^>]*>(.*?)<\\/script>/s", "", $body);
        // remove noscript
        $body = preg_replace('#<noscript(.*?)>(.*?)</noscript>#is', '', $body);
        // remove tag
        $body = preg_replace('#<[^>]+>#', ' ', $body);

        $body = preg_replace("@\\b[a-z]\\b ?@i", "", $body);

        $x = strtolower($body);
        $x = $this->get_stopwords($x);
        $x = preg_replace('/\s+/', ' ', $x);
        $x = str_replace(array('.', ',', '='), ' ', $x);
        $x = str_replace(' ', '-', $x);
        $x = preg_replace('/[^A-Za-z0-9\-]/', '', $x);
        $x = preg_replace('/\-+/', '-', $x);
        $x = str_replace('-', ' ', $x);

        return($x);
    }

    //////////////////////////////////////////////////////////////get meta description///////////////////////////////////////////////////////////////

    public function get_meta_description($html) {
        $description = "";
        foreach ($html->find("meta[@name='description']") as $promo) {
            $description = $promo->content;
        }
        $this->meta_description = $description;

        // print_r($this->meta_description);
        $length = strlen($description);
        $description_and_length = $description . ",," . $length;
        $description_and_length = explode(",,", $description_and_length);


        return $description_and_length;
    }

    ////////////////////////for meta keywords//////////////////////////////////////////////////

    public function get_meta_keywords($html) {
        $meta_keywords = "";
        foreach ($html->find("meta[@name='keywords']") as $promo) {
            $meta_keywords = $promo->content;
        }
        if ($meta_keywords === "") {
            return NULL;
        } else {
            $meta_keywords = explode(",", $meta_keywords);
            return($meta_keywords);
        }
    }

    //////////////////////////////////////////////////////////////get heading h1 h32.....h6///////////////////////////////////////////////////////////////


    public function get_heading($html) {
        $head = '';
        $h1_count = 0;
        $h2_count = 0;
        $h3_count = 0;
        $h4_count = 0;
        $h5_count = 0;
        $h6_count = 0;

        foreach ($html->find("h1") as $promo) {
            $h1_count++;
            $this->heading_h1 = $this->heading_h1 . " " . $promo->innertext;
        }
        foreach ($html->find("h2") as $promo) {
            $h2_count++;
            $this->heading_h2 = $this->heading_h2 . " " . $promo->innertext;
        }
        foreach ($html->find("h3") as $promo) {
            $h3_count++;
            $this->heading_h3 = $this->heading_h3 . " " . $promo->innertext;
        }
        foreach ($html->find("h4") as $promo) {
            $h4_count++;
            $this->heading_h4 = $this->heading_h4 . " " . $promo->innertext;
        }
        foreach ($html->find("h5") as $promo) {
            $h5_count++;
            $this->heading_h5 = $this->heading_h5 . " " . $promo->innertext;
        }
        foreach ($html->find("h6") as $promo) {
            $h6_count++;
            $this->heading_h6 = $this->heading_h6 . " " . $promo->innertext;
        }
        $head = $h1_count . "=" . $h2_count . "=" . $h3_count . "=" . $h4_count . "=" . $h5_count . "=" . $h6_count;
        $head = explode("=", $head);

        return $head;
    }

    //////////////////////////////////////////////////////////////get heading details///////////////////////////////////////////////////////////////


    public function get_heading_details($html) {
        $count = '';
        for ($i = 1; $i < 7; $i++) {
            foreach ($html->find("h$i") as $promo) {
                $count = $count . "=" . "&#60h$i&#62" . $promo->innertext . "&#60/h$i&#62";
            }
        }
        $count = strip_tags($count);

        $count = explode("=", $count);




        return $count;
    }

    //////////////////////////////////////////////////////////////keywords cloud details///////////////////////////////////////////////////////////////
//stopwords

    public function get_stopwords($html) {
        $stopwords = array('nbsp', 'to', 'more', 'lt', 'px', 'gt', 'itspan', 'a', 'able', 'about', 'above', 'abroad', 'according', 'accordingly', 'across', 'actually', 'adj', 'after', 'afterwards', 'again', 'against', 'ago', 'ahead', 'ain\'t', 'all', 'allow', 'allows', 'almost', 'alone', 'along', 'alongside', 'already', 'also', 'although', 'always', 'am', 'amid', 'amidst', 'among', 'amongst', 'an', 'and', 'another', 'any', 'anybody', 'anyhow', 'anyone', 'anything', 'anyway', 'anyways', 'anywhere', 'apart', 'appear', 'appreciate', 'appropriate', 'are', 'aren\'t', 'around', 'as', 'a\'s', 'aside', 'ask', 'asking', 'associated', 'at', 'available', 'away', 'awfully', 'b', 'back', 'backward', 'backwards', 'be', 'became', 'because', 'become', 'becomes', 'becoming', 'been', 'before', 'beforehand', 'begin', 'behind', 'being', 'believe', 'below', 'beside', 'besides', 'best', 'better', 'between', 'beyond', 'both', 'brief', 'but', 'by', 'c', 'came', 'can', 'cannot', 'cant', 'can\'t', 'caption', 'cause', 'causes', 'certain', 'certainly', 'changes', 'clearly', 'c\'mon', 'co', 'co.', 'com', 'come', 'comes', 'concerning', 'consequently', 'consider', 'considering', 'contain', 'containing', 'contains', 'corresponding', 'could', 'couldn\'t', 'course', 'c\'s', 'currently', 'd', 'dare', 'daren\'t', 'definitely', 'described', 'despite', 'did', 'didn\'t', 'different', 'directly', 'do', 'does', 'doesn\'t', 'doing', 'done', 'don\'t', 'down', 'downwards', 'during', 'e', 'each', 'edu', 'eg', 'eight', 'eighty', 'either', 'else', 'elsewhere', 'end', 'ending', 'enough', 'entirely', 'especially', 'et', 'etc', 'even', 'ever', 'evermore', 'every', 'everybody', 'everyone', 'everything', 'everywhere', 'ex', 'exactly', 'example', 'except', 'f', 'fairly', 'far', 'farther', 'few', 'fewer', 'fifth', 'first', 'five', 'followed', 'following', 'follows', 'for', 'forever', 'former', 'formerly', 'forth', 'forward', 'found', 'four', 'from', 'further', 'furthermore', 'g', 'get', 'gets', 'getting', 'given', 'gives', 'go', 'goes', 'going', 'gone', 'got', 'gotten', 'greetings', 'h', 'had', 'hadn\'t', 'half', 'happens', 'hardly', 'has', 'hasn\'t', 'have', 'haven\'t', 'having', 'he', 'he\'d', 'he\'ll', 'hello', 'help', 'hence', 'her', 'here', 'hereafter', 'hereby', 'herein', 'here\'s', 'hereupon', 'hers', 'herself', 'he\'s', 'hi', 'him', 'himself', 'his', 'hither', 'hopefully', 'how', 'howbeit', 'however', 'hundred', 'i', 'i\'d', 'ie', 'if', 'ignored', 'i\'ll', 'i\'m', 'immediate', 'in', 'inasmuch', 'inc', 'inc.', 'indeed', 'indicate', 'indicated', 'indicates', 'inner', 'inside', 'insofar', 'instead', 'into', 'inward', 'is', 'isn\'t', 'it', 'it\'d', 'it\'ll', 'its', 'it\'s', 'itself', 'i\'ve', 'j', 'just', 'k', 'keep', 'keeps', 'kept', 'know', 'known', 'knows', 'l', 'last', 'lately', 'later', 'latter', 'latterly', 'least', 'less', 'lest', 'let', 'let\'s', 'like', 'liked', 'likely', 'likewise', 'little', 'look', 'looking', 'looks', 'low', 'lower', 'ltd', 'm', 'made', 'mainly', 'make', 'makes', 'many', 'may', 'maybe', 'mayn\'t', 'me', 'mean', 'meantime', 'meanwhile', 'merely', 'might', 'mightn\'t', 'mine', 'minus', 'miss', 'more', 'moreover', 'most', 'mostly', 'mr', 'mrs', 'much', 'must', 'mustn\'t', 'my', 'myself', 'n', 'name', 'namely', 'nd', 'near', 'nearly', 'necessary', 'need', 'needn\'t', 'needs', 'neither', 'never', 'neverf', 'neverless', 'nevertheless', 'new', 'next', 'nine', 'ninety', 'no', 'nobody', 'non', 'none', 'nonetheless', 'noone', 'no-one', 'nor', 'normally', 'not', 'nothing', 'notwithstanding', 'novel', 'now', 'nowhere', 'o', 'obviously', 'of', 'off', 'often', 'oh', 'ok', 'okay', 'old', 'on', 'once', 'one', 'ones', 'one\'s', 'only', 'onto', 'opposite', 'or', 'other', 'others', 'otherwise', 'ought', 'oughtn\'t', 'our', 'ours', 'ourselves', 'out', 'outside', 'over', 'overall', 'own', 'p', 'particular', 'particularly', 'past', 'per', 'perhaps', 'placed', 'please', 'plus', 'possible', 'presumably', 'probably', 'provided', 'provides', 'q', 'que', 'quite', 'qv', 'r', 'rather', 'rd', 're', 'really', 'reasonably', 'recent', 'recently', 'regarding', 'regardless', 'regards', 'relatively', 'respectively', 'right', 'round', 's', 'said', 'same', 'saw', 'say', 'saying', 'says', 'second', 'secondly', 'see', 'seeing', 'seem', 'seemed', 'seeming', 'seems', 'seen', 'self', 'selves', 'sensible', 'sent', 'serious', 'seriously', 'seven', 'several', 'shall', 'shan\'t', 'she', 'she\'d', 'she\'ll', 'she\'s', 'should', 'shouldn\'t', 'since', 'six', 'so', 'some', 'somebody', 'someday', 'somehow', 'someone', 'something', 'sometime', 'sometimes', 'somewhat', 'somewhere', 'soon', 'sorry', 'specified', 'specify', 'specifying', 'still', 'sub', 'such', 'sup', 'sure', 't', 'take', 'taken', 'taking', 'tell', 'tends', 'th', 'than', 'thank', 'thanks', 'thanx', 'that', 'that\'ll', 'thats', 'that\'s', 'that\'ve', 'the', 'their', 'theirs', 'them', 'themselves', 'then', 'thence', 'there', 'thereafter', 'thereby', 'there\'d', 'therefore', 'therein', 'there\'ll', 'there\'re', 'theres', 'there\'s', 'thereupon', 'there\'ve', 'these', 'they', 'they\'d', 'they\'ll', 'they\'re', 'they\'ve', 'thing', 'things', 'think', 'third', 'thirty', 'this', 'thorough', 'thoroughly', 'those', 'though', 'three', 'through', 'throughout', 'thru', 'thus', 'till', 'to', 'together', 'too', 'took', 'toward', 'towards', 'tried', 'tries', 'truly', 'try', 'trying', 't\'s', 'twice', 'two', 'u', 'un', 'under', 'underneath', 'undoing', 'unfortunately', 'unless', 'unlike', 'unlikely', 'until', 'unto', 'up', 'upon', 'upwards', 'us', 'use', 'used', 'useful', 'uses', 'using', 'usually', 'v', 'value', 'various', 'versus', 'very', 'via', 'viz', 'vs', 'w', 'want', 'wants', 'was', 'wasn\'t', 'way', 'we', 'we\'d', 'welcome', 'well', 'we\'ll', 'went', 'were', 'we\'re', 'weren\'t', 'we\'ve', 'what', 'whatever', 'what\'ll', 'what\'s', 'what\'ve', 'when', 'whence', 'whenever', 'where', 'whereafter', 'whereas', 'whereby', 'wherein', 'where\'s', 'whereupon', 'wherever', 'whether', 'which', 'whichever', 'while', 'whilst', 'whither', 'who', 'who\'d', 'whoever', 'whole', 'who\'ll', 'whom', 'whomever', 'who\'s', 'whose', 'why', 'will', 'willing', 'wish', 'with', 'within', 'without', 'wonder', 'won\'t', 'would', 'wouldn\'t', 'x', 'v', 'y', 'yes', 'yet', 'you', 'you\'d', 'you\'ll', 'your', 'you\'re', 'yours', 'yourself', 'yourselves', 'you\'ve', 'z', 'zero');

        $filtered_data = preg_replace('/\b(' . implode('|', $stopwords) . ')\b/', '', $html);
        return $filtered_data;
    }

//keyword cloud
// main

    public function get_keyword_cloud($html) {

        $obj = $this->get_data_by_tags($this->html);
        $img_alt = $this->img_alt_text($this->html);
        extract($obj);   // extract keyword into variables.
        $body = $body . $img_alt;
        $keyword_body_title = $this->get_keyword_count($body);
        return($keyword_body_title);
    }

// img alt text

    public function img_alt_text($html) {


        $total = array();
        foreach ($html->find('img')as $element) {
            $total[] = $element->alt;
        }
        $str = implode(' ', $total);
        $str = $str . " ";
        $this->img_alt = $str;
        return $str;
    }

//count
    public function get_keyword_count($body) {
        // remove comment
        $body = preg_replace('/<!--(.|\s)*?-->/', '', $body);
        // remove style
        $body = preg_replace("/<style\\b[^>]*>(.*?)<\\/style>/s", "", $body);
        // remove script
        $body = preg_replace("/<script\\b[^>]*>(.*?)<\\/script>/s", "", $body);
        // remove noscript
        $body = preg_replace('#<noscript(.*?)>(.*?)</noscript>#is', '', $body);
        // remove tag
        $body = preg_replace('#<[^>]+>#', ' ', $body);

        $body = preg_replace("@\\b[a-z]\\b ?@i", "", $body);

        $x = strtolower($body);
        $x = $this->get_stopwords($x);
        //remove digit
        $x = trim(str_replace(range(0, 9), '', $x));

        $x = preg_replace('/\s+/', ' ', $x);
        $x = str_replace(array('.', ',', '='), ' ', $x);
        $x = str_replace(chr(194), ' ', $x);

        // single character wid space
        $x = trim(preg_replace("/(^|\s+)(\S(\s+|$))+/", " ", $x));
        $x = str_replace(' ', '-', $x);
        $x = preg_replace('/[^A-Za-z0-9\-]/', '', $x);
        $x = preg_replace('/\-+/', '-', $x);
        $textToCount = explode('-', $x);
        $words = array_count_values($textToCount);
        $t_count = count($words);
        arsort($words);
        $output = array_slice($words, 0, 5);

        return $output;
    }

// main

    public function get_keyword($html) {
        $html_tag = $this->get_data_by_tags($html);

        $img_alt = $this->img_alt;
        $heading_text = $this->heading_h1 . " " . $this->heading_h2 . " " . $this->heading_h3 . " " . $this->heading_h4 . " " . $this->heading_h5 . " " . $this->heading_h6;
        $heading = strip_tags($heading_text);
        $heading = strtolower($heading);
        extract($html_tag);   // extract keyword into variables.
        $body = $body . $img_alt;
        $keyword_body_title = $this->remove_unwanted_tags_script_comments_count($body);
        $script_filtered_page = $this->remove_unwanted_tags_script_comments($body);
        $word_tag = $this->match_keywords_from_data($script_filtered_page, $keyword_body_title);
        extract($word_tag);
        // $title=implode(" ",$title);

        $keyword_con = "";

        for ($i = 0; $i < count($keyword_body_title); ++$i) {
            $words[$i] . "<br>";
            $repeatkeyword[$i] . "<br>";
            $density[$i] . "<br>";
            if (strpos($title, $words[$i]) !== false) {
                if (preg_match_all("~\b$words[$i]\b~", $title, $t)) {
                    $title_check = count($t[0]);
                } else {
                    $title_check = 'false';
                }
            } else {
                $title_check = 'false';
            }
            if (strpos($description, $words[$i]) !== false) {
                if (preg_match_all("~\b$words[$i]\b~", $description, $d)) {
                    $des = count($d[0]);
                } else {
                    $des = 'false';
                }
            } else {
                $des = 'false';
            }
            if (strpos($heading, $words[$i]) !== false) {
                if (preg_match_all("~\b$words[$i]\b~", $heading, $h)) {
                    $head = count($h[0]);
                } else {
                    $head = 'false';
                }
            } else {
                $head = 'false';
            }
            $keyword_con = $keyword_con . "|" . $words[$i] . "," . $repeatkeyword[$i] . "," . $density[$i] . "," . $title_check . "," . $des . "," . $head;
        }
        $keyword_con = ltrim($keyword_con, '|');
        $tmp = explode('|', $keyword_con);
        $data = array();
        foreach ($tmp as $k => $v) {
            $data[] = explode(',', $v);
        }
        return($data);
    }

    function match_keywords_from_data($script_filtered_page, $findkeywords) {

        $filterdata = strtolower($script_filtered_page);
        $totalkeyword = str_word_count($filterdata);


        foreach ($findkeywords as $words) {
            $len = str_word_count($words);

            $words = strtolower($words);
            $string = "/\b$words\b/";
            if (preg_match_all($string, $filterdata, $matches)) {
                if (count($matches[0])) {
                    $repeatkeyword = count($matches[0]);
                    $result['repeatkeyword'][] = $repeatkeyword;
                    $result['words'][] = $words;
                    $result['density'][] = round((($repeatkeyword * $len * 100) / $totalkeyword), 2) . " %";
                } else {
                    
                }
            } else {
                $repeatkeyword = "-";
                $result['repeatkeyword'][] = $repeatkeyword;
                $result['words'][] = ' ';
                $result['density'][] = "-";
            }
        }
        return($result);
    }

//

    public function remove_unwanted_tags_script_comments($body) {
        $body = preg_replace('/<!--(.|\s)*?-->/', '', $body);
        // remove style
        $body = preg_replace("/<style\\b[^>]*>(.*?)<\\/style>/s", "", $body);
        // remove script
        $body = preg_replace("/<script\\b[^>]*>(.*?)<\\/script>/s", "", $body);
        // remove noscript
        $body = preg_replace('#<noscript(.*?)>(.*?)</noscript>#is', '', $body);
        // remove tag
        $body = preg_replace('#<[^>]+>#', ' ', $body);

        $body = preg_replace("@\\b[a-z]\\b ?@i", "", $body);

        $x = strtolower($body);
        //$x = $this->get_stopwords($x);
        //remove digit
        $x = trim(str_replace(range(0, 9), '', $x));

        $x = preg_replace('/\s+/', ' ', $x);
        $x = str_replace(array('.', ',', '='), ' ', $x);
        $x = str_replace(chr(194), ' ', $x);

        // single character wid space
        $x = trim(preg_replace("/(^|\s+)(\S(\s+|$))+/", " ", $x));
        $x = str_replace(' ', '-', $x);
        $x = preg_replace('/[^A-Za-z0-9\-]/', '', $x);
        $x = preg_replace('/\-+/', '-', $x);
        $filtered_data = str_replace('-', ' ', $x);
        // echo $filtered_data;
        return($filtered_data);
    }

// count keyword
    public function remove_unwanted_tags_script_comments_count($body) {

        $body = preg_replace('/<!--(.|\s)*?-->/', '', $body);
        // remove style
        $body = preg_replace("/<style\\b[^>]*>(.*?)<\\/style>/s", "", $body);
        // remove script
        $body = preg_replace("/<script\\b[^>]*>(.*?)<\\/script>/s", "", $body);
        // remove noscript
        $body = preg_replace('#<noscript(.*?)>(.*?)</noscript>#is', '', $body);
        // remove tag
        $body = preg_replace('#<[^>]+>#', ' ', $body);

        $body = preg_replace("@\\b[a-z]\\b ?@i", "", $body);

        $x = strtolower($body);
        $x = $this->get_stopwords($x);
        //remove digit
        $x = trim(str_replace(range(0, 9), '', $x));

        $x = preg_replace('/\s+/', ' ', $x);
        $x = str_replace(array('.', ',', '='), ' ', $x);
        // single character wid space
        $x = trim(preg_replace("/(^|\s+)(\S(\s+|$))+/", " ", $x));
        $x = str_replace(' ', '-', $x);
        $x = preg_replace('/[^A-Za-z0-9\-]/', '', $x);
        $x = preg_replace('/\-+/', '-', $x);
        $textToCount = explode('-', $x);
        $words = array_count_values($textToCount);
        $t_count = count($words);
        arsort($words);
        $output = array_slice($words, 0, 10);
        $i = 1;
        foreach ($output as $x => $x_value) {
            $total[] = $x;
        }
        return($total);
    }

///////////////////////////////////////////////////////////////////////////////////////
    public function get_body_content($html) {

        foreach ($html->find('body') as $element) {
            $body_text = $element->innertext;
        }

        $this->body_content = $body_text;


        return $body_text;
    }

    ///////////////////////////////////////////////////////////
// title description keyword body

    public function get_data_by_tags($html) {


        $x = implode(' ', $this->title);
        $obj['title'] = strtolower($x);

        $obj['description'] = strtolower($this->meta_description);


        $obj['body'] = strtolower($this->body_content);


        return $obj;
    }

    public function get_no_words($html) {

        $html_tag = $this->get_data_by_tags($this->html);

        $img_alt = $this->img_alt;
        extract($html_tag);   // extract keyword into variables.
        $heading_text = $this->heading_h1 . " " . $this->heading_h2 . " " . $this->heading_h3 . " " . $this->heading_h4 . " " . $this->heading_h5 . " " . $this->heading_h6;
        $heading = strip_tags($heading_text);
        $body = $body . $img_alt;
        $body = $body . $heading;
        $script_filtered_page = $this->remove_unwanted_tags_script_comments($body);
        $totl_without_stop = str_word_count($script_filtered_page);
        $unique_without_stop = $this->find_unique_keyword($script_filtered_page);
        $script_filtered_page = $this->get_stopwords($script_filtered_page);
        $word = str_word_count($script_filtered_page);
        $word1 = $word . " ( Including Stopwords " . $totl_without_stop . ")";
        $total[] = $word1;
        $wordd = $this->find_unique_keyword($script_filtered_page);
        $word2 = $wordd . " ( Including Stopwords " . $unique_without_stop . ")";
        $total[] = $word2;
        return($total);
    }

    public function find_unique_keyword($script_filtered_page) {
        $unique_keyword = strtolower($script_filtered_page);
        //$unique_keyword=preg_replace('/[^A-Za-z\-]/', ' ', $script_filtered_page);
        $unique_keyword = implode(' ', array_unique(explode(' ', $unique_keyword)));
        $unique_keyword = str_word_count($unique_keyword);
        return $unique_keyword;
    }

    //////////////////////////////////////////////////////////////get total images and count///////////////////////////////////////////////////////////////

    public function getimage($html) {
        $src_with_and_without_alt = '';
        $i = "";
        $count_img = '';
        $img_with_out_alt = '';
        foreach ($html->find('img') as $promo) {
            $image['src'][] = $promo->src;
            $count_img++;
        }
        foreach ($html->find('img') as $promo) {
            $image['alt'][] = $promo->alt;
            if ($promo->alt != '') {
                $i++;
            }
        }


        foreach ($html->find('img') as $img_data) {


            $src_with_and_without_alt[] = array(
                "src" => $img_data->src,
                "alt" => $img_data->alt,
            );
        }


        $img_with_out_alt = $count_img - $i;
        $total = $count_img . " | " . $img_with_out_alt;
        $total = explode(" | ", $total);
        $image['total'] = $total;
        $image['src_with_and_without_alt'] = $src_with_and_without_alt;

        return ($image);
    }

    public function get_missing_alt($html) {
        $image = array();

        foreach ($html->find('img')as $promo) {

            if (!isset($promo->alt) || ($promo->alt == "")) {

                $image[] = $promo->src;
            }
        }

        return $image;
    }

    //////////////////////////////////////////////////////////////get text/html ratio///////////////////////////////////////////////////////////////

    public function get_text_ratio($url) {
        $total_len = strlen($url);
        $page_size = round(($total_len / 1024), 2);
        $toparse = preg_replace('/(<script.*?>.*?<\/script>|<style.*?>.*?<\/style>|<.*?>|\r|\n|\t)/ms', '', $url);
        $toparse = preg_replace('/ +/ms', ' ', $toparse);
        $textlen = strlen($toparse);
        $text_size = round(($textlen / 1024), 2);
        $ratio = round((($text_size / $page_size) * 100), 2) . "%";
        return $ratio;
    }

    //////////////////////////////////////////////////////////////get Indexed Pages///////////////////////////////////////////////////////////////

    public function index_page($url) {

        $result = '';
//echo $url;
        $result_in = "http://www.google.com/search?q=site:$url";

        $result_in_html = $this->curl($result_in);

        $site_dom = $this->dom_parser->str_get_html($result_in_html);
        /// echo($site_dom);
        if (!empty($site_dom)) {
            foreach ($site_dom->find('div[id="resultStats"]') as $promo) {
                $result = $promo->innertext;
            }

            // echo($result);




            return $result;
        }

//        if (preg_match('/Results .*? of about (.*?) from/sim', $result_in_html, $regs)) {
//            $indexed_pages = trim(strip_tags($regs[1])); //use strip_tags to remove bold tags
//            return $indexed_pages;
//        } elseif (preg_match('/About (.*?) results/sim', $result_in_html, $regs)) {
//            $indexed_pages = trim(strip_tags($regs[1])); //use strip_tags to remove bold tags
//            return $indexed_pages;
//        } else {
//            return ($url) . 'Has Not Been Indexed @ Google.com!';
//        }
    }

    //////////////////////////////////////////////////////////////get popular pages///////////////////////////////////////////////////////////////

    public function popular_page($domain) {
        $tag = '';
        $total = '';
        $url2 = "http://www.google.com/search?q=site:$domain&start=0&num=4&gws_rd=ssl";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url2);
        $data = curl_exec($ch);
        curl_close($ch);
        $pg_url = $this->dom_parser->str_get_html($url2);
        foreach ($pg_url->find('h3.r a') as $promo) {
            $title = trim($promo->innertext);
            $link = $promo->href;
            $link = strrev($link);
            $link = substr($link, strripos($link, "&"));
            $link = strrev($link);
            $link = substr($link, 0, -1);
            if (!preg_match('/^[a-zA-Z]/', $link)) {
                $link = preg_replace('/^[a-zA-Z]/', "", $link);
                $link = str_replace("/url?q=", "", $link);
            }
            $tag = "<a href=" . $link . ">$title</a>" . "@|@" . $tag;
        }
        $total = explode("@|@", $tag);

        return $total;
    }

    //////////////////////////////////////////////////////////////get google  preview of any ur///////////////////////////////////////////////////////////////
    public function google_preview($url, $html) {


        $meta = $this->meta_description;
        $title = implode(" ", $this->title);
        $title = substr($title, 0, 75);
        $url = substr($url, 0, 40) . "..";

        //echo $title;
        // echo $meta;

        $body = preg_replace('/<!--(.|\s)*?-->/', '', $this->body_content);
        // remove style
        $body = preg_replace("/<style\\b[^>]*>(.*?)<\\/style>/s", "", $body);
        // remove script
        $body = preg_replace("/<script\\b[^>]*>(.*?)<\\/script>/s", "", $body);
        // remove noscript
        $body = preg_replace('#<noscript(.*?)>(.*?)</noscript>#is', '', $body);
        // remove tag
        $body = preg_replace('#<[^>]+>#', ' ', $body);

        $body = preg_replace("@\\b[a-z]\\b ?@i", "", $body);

        $body = substr($body, 0, 158);
        if (empty($meta)) { // same result as $test = ''
            $meta = $body;
        }



//		$body = preg_replace('/<!--(.|\s)*?-->/', '', $this->body_content);
//        // remove style
//        $body = preg_replace("/<style\\b[^>]*>(.*?)<\\/style>/s", "", $body);
//        // remove script
//        $body = preg_replace("/<script\\b[^>]*>(.*?)<\\/script>/s", "",$body);
//        // remove noscript
//        $body = preg_replace('#<noscript(.*?)>(.*?)</noscript>#is', '', $body);
//        // remove tag
//        $body = preg_replace('#<[^>]+>#', ' ',$body);
//
//        $body = preg_replace("@\\b[a-z]\\b ?@i", "",$body);
//		
//		
//        $title=  implode(" ", $this->title);
//       $query = $url;
//		$site = "http://ajax.googleapis.com/ajax/services/search/web?v=1.0&q=".$query;
//
//			$body1 = file_get_contents($site);
//			$json = json_decode($body1);
//			
//			for($x=0;$x<count($json->responseData->results);$x++){
//			$data['preview'] = $json->responseData->results[$x]->visibleUrl;
//			
//			//echo $url;
//			$site = substr($url,7);
//			
//			$str = strtok($site, '/');
//			$x1  = strstr($str,$data['preview']);
//			
//			
//			if($x1 == $data['preview']){
//			
//			$data['preview2'] = $site;
//			
//			$data['preview3'] = $json->responseData->results[$x]->title;
//			
//			$data['preview4'] = $json->responseData->results[$x]->content;
//			
//			
//			
//			return $data;
//			}
//		
//		else
//		
//		{
//		$gp_meta = '';
//        $gp_title = '';
//        $gp_main = '';
//        $gp_title = $this->gettitle($html);
//
//        foreach ($gp_title as $key => $value) {
//
//            $title = $value;
//            break;
//        }
//        $gp_url = $url;
//        $gp_meta = $this->get_meta_description($html);
//        foreach ($gp_meta as $key => $value) {
//
//            $meta = $value;
//            break;
//        }
//		if($meta != '' && isset($meta)){
//		
//		$gp_main = $title . "||" . $url . "||" . $meta;
//
//        $gp_main = explode("||", $gp_main);
//
//        return $gp_main;
//		}
//		else{
//
//		$cut = substr($body,0,158);
//	
//		
//		$gp_main = $title . "||" . $url . "||" . $cut;
//
//        $gp_main = explode("||", $gp_main);
//
//        return $gp_main;
//		}
//		}
// } 


        $gp_main = $title . "||" . $url . "||" . $meta;

        $gp_main = explode("||", $gp_main);

        return $gp_main;
    }

    //////////////////////////////////////////////////////////////get images internal external links///////////////////////////////////////////////////////////////



    public function page_link($url, $html) {
        $countlink = 0;
        $count_underscore = 0;
        $linkcount = 0;
        foreach ($html->find('a') as $element) {
            $linkcount++;
            if (preg_match('/_/', $element->href)) {
                $count_underscore++;
            }
            //echo $element->href . '<br>';
        }
//        foreach ($html->find('a') as $element) {
//            $linkcount++;
//        }
        $externallinks = $this->find_external_links($url);
        $internal_links = $linkcount - $externallinks;
        $external_links = $externallinks;
        $links_without_underscores = $linkcount - $count_underscore;
        $pagelink = $linkcount . "=" . $internal_links . "=" . $external_links . "=" . $links_without_underscores;
        $pagelink = explode("=", $pagelink);
        return $pagelink;
    }

    public function find_external_links($url) {
        $pUrl = parse_url($url);
        // Load the HTML into a DOMDocument
        $doc = new DOMDocument;
        @$doc->loadHTMLFile($url);
        // Look for all the 'a' elements
        $links = $doc->getElementsByTagName('a');
        $numLinks = 0;
        foreach ($links as $link) {
            // Exclude if not a link or has 'nofollow'
            preg_match_all('/\S+/', strtolower($link->getAttribute('rel')), $rel);
            if (!$link->hasAttribute('href') || in_array('nofollow', $rel[0])) {
                continue;
            }
            // Exclude if internal link
            $href = $link->getAttribute('href');
            if (substr($href, 0, 2) === '//') {
                // Deal with protocol relative URLs as found on Wikipedia
                $href = $pUrl['scheme'] . ':' . $href;
            }
            $pHref = @parse_url($href);
            if (!$pHref || !isset($pHref['host']) ||
                    strtolower($pHref['host']) === strtolower($pUrl['host'])
            ) {
                continue;
            }
            // Increment counter otherwise
            // echo 'URL: ' . $link->getAttribute('href') . "<br>";
            $numLinks++;
        }
        return $numLinks;
    }

    public function chart($total_link_no, $internal_link_no, $external_link_no) {
        $data['total'] = $total_link_no;
        $data['internal'] = $internal_link_no;
        $data['external'] = $external_link_no;
        $data['internal_per'] = round((( $internal_link_no / $total_link_no) * 100), 1);
        $data['external_per'] = round((( $external_link_no / $total_link_no) * 100), 1);
        return $data;
    }

    //////////////////////////////////////////////////////////////////
//
//    public function back_links_juice($url) {
//        
//
//        $pUrl = parse_url($url);
//        $domain = isset($pUrl['host']) ? $pUrl['host'] : '';
//        if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) {
//            $find = ($regs['domain']);
//        } else {
//            $find = substr($url, 11);
//        }
//        $find_int = "#";
//        // Load the HTML into a DOMDocument
//
//
//
//        $doc = new DOMDocument;
//        @$doc->loadHTMLFile($url);
//        // Look for all the 'a' elements
//        $links = $doc->getElementsByTagName('a');
//        $numLinks = 0;
//        $internal_count = 0;
//        $abc = '';
//        $internal_links = '';
//        $external_links = '';
//        $internal = 'Internal';
//        $external = 'External';
//        $xyz = '';
//
//        foreach ($links as $link) {
//
//            // Exclude if internal link
//            $href = $link->getAttribute('href');
//
//
//            $pos = strpos($href, $find);
//            $pos_has = strpos($href, $find_int);
//            $x = $link->getAttribute('href');
//            $slash = $link->getAttribute('href');
//            if ($pos != FALSE || (substr($x, 0, 1) === "#") || (substr($x, 0, 1) === "/")) {
//                if ($link->getAttribute("rel='nofollow'")) {
//
//                    $abc = "nofollow";
//                } else if ($link->getAttribute("rel='follow'")) {
//
//                    $abc = "dofollow";
//                } else if (!$link->getAttribute('rel')) {
//                    $abc = "dofollow";
//                }
//                if ($link->textContent) {
//                    $xyz = $link->textContent;
//                }
//              $head=$this->getstatus($href);
//              //$head = get_headers($robotlink, 1);
//
//              
//             $http_status = '0';
//             if (strpos($head, '200') !== false) {
//               $http_status = "200";
//              }
//               if (strpos($head, '404') !== false) {
//                   $http_status = '404';
//             }
//              if (strpos($head, '301') !== false) {
//                   $http_status = '301';
//             }
//              if (strpos($head, '302') !== false) {
//                   $http_status = '302';
//             }
//                if (substr($href, 0, 1) === "/") {
//                    $href = "http://www." . $find . "" . $href;
//                    $internal_links = $internal_links . "|" . $href . ",," . $internal . ",," . $abc . ",," . $xyz.",,".$http_status;
//                } else {
//                    $internal_count++;
//                    $internal_links = $internal_links . "|" . $href . ",," . $internal . ",," . $abc . ",," . $xyz.",,".$http_status;
//                }
//            }
//
//
//            // External link part start here....
//
//            $href = $link->getAttribute('href');
//            $pos = strpos($href, $find);
//            $x = $link->getAttribute('href');
//            if (!$pos == TRUE && substr($x, 0, 1) != "#") {
//
//                // Increment counter otherwise
//                $ext_href = $link->getAttribute('href');
//                $numLinks++;
//
//                if ($link->getAttribute("rel='nofollow'")) {
//                    $abc = "nofollow";
//                } else if ($link->getAttribute("rel='follow'")) {
//                    $abc = "dofollow";
//                } else if ($link->getAttribute('href') && !$link->getAttribute('rel')) {
//                    $abc = "dofollow";
//                }
//                if ($link->textContent) {
//                    $xyz = $link->textContent;
//                }
//                
//                              $head=$this->getstatus($href);
//                              
//
//                 $http_status = '0';
//             if (strpos($head, '200') !== false) {
//               $http_status = "200";
//              }
//               if (strpos($head, '404') !== false) {
//                   $http_status = '404';
//             }
//              if (strpos($head, '301') !== false) {
//                   $http_status = '301';
//             }
//              if (strpos($head, '302') !== false) {
//                   $http_status = '302';
//             }
//                if ((substr($href, 0, 1) === "/") || ($href == '')) {
//                    
//                } else {
//                    $external_links = $external_links . "|" . $ext_href . ",," . $external . ",," . $abc . ",," . $xyz.",,".$http_status;
//                }
//            }
//        }
//        //internal data
//        $internal_links = ltrim($internal_links, '|');
//        $tmp = explode('|', $internal_links);
//        $data = array();
//        foreach ($tmp as $k => $v) {
//            $data[] = explode(',,', $v);
//        }
//        //  print_r($data);
//        //external data
//        $external_links = ltrim($external_links, '|');
//        $tmp = explode('|', $external_links);
//        $data1 = array();
//        foreach ($tmp as $k => $v) {
//            $data1[] = explode(',,', $v);
//        }
//        //     print_r($data1);
//        //internal & external data merge in one array...          
//        $tot_links = array_merge($data, $data1);
//        return ($tot_links);
//    }
    //////////////////////////////////////////////////////////////get broken links details 404///////////////////////////////////////////////////////////////



    public function broken_links($html, $url) {

        foreach ($html->find("link") as $element) {
            $href = $element->href;


            if (stripos($href, "http://") === 0 || stripos($href, "https://") === 0) {
                
            } elseif (stripos($href, "//") === 0) {
                $href = "http:" . $href;
            } elseif (stripos($href, "/") === 0 || stripos($href, "?") === 0) {
                $href = $url . "/" . $href;
            } else {
                $href = $url . "/" . $href;
            }
            $head = get_headers($href, 1);

            if (stripos($head[0], '403') !== FALSE) {
                $link = $href;
                $code = 403;
                $staus = "Forbidden";
                $links[] = $link . "@@" . $staus . "@@" . $code;
            } elseif (stripos($head[0], '404') !== false) {
                $link = $href;
                $code = 404;
                $staus = "Not Found";
                $links[] = $link . "@@" . $staus . "@@" . $code;
            }
        }
        foreach ($html->find('a') as $element) {
            $href = $element->href;


            if (stripos($href, "http://") === 0 || stripos($href, "https://") === 0) {
                
            } elseif (stripos($href, "//") === 0) {
                $href = "http:" . $href;
            } elseif (stripos($href, "/") === 0 || stripos($href, "?") === 0) {
                $href = $url . "/" . $href;
            } else {
                $href = $url . "/" . $href;
            }
            $head = get_headers($href, 1);

            if (stripos($head[0], '403') !== FALSE) {
                $link = $href;
                $code = 403;
                $staus = "Forbidden";
                $links[] = $link . "@@" . $staus . "@@" . $code;
            } elseif (stripos($head[0], '404') !== false) {
                $link = $href;
                $code = 404;
                $staus = "Not Found";
                $links[] = $link . "@@" . $staus . "@@" . $code;
            }
        }

        if (!empty($links))
            $data['link'] = $links;
        else
            $data['link'] = 0;

        return $data;
    }

    //////////////////////////////////////////////////////////////get google backlinks///////////////////////////////////////////////////////////////



    public function GoogleBL($url) {

        $result = '';
//echo $url;
        $result_in = 'http://www.google.com/search?q=link:"' . $url . '"';



        //echo $result_in;
        $result_in_html = $this->curl($result_in);

        $site_dom = $this->dom_parser->str_get_html($result_in_html);
        /// echo($site_dom);

        foreach ($site_dom->find('div[id="resultStats"]') as $promo) {
            $result = $promo->innertext;
        }
        $result = str_replace('About', '', $result);
        $result = str_replace('results', '', $result);
        $result = trim($result);
        if ($result == NULL) {

            $result = 0;
        }
        //  echo($result);


        return $result;
    }

    //////////////////////////////////////////////////////////////www redirect backlinks///////////////////////////////////////////////////////////////


    public function redirect($Domain) {
        $redirect = new redirect_url();
        $redirect = $redirect->redirecturl($Domain);
        return $redirect;
    }

    //////////////////////////////////////////////////////////////find robots .txt file///////////////////////////////////////////////////////////////


    public function robotstxt() {
        $robotlink = $_POST['url'] . "/robots.txt";
        //$head = $this->getstatus($robotlink);
        $head = get_headers($robotlink, 1);
        $http_status = '';
        if (strpos($head, '200') !== false) {
            $http_status = "1";
        } else if (strpos($head, '301') !== false) {
            $http_status = "1";
        } else {
            $http_status = '0';
        }
        $total = $http_status . "@|@" . $robotlink;
        $total = explode("@|@", $total);
        $robots = $total;
        $robot_result = "";
        $count = 1;
        if (isset($robots)) {

            if ($robots[0] == '1') {
                $robot_result .= "Great, your website has a robots.txt file.<br>" . "<a href=$robots[1]  target=\"_blank\">" . $robots[1] . "</a><br>";

                $robots_data = '';
                $file = fopen("$robots[1]", "r");

                while (!feof($file)) {
                    $line = fgets($file, 1024);
                    /* This only works if the title and its tags are on one line */
                    $robots_data = $robots_data . " " . $line;
                }

                fclose($file);
                $robot_result .= "<div class=\"robots\">" . "<pre class=\"robots_font\">" . $robots_data . "</pre>" . "</div>";
            } else if ($robots[0] == '0') {
                $robot_result .="Bad, not found robots.txt file.<br>";
                $count = 0;
            } else {
                $robot_result .="Bad, not found robots.txt file.<br>";
                $count = 0;
            }
        } else {
            $robot_result .= "missing";
            $count = 0;
        }
        $robot = array(
            'robot_result' => $robot_result,
            'count' => $count
        );



        //$robots_txt_json = json_encode($robot);
        $robots_txt_json = "$robot_result###$count";
        $url_search_id = $_POST['id'];
        $ajax_txt_json = array("robots_txt" => "$robots_txt_json");
        $this->aorankmodel->update_data($url_search_id, $ajax_txt_json);





        echo $robots_txt_json; //ajax  6 done..
    }

    //////////////////////////////////////////////////////////////find  sidemap.xml file///////////////////////////////////////////////////////////////



    public function xmlsitemap() {

        $xml_link = $_POST['url'] . "/sitemap.xml";
        //echo $xml_link;

        $head = $this->getstatus($xml_link);



        //$head = get_headers($robotlink, 1);


        $http_status = '';
        if (strpos($head, '200') !== false) {
            $http_status = '1';
        } else {
            $http_status = '0';
        }

        $total = $http_status . "@|@" . $xml_link;
        $total = explode("@|@", $total);
        $xmlsitemap = $total;
        $xml_result = "";
        $count = 1;

        if (isset($xmlsitemap)) {

            if ($xmlsitemap[0] == '1') {
                $xml_result .= "Great, your website has a sitemap.xml file.<br>";
            } else if ($xmlsitemap[0] == '0') {
                $xml_result .="Bad, not found sitemap.xml file.<br>";
                $count = 0;
            } else {
                $xml_result .="Bad, not found sitemap.xml file.<br>";
                $count = 0;
            }
        } else {
            $xml_result .= "Missing";
            $count = 0;
        }

        $xml = array(
            'sitemap' => $xml_result,
            'count' => $count
        );


        $sitemap_xml_json = json_encode($xml);
        $url_search_id = $_POST['id'];
        $ajax_xml_json = array("sitemap_xml" => "$sitemap_xml_json");
        $this->aorankmodel->update_data($url_search_id, $ajax_xml_json);






        echo $sitemap_xml_json;  // ajax 7..
    }

    //////////////////////////////////////////////////////////////url rewrite///////////////////////////////////////////////////////////////


    public function url_rewrite($url) {

        if (parse_url($url, PHP_URL_QUERY)) {
            return false;
        } else {
            return true;
        }
    }

    //////////////////////////////////////////////////////////////url underscore///////////////////////////////////////////////////////////////


    public function url_underscores($url) {


        if (strpos($url, '_') !== false) {
            return true;
        } else {

            return false;
        }
    }

    //////////////////////////////////////////////////////////////find domain info///////////////////////////////////////////////////////////////


    public function whois_info() {

        $Domain1 = $_POST['src_url'];
        $age = $_POST['domain'];
        $whois = new Whois;
        $str = $whois->whoislookup($Domain1);
        $tmp = explode("\r\n\r\n", $str, 2);
        $whoIs = $tmp[0];
        $cut_data = explode("\r\n", $whoIs);
        $whoIs_info = array();
        foreach ($cut_data as $row) {
            $whoIs_data = explode(":", $row, 2);
            $whoIs_info[@$whoIs_data[0]] = @$whoIs_data[1];
        }

        if (isset($whoIs_info['Registrar Registration Expiration Date'])) {
            $exp_date = $whoIs_info['Registrar Registration Expiration Date'];

            $curr_date = date("Y-m-d h:i:s");
            $date1 = new DateTime($curr_date);
            $date2 = new DateTime($exp_date);
            $interval = $date1->diff($date2);
            $exp_time = $interval->y . " year(s), " . $interval->m . " month(s) and " . $interval->d . " day(s) ";
            $whoIs_info['expiration'] = $exp_time;
        }

        $d_age = $this->domainage($age);
        $whoIs_info["Domain Age"] = $d_age;
        $result = "";
        $owner_info = "";
        $result .= "<br><div class=\"mycss\">Domain Info</div><br>";
        if (isset($whoIs_info['Creation Date'])) {
            $x = date_parse($whoIs_info['Creation Date']);
            $result .="<li><p>Creation Date : " . $x['day'] . "/" . $x['month'] . "/" . $x['year'] . "</p>";
        }
        if (isset($whoIs_info["Registrar Registration Expiration Date"])) {
            $x = date_parse($whoIs_info['Registrar Registration Expiration Date']);
            $result .= "<p>Expiration Date: " . $x['day'] . "/" . $x['month'] . "/" . $x['year'] . "</p>";
        }
        if (isset($whoIs_info["Domain Age"]) && ($whoIs_info["Domain Age"] != NULL)) {

            $result .= '<p>Domain Age: ' . $whoIs_info["Domain Age"] . "</p></li>";
        }


        if (isset($whoIs_info['Registrant Name'])) {
//            $owner_info .= "<br><hr><br><div class=\"mycss\">Website Registrar Info</div><br><li><p>Name: " . $whoIs_info['Registrant Name'] . "</p>";
//        }
//        if (isset($whoIs_info['Registrant Email'])) {
//            $owner_info .= "<p>Email: " . $whoIs_info['Registrant Email'] . "</p>";
//        }
//        if (isset($whoIs_info['Registrant Phone'])) {
//           $owner_info .= "<p>Phone: " . $whoIs_info['Registrant Phone'] . "</p>";
//        }
//        if (isset($whoIs_info['Registrant Street'])) {
//            $owner_info .= "<p>Address: " . $whoIs_info['Registrant Street'];
//        }
//        if (isset($whoIs_info['Registrant City'])) {
//            $owner_info .= $whoIs_info['Registrant City'];
//        }
//
//        if (isset($whoIs_info['Registrant Postal Code'])) {
//            $owner_info .= "-" . $whoIs_info['Registrant Postal Code'] . ",";
//        }
//        if (isset($whoIs_info['Registrant State/Province'])) {
//            $owner_info .= $whoIs_info['Registrant State/Province'];
//        }
//        if (isset($whoIs_info['Registrant Country'])) {
//            $owner_info .= "" . $whoIs_info['Registrant Country'] . "</p></li>";
        }

        $who_is_full_info = array(
            'time_info' => $result,
            'owner_info' => $owner_info
        );
        $who_is_full_info = "$result###$owner_info";

        $who_is_full_info_json = $who_is_full_info;
        $url_search_id = $_POST['id'];
        $ajax_who_is_full_info = array("who_is_full_info" => "$who_is_full_info_json");
        $this->aorankmodel->update_data($url_search_id, $ajax_who_is_full_info);


        echo $who_is_full_info_json; // ajax 8 .. done
    }

    public function domainage($age) {
        $w = new Domainage();
        $str = $w->age($age);
        return $str;
    }

    //////////////////////////////////////////////////////////////find url count total numberof char///////////////////////////////////////////////////////////////



    public function url_char_count($url) {
        $url = trim($url, "http://www.");

        return strlen($url);
    }

    //////////////////////////////////////////////////////////////find favicon///////////////////////////////////////////////////////////////


    public function get_favicon($html, $Domain1) {
        //Check for Fevicon

        $count = 0;
        $app_icon = array();
        foreach ($html->find('link') as $element) {
            if ($element->rel == 'shortcut icon' || $element->rel == 'icon') {
                $app_icon[] = $element->href;
                $count++;
            }
        }

        //Check for Fevicon

        $favicon = parse_url(@$app_icon[0]);
        $fav_len = strlen(@$app_icon[0]);
        if ($fav_len > 0) {
            if (isset($favicon['scheme'])) {
                $src = @$app_icon[0];
            } else {
                if (substr($favicon['path'], 0, 4) === "www.") {
                    $src = $favicon['path'];
                } else {
                    $src = "http://www." . $Domain1 . "/" . $favicon ['path'];
                }
            }
        } else {
            $url = "https://www." . $Domain1 . "/favicon.ico";
            $src_info = get_headers($url, 1);
            if ($src_info[0] == 'HTTP/1.0 404 Not Found') {
                $src = 0;
            } else {
                $src = $url;
            }
        }
        return $src;
    }

    //////////////////////////////////////////////////////////////custom 404///////////////////////////////////////////////////////////////



    public function checkurl($url) {
        if (!$url) {
            return FALSE;
        }
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_exec($ch);

        if (curl_getinfo($ch, CURLINFO_HTTP_CODE) == 404) {
            curl_close($ch);
            return FALSE;
        } else {
            curl_close($ch);
            return TRUE;
        }
        return FALSE;
    }

    //////////////////////////////////////////////////////////////find convirsion form set or not///////////////////////////////////////////////////////////////




    public function conversion_forms($html) {

        $form = '';
        foreach ($html->find('form') as $element) {

            $form++;
        }

        return $form;
    }

    //////////////////////////////////////////////////////////////find page size///////////////////////////////////////////////////////////////


    public function headers($curl_ob) {
        $tmp = explode("\r\n\r\n", $curl_ob, 2);
        $rawHeader = @$tmp[0];
        $response = @$tmp[1];
        echo "<br/>";
        $cutHeaders = explode("\r\n", $rawHeader);
        $headers = array();
        foreach ($cutHeaders as $row) {
            $cutRow = explode(":", $row, 2);
            $headers[@$cutRow[0]] = @$cutRow[1];
        }
        return $headers;
    }

    //////////////////////////////////////////////////////////////find load time///////////////////////////////////////////////////////////////

    public function load_time($url) {

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_exec($ch);

        $x = curl_getinfo($ch);
//print_r($x);
        $size = $x['size_download'];
        $start4 = microtime(true);
        $dns = $x['namelookup_time'];
        $end4 = microtime(true);
        $dns1 = number_format($end4 - $start4, 10);
        $dns = round($dns1 * 10000, 3);
        $con_time = round($x['connect_time'], 3);

        $wait_time = round(($x['pretransfer_time'] + $x['starttransfer_time']), 3);

        $file_load = round($x['total_time'], 3);

        $total = ($x['namelookup_time'] + $x['connect_time'] + $x['pretransfer_time'] + $x['starttransfer_time'] + $x['total_time']);
        $total = $total;

        $load = array(
            'dns' => $dns,
            'con_time' => $con_time,
            'waiting_time' => $wait_time,
            'file_load' => $file_load,
            'total' => $total,
            'size' => $size
        );

        return $load;
    }

    //////////////////////////////////////////////////////////////find language///////////////////////////////////////////////////////////////


    public function language($url, $html) {
        $langs1 = array(
            'ab' => 'Abkhazian',
            'aa' => 'Afar',
            'af' => 'Afrikaans',
            'sq' => 'Albanian',
            'am' => 'Amharic',
            'ar' => 'Arabic',
            'an' => 'Aragonese',
            'hy' => 'Armenian',
            'as' => 'Assamese',
            'ay' => 'Aymara',
            'az' => 'Azerbaijani',
            'ba' => 'Bashkir',
            'eu' => 'Basque',
            'bn' => 'Bengali (Bangla)',
            'dz' => 'Bhutani',
            'bh' => 'Bihari',
            'bi' => 'Bislama',
            'br' => 'Breton',
            'bg' => 'Bulgarian',
            'my' => 'Burmese',
            'be' => 'Byelorussian (Belarusian)',
            'km' => 'Cambodian',
            'ca' => 'Catalan',
            'zh' => 'Chinese (Simplified)',
            'co' => 'Corsican',
            'hr' => 'Croatian',
            'cs' => 'Czech',
            'da' => 'Danish',
            'nl' => 'Dutch',
            'en' => 'English',
            'eo' => 'Esperanto',
            'et' => 'Estonian',
            'fo' => 'Faeroese',
            'fa' => 'Farsi',
            'fj' => 'Fiji',
            'fi' => 'Finnish',
            'fr' => 'French',
            'fy' => 'Frisian',
            'gl' => 'Galician',
            'gd' => 'Gaelic (Scottish)',
            'gv' => 'Gaelic (Manx)',
            'ka' => 'Georgian',
            'de' => 'German',
            'el' => 'Greek',
            'kl' => 'Greenlandic',
            'gn' => 'Guarani',
            'gu' => 'Gujarati',
            'ht' => 'Haitian Creole',
            'ha' => 'Hausa',
            'he' => 'Hebrew',
            'hi' => 'Hindi',
            'hu' => 'Hungarian',
            'is' => 'Icelandic',
            'io' => 'Ido',
            'id' => 'Indonesian',
            'ia' => 'Interlingua',
            'ie' => 'Interlingue',
            'iu' => 'Inuktitut',
            'ik' => 'Inupiak',
            'ga' => 'Irish',
            'it' => 'Italian',
            'ja' => 'Japanese',
            'jv' => 'Javanese',
            'kn' => 'Kannada',
            'ks' => 'Kashmiri',
            'kk' => 'Kazakh',
            'rw' => 'Kinyarwanda (Ruanda)',
            'ky' => 'Kirghiz',
            'rn' => 'Kirundi (Rundi)',
            'ko' => 'Korean',
            'ku' => 'Kurdish',
            'lo' => 'Laothian',
            'la' => 'Latin',
            'lv' => 'Latvian (Lettish)',
            'li' => 'Limburgish ( Limburger)',
            'ln' => 'Lingala',
            'lt' => 'Lithuanian',
            'mk' => 'Macedonian',
            'mg' => 'Malagasy',
            'ms' => 'Malay',
            'ml' => 'Malayalam',
            'mt' => 'Maltese',
            'mi' => 'Maori',
            'mr' => 'Marathi',
            'mo' => 'Moldavian',
            'mn' => 'Mongolian',
            'na' => 'Nauru',
            'ne' => 'Nepali',
            'no' => 'Norwegian',
            'oc' => 'Occitan',
            'or' => 'Oriya',
            'om' => 'Oromo (Afaan Oromo)',
            'ps' => 'Pashto (Pushto)',
            'pl' => 'Polish',
            'pt' => 'Portuguese',
            'pa' => 'Punjabi',
            'qu' => 'Quechua',
            'rm' => 'Rhaeto-Romance',
            'ro' => 'Romanian',
            'ru' => 'Russian',
            'sm' => 'Samoan',
            'sg' => 'Sangro',
            'sa' => 'Sanskrit',
            'sr' => 'Serbian',
            'sh' => 'Serbo-Croatian',
            'st' => 'Sesotho',
            'tn' => 'Setswana',
            'sn' => 'Shona',
            'ii' => 'Sichuan YI',
            'sd' => 'Sindhi',
            'si' => 'Sinhalese',
            'ss' => 'Siswati',
            'sk' => 'Slovak',
            'sl' => 'Slovenian',
            'so' => 'Somali',
            'es' => 'Spanish',
            'su' => 'Sundanese',
            'sw' => 'Swahili (Kiswahili)',
            'sv' => 'Swedish',
            'tl' => 'Tagalog',
            'tg' => 'Tajik',
            'ta' => 'Tamil',
            'tt' => 'Tatar',
            'te' => 'Telugu',
            'th' => 'Thai',
            'bo' => 'Tibetan',
            'ti' => 'Tigrinya',
            'to' => 'Tonga',
            'ts' => 'Tsonga',
            'tr' => 'Turkish',
            'tk' => 'Turkmen',
            'tw' => 'Twi',
            'ug' => 'Uighur',
            'uk' => 'Ukrainian',
            'ur' => 'Urdu',
            'uz' => 'Uzbek',
            'vi' => 'Vietnamese',
            'vo' => 'Volapük',
            'wa' => 'Wallon',
            'cy' => 'Welsh',
            'wo' => 'Wolof',
            'xh' => 'Xhosa',
            'yi' => 'Yiddish',
            'yo' => 'Yoruba',
            'zu' => 'Zulu'
        );




        foreach ($html->find('html') as $langs) {

            $lang = $langs->lang;
        }

        if (isset($lang)) {

            $lang = substr($lang, 0, 2);
            $lang = strtolower($lang);


            if ($lang != "") {
                $data['langs'] = ($langs1[$lang]);
                return $data['langs'];
            }
        }
    }

    public function get_lang($site, $html) {



// Execute
        $m1 = $html->find('meta');

        $m2 = $html->find('meta[http-equiv=Content-Type]', 0);


        $lang = array(
            'gb2312' => 'Chinese Simplified',
            'shift_jis' => 'Japanese',
            'asmo-708' => 'Arabic',
            'dos-720' => 'Arabic',
            'iso-8859-6' => 'Arabic',
            'x-mac-arabic' => 'Arabic',
            'windows-1256' => 'Arabic',
            'ibm775' => 'Baltic',
            'iso-8859-4' => 'Baltic',
            'windows-1257' => 'Baltic',
            'ibm852' => 'Central European',
            'iso-8859-2' => 'Central European',
            'x-mac-ce' => 'Central European',
            'windows-1250' => 'Central European',
            'euc-cn' => 'Chinese Simplified',
            'hz-gb-2312' => 'Chinese Simplified',
            'x-mac-chineseimp' => 'Chinese Simplified',
            'big5' => 'Chinese Traditional',
            'x-chinese-cns' => 'Chinese Traditional',
            'x-chinese-eten' => 'Chinese Traditional',
            'cp866' => 'Cyrillic',
            'iso-8850-5' => 'Cyrillic',
            'koi8-r' => 'Cyrillic',
            'koi8-u' => 'Cyrillic',
            'x-mac=cyrillic' => 'Cyrillic',
            'windows-1251' => 'Cyrillic',
            'x-europa' => 'Europa',
            'x-ia5-german' => 'German',
            'ibm737' => 'Greek',
            'iso-8859-7' => 'Greek',
            'x-mac-greek' => 'Greek',
            'windows-1253' => 'Greek',
            'ibm869' => 'Greek Modern',
            'dos-862' => 'Hebrew',
            'iso-8859-8-i' => 'Hebrew',
            'iso-8859-8' => 'Hebrew',
            'x-mac-hebrew' => 'Hebrew',
            'windows-1255' => 'Hebrew',
            'x-ebcdic-arabic' => 'Arabic',
            'x-ebcdic-cyrillicRussian' => 'Cyrillic Russian',
            'x-ebcdic-cyrillicSerbianBulgarian' => 'Cyrillic Serbian-Bulgarian',
            'x-ebcdic-denmarkNorway' => 'Denmark-Norway',
            'x-ebcdic-denmarknorway-euro' => 'Denmark-Norway-Euro',
            'x-ebcdic-finalSweden' => 'Finland-Sweden',
            'x-ebcdic-finlandsweden-euro' => 'Finland-Sweden-Euro',
            'x-ebcdic-germany-euro' => 'France-Euro',
            'x-ebcdic-germany' => 'Germany',
            'x-ebcdic-germany-euro' => 'Germany Euro',
            'x-ebcdic-germany' => 'Greek Modern',
            'x-ebcdic-greek' => 'Greek',
            'x-ebcdic-hebrew' => 'Hebrew',
            'x-ebcdic-icelandic' => 'Icelandic',
            'x-ebcdic-icelandic-euro' => 'Icelandic-Euro',
            'x-ebcdic-international-euro' => 'International-Euro',
            'x-ebcdic-italy' => 'Italy',
            'x-ebcdic-italy-euro' => 'Italy-Euro',
            'x-ebcdic-japaneseandkana' => 'Japanese and Japanese Kaakana',
            'x-ebcdic-japaneseandjapaneselatin' => 'Japanese and Japanese-Latin',
            'x-ebcdic-japaneseanduscanada' => 'Japanese and US-Canada',
            'x-ebcdic-japanesekatakana' => 'Japanese katakana',
            'x-ebcdic-koreanandkoreanextended' => 'Korean and Korean Extended',
            'x-ebcdic-koreanextended' => 'Korean Extended',
            'cp870' => 'Multilingual Latin-2',
            'x-ebcdic-simplifiedchinese' => 'Simplified Chinese',
            'x-ebcdic-spain' => 'Spain',
            'x-ebcdic-spain-euro' => 'Spain-Euro',
            'x-ebcdic-thai ' => 'Thai',
            'x-ebcdic-traditionalchinese' => 'Traditional Chinese',
            'cp1026' => 'Trukish Chinese',
            'x-ebcdic-turkish ' => 'Turkish',
            'x-ebcdic-uk' => 'UK',
            'x-ebcdic-uk-euro' => 'UK-Euro',
            'ebcdic-cp-us' => 'US-Canada',
            'x-ebcdic-cp-us-euro' => 'US-Canada-Euro',
            'ibm861' => 'Icelandic',
            'x-mac-icelandic' => 'Icelandic',
            'x-iscii-as' => 'Assamese',
            'x-iscii-be' => 'Bengali',
            'x-iscii-de' => 'Devanagari',
            'x-iscii-gu' => 'Gujarathi',
            'x-iscii-ka' => 'Kannada',
            'x-iscii-ma' => 'Malayalam',
            'x-iscii-or' => 'Oriya',
            'x-iscii-pa' => 'Punjabi',
            'x-iscii-ta' => 'Tamil',
            'x-iscii-te' => 'Telugu',
            'x-euc-jp' => 'Japanese',
            'iso-2022-jp' => 'Japanese',
            'csISO2022JP' => 'Japanese',
            'x-mac-japanese' => 'Japanese',
            'shift_jis' => 'Japanese',
            'ks_c_5601-1987' => 'Korean',
            'euc-kr' => 'Korean',
            'iso-2022-kr' => 'Korean',
            'johab' => 'Korean',
            'x-mac-korean' => 'Korean',
            'iso-8859-3' => 'Latin 3',
            'iso-8859-15' => 'Latin 9',
            'x-ia5-norwegian' => 'Norwegian',
            'ibm437' => 'OEM United States',
            'x-ia5-swedish' => 'Swedish',
            'windows-874' => 'Thai',
            'ibm857' => 'Turkish',
            'iso-8859-9' => 'Turkish',
            'x-mac-turkish' => 'Turkish',
            'windows-1254' => 'Turkish',
            'unicode' => 'No Language Detected',
            'unicodefffe' => 'No Language Detected',
            'utf-7' => 'No Language Detected',
            'utf-8' => 'No Language Detected',
            'us-ascii' => 'US-ASCII',
            'windows-1258' => 'Vietnamese',
            'ibm850' => 'Western European',
            'x-ia5' => 'Western European',
            'iso-8859-1' => 'Western European',
            'macintosh' => 'Western European',
            'windows-1252' => 'Western European'
        );


        if (isset($m2)) {

            $el = $html->find('meta[http-equiv=Content-Type]', 0);
            $fullvalue = $el->content;
            preg_match('/charset=(.+)/', $fullvalue, $matches);
            $a = ($matches[1]);

            $a = strtolower($a);


            $data['lang'] = $lang[$a];

            return $data['lang'];
        } else {

            if (isset($m1)) {
                $data = array();
                $meta1 = array();
                $char = array();
                foreach ($html->find('meta') as $chars) {

                    $char[] = $chars->charset;
                }

                if (isset($char[0]) && $char[0] != '') {


                    if (array_values($char) == " ") {
                        
                    } else {
                        $meta1 = $char[0];
                    }

                    $a = strtolower($meta1);


                    if ($a != '') {
                        $data['lang'] = $lang[$a];
                        return $data['lang'];
                    }
                }
            }
        }
    }

    //////////////////////////////////////////////////////////////find server and ip location///////////////////////////////////////////////////////////////

    public function get_page_ip($Domain) {
        $ip = gethostbyname($Domain);

        $geoplugin = new geoPlugin();
        $geoplugin->locate($ip);

        $ip_info = array(
            'no_of_ip' => $geoplugin->ip,
            'city' => $geoplugin->city,
            'Country' => $geoplugin->countryName
        );
        return($ip_info);
    }

    //////////////////////////////////////////////////////////////find used technology///////////////////////////////////////////////////////////////




    public function technology($url1) {

        $header = get_headers($url1, 1);
        $head = array(
            'server' => @$header['Server'],
            'technology' => @$header['X-Powered-By']
        );
        return $head;
    }

    //////////////////////////////////////////////////////////////find speed tips inline css internal table etc///////////////////////////////////////////////////////////////



    public function speed_tips($url, $html) {
        global $inlinecss;
        foreach ($html->find("*") as $element) {
            if ($element->style) {

                $inlincss++;
            } else {

                $inlinecss = 0;
                $styletag = 0;
                $tabletag = 0;
            }
        }
        $incss = "$inlinecss inline CSS found";
        foreach ($html->find('style') as $element) {

            $styletag++;
        }
        $styletags = "$styletag style tag is found";
        foreach ($html->find('table') as $element) {

            $tabletag++;
        }

        $tabletags = "$tabletag table tag is found";
        $total = $incss . " | " . $styletags . " | " . $tabletags;
        $total = explode(" | ", $total);
        return $total;
    }

    //////////////////////////////////////////////////////////////find google analytics is used or not///////////////////////////////////////////////////////////////
    public function google_analytic($curl_ob) {
        $content = $curl_ob;
        $flag1_trackpage = false;
        $script_regex = "/<script\b[^>]*>([\s\S]*?)<\/script>/i";
        $ua_regex = "/UA-[0-9]{5,}-[0-9]{1,}/";
        preg_match_all($script_regex, $content, $inside_script);
        for ($i = 0; $i < count($inside_script[0]); $i++) {
            if (stristr($inside_script[0][$i], "UA"))
                $inside_script[0][$i];
            $flag2_ga_js = TRUE;
            if (stristr($inside_script[0][$i], "_trackPageview"))
                $inside_script[0][$i];
            $flag1_trackpage = TRUE;
        }

        preg_match_all($ua_regex, $content, $ua_id);
        if ($flag2_ga_js && $flag1_trackpage && count($ua_id > 0))
            return($ua_id);
        else
            return(NULL);
    }

    public function wordpress($html) {
        $wpress = array();
        foreach ($html->find("meta[name='generator']") as $element) {
            $wpress[] = $element->content;
        }
        return($wpress);
    }

    public function diff_analytic($html) {
        $script = array();
        $src = array();
        foreach ($html->find('script') as $element) {
            $script[] = $element->innertext;
            $src[] = $element->type;
        }
        $tot_script = count($script);
        $analytic = array();
        $NREUM = 0;
        $heap = 0;
        $woopra = 0;
        $comscore = 0;
        $js = 0;
        $comscore = 0;
        for ($i = 0; $i < $tot_script; $i++) {
            if (preg_match("~\bNREUM\b~", $script[$i])) {
                $NREUM++;
            }
        }
        for ($i = 0; $i < $tot_script; $i++) {
            if (preg_match("~\bheap\b~", $script[$i])) {
                $heap++;
            }
        }

        for ($i = 0; $i < $tot_script; $i++) {
            if (preg_match("~\bwoopra\b~", $script[$i])) {
                $woopra++;
            }
        }

        for ($i = 0; $i < $tot_script; $i++) {
            if (preg_match("~\b_comscore\b~", $script[$i])) {
                $comscore++;
            }
        }

        for ($i = 0; $i < $tot_script; $i++) {
            if (preg_match("~\btext/javascript\b~", $src[$i])) {
                $js++;
            }
        }

        $analytic = array(
            'NREUM' => $NREUM,
            'heap' => $heap,
            'woopra' => $woopra,
            'comscore' => $comscore,
            'js' => $js
        );

        return $analytic;
    }

    //////////////////////////////////////////////////////////////find w3c errors///////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////find what document type used like html5 or xhtml etc///////////////////////////////////////////////////////////////

    public function doctype($curl_ob) {
        $html = $curl_ob;


        $html = str_replace("\n", "", $html);
        $str = strstr($html, "<!DOCTYPE html>");
        $get_doctype = preg_match("/<!DOCTYPE html>/", $html);
        if ($get_doctype == 1) {
            $data = "html5";
            return $data;
        }
        $get_doctype = strripos($html, '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">');
        if ($get_doctype != 0) {
            $data = "xhtml1.1";
            return $data;
        }
        $get_doctype = strripos($html, '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">');
        if ($get_doctype != 0) {
            $data = "xhtml1.0-strict";
            return $data;
        }
        $get_doctype = strripos($html, '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">');
        if ($get_doctype != 0) {
            $data = "xhtml1.0-Transitional";
            return $data;
        }
        $get_doctype = strripos($html, '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">');
        if ($get_doctype != 0) {
            $data = "xhtml1.0-Frameset";
            return $data;
        }
        $get_doctype = strripos($html, '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">');
        if ($get_doctype != 0) {
            $data = "html4.01-strict";
            return $data;
        }
        $get_doctype = strripos($html, '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">');
        if ($get_doctype != 0) {
            $data = "html4.01-Transitional";
            return $data;
        }
        $get_doctype = strripos($html, '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">');
        if ($get_doctype != 0) {
            $data = "html4.01-Frameset";
            return $data;
        }
    }

    //////////////////////////////////////////////////////////////find encoding type like utf8///////////////////////////////////////////////////////////////



    public function encoding($html) {
        if ($html->find('meta[charset]')) {
            $total = '';
            foreach ($html->find('meta[charset]') as $element) {

                $total = $element->charset;

                return $total;
                //$total = $element;
                //  break;
            }
        } else if ($html->find('meta[http-equiv]')) {

            $el = $html->find('meta[http-equiv=Content-Type]', 0);
            $fullvalue = $el->content;
            $x = preg_match('/charset=(.+)/', $fullvalue, $matches);
            return $matches[1];
        }
    }

    //////////////////////////////////////////////////////////////find Deprecated HTML like menu nav///////////////////////////////////////////////////////////////

    public function dep($html) {

        $total = '';
        $d1 = '';
        $d2 = '';
        $d3 = '';
        $d4 = '';
        $d5 = '';
        $dep_tag = '';
        foreach ($html->find('*') as $element) {

            $total = $total . " " . $element->tag;
        }


        if (strpos($total, 'menu') !== false) {

            $d1 = "Menu tag found Replace with the &#60ul&#62 &#60/ul&#62 tag";
        }
        if (strpos($total, 'applet') !== false) {

            $d2 = "applet tag found used Alternate &#60objec&#62";
        }
        if (strpos($total, 'center') !== false) {

            $d3 = "center tag found used Alternate text-align css";
        }
        if (strpos($total, 'font') !== false) {

            $d4 = "font tag found used Alternate font-family, font-size";
        }
        if (strpos($total, 'xmp') !== false) {

            $d5 = "xmp tag found used Alternate &#60/pre&#62 tag ";
        }

        $dep_tag = $d1 . " | " . $d2 . " | " . $d3 . " | " . $d4 . " | " . $d5;

        $dep_tag = explode(" | ", $dep_tag);

        return $dep_tag;
    }

    //////////////////////////////////////////////////////////////find social share count///////////////////////////////////////////////////////////////

    public function get_facebook() {
        $url = $_POST['social_url'];
        $data = array(
            'url' => $url
        );
        $this->load->library('shareCount', $data);
        $abc = new shareCount($data);

        $fb = $abc->get_fb();

        $tw = $abc->get_tweets();

        $link = $abc->get_linkedin();
        $pin = $abc->get_pinterest();
        $gp = $abc->get_plusones();
        $delicious = $abc->get_delicious();
        $Stumbleupon = $abc->get_stumble();

        $result = "";
        if (isset($fb)) {
            $result .= "<tr><td><img src=" . base_url() . "/images/fb.png> Facebook</td><td>" . $fb . "</td></tr>";
        }
        if (isset($fb)) {
            $result .= "<tr><td><img src=" . base_url() . "/images/tw.png> Tweeter</td><td>" . $tw . "</td></tr>";
        }
        if (isset($fb)) {
            $result .= "<tr><td><img src=" . base_url() . "/images/link.png> Linkedin</td><td>" . $link . "</td></tr>";
        }
        if (isset($fb)) {
            $result .= "<tr><td><img src=" . base_url() . "/images/pin.png> Pinterest</td><td>" . $pin . "</td></tr>";
        }
        if (isset($fb)) {
            $result .= "<tr><td><img src=" . base_url() . "/images/gp.png> Google plus</td><td>" . $gp . "</td></tr>";
        }
        if (isset($fb)) {
            $result .= "<tr><td><img src=" . base_url() . "/images/de.jpg> Delicious</td><td>" . $delicious . "</td></tr>";
        }
        if (isset($fb)) {
            $result .= "<tr><td><img src=" . base_url() . "/images/stm.png> Stumbleupon</td><td>" . $Stumbleupon . "</td></tr>";
        }



        echo $result;  // ajax 9 ..
    }

    //////////////////////////////////////////////////////////////find facebook page info///////////////////////////////////////////////////////////////
    public function facebook_page($url1) {

        $url = trim($url1, "http://www.");
        $url = trim($url, ".com");
        $data = file_get_contents("http://graph.facebook.com/" . $url);
        $array = json_decode($data, true);

        $a = $array['cover'];

        $d1 = $a['source'];
        $d2 = $array['likes'];
        $d3 = $array['talking_about_count'];
        $d4 = $array['link'];
        $d5 = $array['name'];
        $d6 = $array['about'];


        $total = $d1 . " | " . $d2 . " | " . $d3 . " | " . $d4 . " | " . $d5 . " | " . $d6;

        $total = explode(" | ", $total);

        return $total;
    }

    //////////////////////////////////////////////////////////////find google page rank///////////////////////////////////////////////////////////////



    public function get_google_pr() {
        $url1 = $_POST['p_url'];
        $pagerank = new pr();
        $pagerank = $pagerank->get_google_pagerank($url1);


        $url_search_id = $_POST['id'];
        $ajax_pagerank_data = array("pagerank_data" => "$pagerank");
        $this->aorankmodel->update_data($url_search_id, $ajax_pagerank_data);





        echo $pagerank;  // ajax 10 ...
    }

    public function get_google_pr_demo($link_url) {
        $pagerank = new pr();
        $pagerank = $pagerank->get_google_pagerank($link_url);
        return $pagerank;
    }

    //////////////////////////////////////////////////////////////find alexa rank///////////////////////////////////////////////////////////////

    public function get_alexa_rank() {

        $url1 = $_POST['src_url'];
        $alexa_rank = new alexa();
        $alexa_rank = $alexa_rank->get_alexa($url1);
        $data = "";
        if ($alexa_rank) {
            $data.= "Global Rank:- " . $alexa_rank['global'] . "<br>";
            $data.= $alexa_rank['country'] . "  rank:- " . $alexa_rank['c_rank'] . "<br>";

            $url_search_id = $_POST['id'];
            $ajax_alexa_rank = array("alexa_rank" => "$data");
            $this->aorankmodel->update_data($url_search_id, $ajax_alexa_rank);



            echo $data; //ajax 11 ..
        } else {

            $url_search_id = $_POST['id'];
            $ajax_alexa_rank = array("alexa_rank" => "$data");
            $this->aorankmodel->update_data($url_search_id, $ajax_alexa_rank);


            echo "Missing";  // ajax 11 ..
        }
    }

    //////////////////////////////////////////////////////////////find Visitors Localization///////////////////////////////////////////////////////////////

    public function get_country2() {
        /*

          $site1 = $_POST['country2_url'];
          //echo $site1;
          $site = "http://www.alexa.com/siteinfo/" . $site1;
          $curl_ob2 = $this->curl($site);
          $html = $this->dom_parser->str_get_html($curl_ob2);
          ////////////////
          $country2 = array();
          $es = 0;
          foreach ($html->find('tr td span') as $country) {
          $es = $es + 1;
          if ($es <= 10) {
          $country2[] = $country->innertext;
          }
          }

          $country3 = array();
          $country4 = array();
          $country1 = array();
          $es = 0;
          foreach ($html->find('tr td a') as $country) {
          $es = $es + 1;
          if ($es <= 5) {

          $country1[] = $country->innertext;
          $country3[] = $country->innertext;
          }
          }
          $country1 = preg_replace("/<img[^>]+\>/i", "", $country1);
          $country4[] = $country1;
          $country4[] = $country3;
          $country_name = array();
          $result = "";
          if (isset($country2) && isset($country4)) {
          $result .= "<thead><tr><th>Country</th><th>Percent of Visitors</th><th>Rank in Country</th></tr></thead><tr><td>";
          if (preg_match('/<img/', $country4[1][0])) {

          $result .= $country4[1][0] . "</td><td>" . $country2[0] . "</td><td>" . $country2[1] . "</tr>";
          $country_name[] = str_replace('&nbsp;', '', $country4[0][0]);
          }
          if (preg_match('/<img/', $country4[1][1])) {
          $result .= "<tr><td>" . $country4[1][1] . "</td><td>" . $country2[2] . "</td><td>" . $country2[3] . "</tr>";
          $country_name[] = str_replace('&nbsp;', '', $country4[0][1]);
          }
          if (preg_match('/<img/', $country4[1][2])) {
          $country_name[] = str_replace('&nbsp;', '', $country4[0][2]);
          $result .= "<tr><td>" . $country4[1][2] . "</td><td>" . $country2[4] . "</td><td>" . $country2[5] . "</tr>";
          }

          if (preg_match('/<img/', $country4[1][3])) {
          $country_name[] = str_replace('&nbsp;', '', $country4[0][3]);
          $result .= " <tr><td>" . $country4[1][3] . "</td><td>" . $country2[6] . "</td><td>" . $country2[7] . "</tr>";
          }
          if (preg_match('/<img/', $country4[1][4])) {
          $country_name[] = str_replace('&nbsp;', '', $country4[0][4]);
          $result .= " <tr><td>" . $country4[1][4] . "</td><td>" . $country2[8] . "</td><td>" . $country2[9] . "</tr>";
          }
          }
          $final_res = array(
          'res' => $result,
          'country2' => $country2,
          'country4' => $country4,
          'country_name' => $country_name
          );

          /*
          $aorank_ajax_country_data = json_encode($final_res);
          $url_search_id = $this->session->userdata('id');
          $ajax_country_data = array("country_data" => "$aorank_ajax_country_data");
          $this->aorankmodel->update_data($url_search_id, $ajax_country_data);


          echo $aorank_ajax_country_data; // ajax 12. .. */
    }

    public function social_count($url) {



        $url2 = $url;

        $data2 = "http://api.facebook.com/restserver.php?method=links.getStats&format=json&urls=" . $url2;
        $content1 = file_get_contents($data2);
        $array2 = json_decode($content1, true);


        $data3 = "http://www.linkedin.com/countserv/count/share?url=" . $url2 . "&format=json";

        $content2 = file_get_contents($data3);
        $array3 = json_decode($content2, true);

        $data4 = "http://urls.api.twitter.com/1/urls/count.json?url=" . $url2;

        $content3 = file_get_contents($data4);
        $array4 = json_decode($content3, true);


        $d1 = $array2[0]['share_count'];
        $d2 = $array3['count'];
        $d3 = $array4['count'];
        $total = $d1 . " | " . $d2 . " | " . $d3;

        $total = explode(" | ", $total);

        return $total;
    }

    public function showlinks() {
        
        $char = $this->input->post('char');
        if ($char == '') {
            $char = '';
        }

        $numrows = $this->aorankmodel->fetch_numrows($char);

        $rowsperpage = 10;
        $totalpages = ceil($numrows / $rowsperpage);
        if (($this->input->post('currentpage')) && is_numeric($this->input->post('currentpage'))) {
            // cast var as int
            $currentpage = (int) $this->input->post('currentpage');
        } else {
            // default page num
            $currentpage = 1;
        } // end if
// if current page is greater than total pages...
        if ($currentpage > $totalpages) {
            // set current page to last page
            $currentpage = $totalpages;
        } // end if
// if current page is less than first page...
        if ($currentpage < 1) {
            // set current page to first page
            $currentpage = 1;
        } // end if
// the offset of the list, based on current page 

        $offset = ($currentpage - 1) * $rowsperpage;
        $data = $this->aorankmodel->fetch_links($offset, $rowsperpage, $char);
        $range = 3;
        $linksdata['data_present'] = $numrows;
        $linksdata['char'] = $char;
        $linksdata['data'] = $data;
        $linksdata['range'] = $range;
        $linksdata['totalpages'] = $totalpages;
        $linksdata['currentpage'] = $currentpage;
        $this->load->view('urllinks', $linksdata);

        //$this->load->view('urllinks',$linksdata);
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function generate_pdf() {
        $page_url = $this->input->post('page_url');
        $filepath=explode("//","$page_url");
      
        $filename=$filepath[1];
        $filename = str_replace(array('/'), '_', $filename);
        
         
        $filename .=".pdf"; 
       
        $check_file =base_url()."pdf_collection/$filename";
        $result = $this->checkurl($check_file);
        if($result != 1){            
        $url_data_array = $this->aorankmodel->get_data($page_url);
        $url_data = $url_data_array[0];
        $db['get_url'] = $url_data['url'];   // 0 (url)  string form
        $title_json = $url_data['aorank_title'];    // 1 title
        $db['title'] = json_decode($title_json);    // 1 title
        $description_json = $url_data['aorank_description'];  // 2 description 
        $db['description'] = json_decode($description_json);   // 2 description 
        $meta_keywords_json = $url_data['aorank_meta_keywords']; // 3 meta keywords 
        $db['meta_keywords'] = json_decode($meta_keywords_json); // 3 meta keywords 
        $words_data_json = $url_data['aorank_content_length']; // 4 words   ...
        $db['words'] = json_decode($words_data_json);    // 4 words   ...
        $heading_json = $url_data['aorank_headings'];     // 5 heading
        $heading_data = json_decode($heading_json);       // 5 heading
        $db['heading'] = $heading_data[0];                   // 5 heading
        $db['heading_innertext'] = $heading_data[1];             // 5 heading  
        $focused_keyword_json = $url_data['aorank_focused_keywords'];      // 6 focused keyword
        $db['Keywords_cloud'] = json_decode($focused_keyword_json);      // 6 focused keyword
        $keyword_consistency_json = $url_data['aorank_keyword_consistency'];  //7 keyword consistency
        $db['keywords_consistency'] = json_decode($keyword_consistency_json); //7 keyword consistency
        $images_data_json = $url_data['aorank_images']; // 8 images
        $db['image'] = json_decode($images_data_json, true); // 8 images
        $db['get_text_ratio'] = $url_data['aorank_text_ratio']; // 9 text html ratio
        $db['index_page'] = $url_data['aorank_index_page_data']; // 10 page index .. 
        $google_preview_json = $url_data['aorank_google_preview']; //11 google preview .
        $db['google_preview'] = json_decode($google_preview_json); //11 google preview ..
        $blinks_data = $url_data['aorank_in_page_links'];  //   12 page links 
        $blinks_data_json = stripslashes($blinks_data);  //   12 page links 
        $db['blinks'] = json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $blinks_data_json), true); //   12 page links
        $db['GoogleBL'] = $url_data['aorank_page_backlinks'];   //13  page back links
        $db['redirect'] = $url_data['aorank_www_redirect'];   //14
        $db['url_rewrite'] = $url_data['aorank_url_rewrite'];   // 15
        $db['url_underscores'] = $url_data['aorank_url_underscore'];   // 16
        $db['url_char_count'] = $url_data['aorank_domain_char_count']; //17
        $db['favicon'] = $url_data['aorank_favicon']; //18 
        $db['checkurl'] = $url_data['aorank_check_404_page']; // 19
        $db['conversion_forms'] = $url_data['aorank_coversion_form']; // 20
        $page_size_json = $url_data['aorank_page_size'];
        $page_size_data = json_decode($page_size_json, true);
        $db['headers'] = $page_size_data[0];  //21
        $db['load_time'] = $page_size_data[1];  //21
        $language_data_json = $url_data['aorank_language']; // 22
        $language_data = json_decode($language_data_json, true);
        $db['language'] = $language_data[0];   // 22
        $db['lang'] = $language_data[1]; // 22
        $technology_data_json = $url_data['aorank_technology'];
        $technology_data = json_decode($technology_data_json, true);
        $db['tech'] = $technology_data[0]; //23
        $db['google_analytic'] = $technology_data[1]; //23
        $db['diff_analytic'] = $technology_data[2]; //23
        $speed_tips_json = $url_data['aorank_speed_tips'];  //24 
        $db['speed_tips'] = json_decode($speed_tips_json, true);  // 24
        $db['doctype'] = $url_data['aorank_doctype']; // 25
        $db['encoding'] = $url_data['aorank_encoding']; // 26
        $depricated_json = $url_data['aorank_deprecated_html']; // 27
        $db['dep'] = json_decode($depricated_json, true); // 27
        $social_count_json = $url_data['aorank_social_count']; //28
        $db['social_count'] = json_decode($social_count_json, true); //28
        $link_status_json = $url_data['aorank_link_status']; //29
        $db['link_status'] = json_decode($link_status_json, true); //29
        $ip_city_country = $url_data['aorank_ip_city_country'];
        $ip_city_country_data = explode("###", $ip_city_country);
        $no_of_ip = $ip_city_country_data[0];
        $city = $ip_city_country_data[1];
        $country = $ip_city_country_data[2];
        $db['page_ip'] = array(
            "no_of_ip" => "$no_of_ip",
            "city" => "$city",
            "Country" => "$country"
        );
// ajax data .....
        $who_is_info_json = $url_data['ajax_whois_fullinfo_json'];
        $whois_data = explode("###", $who_is_info_json);
        $db['time_info'] = $whois_data[0];
        $db['owner_info'] = $whois_data[1];
        $db['related_websites'] = $url_data['aorank_ajax_related_websites'];
        $db['screen_image'] = $url_data['aorank_ajax_screen_img'];
        $db['page_rank'] = $url_data['aornak_ajax_pagerank'];
        $db['alexa_rank'] = $url_data['aorank_ajax_alexa_rank'];
        $db['w3c_reply'] = $url_data['aorank_ajax_w3c_error_count'];
        $db['pagelink_icon_tics'] = $url_data['aorank_ajax_pagelink'];
        $robot_text_json = $url_data['aorank_ajax_robot_txt_json'];
        $robot_text_data = explode("###", "$robot_text_json");
        $robot_result = $robot_text_data[0];
        $count = $robot_text_data[1];
        $db['robot_text_data'] = array(
            'robot_result' => $robot_result,
            'count' => $count
        );
        $xml_data_json = $url_data['aorank_ajax_sitemap_xml'];
        $db['xml_data'] = json_decode($xml_data_json, true);
        $db['alexa_ranks'] = $url_data['aorank_ajax_alexa_rank'];
        $domain_country_json = $url_data['aorank_ajax_country'];
        $db['unique_title'] = $url_data['aorank_ajax_unique_title'];
        //$domain_country_data = json_decode($domain_country_json,true);
// i am from database ...

//load the view, pass the variable and do not show it but "save" the output into $html variable
        $html = $this->load->view('pdf_download', $db, true);

//this the the PDF filename that user will get to download
        
        $pdfFilePath = "pdf_collection/$filename";

//load mPDF library
        $this->load->library('m_pdf');
//actually, you can pass mPDF parameter on this load() function
        $pdf = $this->m_pdf->load();
//generate the PDF!
        $pdf->SetHTMLFooter('<div style="background-color:grey; color:white; text-align: right; font-family: Arial, Helvetica,
sans-serif; font-weight: bold;font-size: 10pt; margin-top:20px;">2015 © '.base_url().' All rights reserved</div>');
        $pdf->SetDisplayMode('fullpage');
        $pdf_style_path = "<?php echo base_url(); ?>css/mpdfstyleA4.css";
        $stylesheet ="form.searchform {
    display: none;
}
h3.st-sub-title {
    display: none;
}

div#recent_tests_button{
   visibility: hidden;
}
 
input#searchsubmit{
    visibility: hidden;
}
 ";
        
        $pdf->WriteHTML($stylesheet,1);
        $pdf->WriteHTML($html);
//offer it to user via browser download! (The PDF won't be saved on your server HDD)
        $pdf->Output($pdfFilePath, "F");
  }else{      
      $url_data_array = $this->aorankmodel->get_data($page_url);
      $url_data = $url_data_array[0];
      $db['get_url'] = $url_data['url']; 
      $db['screen_image'] = $url_data['aorank_ajax_screen_img'];
      
  }
        
        
         $file_path= $check_file;
      
       $receiver_email = $this->input->post('user_email');
       
       $this->Send_Mail($receiver_email,$filename,$db['screen_image'],$db['get_url'],$file_path);
       
    }

    public function Send_Mail($receiver_email,$filename,$screen_image,$page_url,$file_path) {
        $smtp_host = "smtp.mandrillapp.com";
        $sender_email = "neeraga@gmail.com";
        $user_password = "mFAJ9Lh31JiK_Q1vkbvi4g";        
        $username = "Aorank.com";
        $subject = "Download complete SEO Reports";
        
        
        $data['screen_image']=$screen_image;
        $data['page_url']=$page_url;
        $data['receiver_email']=$receiver_email;
        $data['file_path']=$file_path;
        
        
         $user_id= $this->aorankmodel->aorank_fetch_email($receiver_email);
         if($user_id == -1 ){ 
             $this->aorankmodel->insert_email($receiver_email,1);             
         }else{
            $this->aorankmodel->aorank_increment_email_counter($user_id);
         }
        
        
        
        

// loading library         
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('encrypt');

// Configure email library
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = $smtp_host;
        $config['smtp_port'] = 587;
        $config['smtp_user'] = $sender_email;
        $config['smtp_pass'] = $user_password;
        $config['mailtype'] = 'html';

// Load email library and passing configured values to email library
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");

// Sender email address
        $this->email->from('noreply@formget.com', $username);
// Receiver email address
        $this->email->to($receiver_email);
// Subject of email
        $this->email->subject($subject);
// Message in email 
        $message=$this->load->view('email',$data,TRUE);
        $this->email->message($message);

        if ($this->email->send()) {
            $data['message_display'] = "SEO Report Successfully Sent to <br><b>$receiver_email </b><br> Kindly check your Email ! ";
        } else {
            $data['message_display'] = '<p class="error_msg">Invalid Gmail Account or Password !</p>';
        }
        echo $data['message_display'];
    }

}

?>