<!DOCTYPE html>
<html>
		<head>
				<meta charset="utf-8">
				<meta http-equiv="X-UA-Compatible" content="IE=edge">
				<title>Profile Settings</title>
				<meta name="viewport" content="width=device-width, initial-scale=1">
				<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/global.css" />
				<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/fg-main-style.min.css" />
				<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/pagestyle.css" />
                                <script type="text/javascript" src="http://code.jquery.com/jquery-2.1.1.min.js"></script>

                                <script>
                                function checkemail(event){
                                     var emailadd = $('#emailadd').val();
                                  event.preventDefault();
                                     $('#loding').css('display','block');
                                         $.ajax({
                                             
                    type: 'POST',
                    url: '<?php echo base_url() . "tracker/forgotpassword1" ?>',
                  dataType: 'json',
                    data: {"emailaddress": emailadd},
                   success: function (data)
                  {
                    $('#loding').css('display','none');
        if(data == "notexist")
      {
   // alert('not exist');
    $('#loding').css('display','none');
     $('#emailnot').html('Email Adress Not Exists');
    $('#emailnot').css('display','block');
    }
  //  alert(data);
                    }, 
                    error: function (jqXHR, textStatus, errorThrown) {
                     //   alert('Error Message: ' + textStatus);
                     //   alert('HTTP Error: ' + errorThrown);
                           $('#loding').css('display','none');
                           
                         
                             $('#emailget').css('display','block');
                             $('#emailnot').css('display','none');
                             $('#hidbutton').css('display','inline');
                             
                //    location.href = "<?php echo base_url() . 'tracker/login/sent' ?>";
                    }
                });
                                   
                                }
                                </script>
                       
		</head>
		<body>
			<div id="" class="container-fluid" style="width:100%;max-width:900px;">
				
				<!-- Header Section -->
				<div class="row">
					<div class="main-header" style="position: relative;">  
						<div class="col-xs-3 col-sm-3 col-md-3">
							<a href="#" class="logo"><img src="<?php  echo base_url(); ?>images/logo.png" alt="Formget Fugo"></a>
						</div>
							<div class="col-xs-9 col-sm-9 col-md-9">
								<div>
									<div class="user-nav">
										<span class="wel-name"></span>
										<div id="user-img">
											<!--<img src="#" alt="Profile Picture">
											<span class="arrow-down"></span>-->
											<div id="fg-dropdown">
												<div class="fg-dropdown-inner">
													<ul>
														<li><a href="dashboard.html" class="first">Dashboard</a></li>
														<li><a href="#">Account Settings</a></li>
														<li><a href="#">Become Affiliate</a></li>
														<li><a href="#" class="last">Log Out</a></li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="fg-clear"></div>
					
							<div class="fg-clear"></div>
					</div>
				</div>

				<!-- Middle Section -->
				<div class="row  main-container blue-bg" style="padding-left:0px;">

					<div class="col-md-12 nopadding">
						<div id="right-section-wrapper">
							<div class="top-section-heading">
                                                            <h2>Password Settings</h2>
                                              	</div>
							<div class="clearfix"></div>
							<div class="fg-parent-box">
								<div class="fg-box first last">
									<p class="fg-box-header rl-pad">Enter Email Address</p>
									<div class="fg-inner-box rl-pad">
										<form action="<?php echo base_url()."tracker/forgotpassword1"; ?>" method="post">
											<div class="fg-row">
												<label class="block fg-label">Email Address</label>
                                                                                                <input id="emailadd" type="text" name="emailaddress" class="fg-input fg-fw password">
											</div>
                                                                                    <p id="emailnot" class="alertmessage"></p>
                                                                                    <p id="emailget" class="alertmessage2">Password Has Been Sent To Your Email Address Click Here To   <a href = "<?php echo base_url() . 'tracker/login' ?>">Login</a></p>
										<!--	<div class="fg-row">
												<label class="block fg-label">Re-Enter New Password</label>
												<input type="password" class="fg-input fg-fw password">
											</div>-->
											<div class="fg-row last">
                                                                                            <input type="submit" class="fg-btn green medium inline" value="Send Password" onclick="checkemail(event);">
											</div>
										</form>
									</div>
								</div>
							</div><!-- fg-parent-box End-->
                                                       <center> <div id="loding"> <img src="<?php echo base_url()."images/loding.GIF"; ?>"> </div></center>
                                                        <?php //echo base_url()."index.php/tracker/forgotpassword1"; ?>
						</div>
					
					</div>
				</div>

				<!-- Footer Section -->
				<div class="row">
					<div class="main-footer">
						<div class="link-footer">
							<ul>
								<li><a href="#">Home</a></li>
								<li><a href="#">Features</a></li>
								<li><a href="#">Faqs</a></li> 
								<li><a href="#">Blog</a></li>
								<li><a href="#">Affiliates</a></li>
								<li><a href="#">About Us</a></li>
								<li><a href="#">Terms &amp; Conditions and License</a></li>
								<li><a href="#">Privacy Policy</a></li>
								<li><a href="#">Contact Us</a></li>
							</ul>
						</div>
						<div class="copyright-footer">
							<p>2014 &copy; FormGet.com All rights reserved.</p>
						</div>
					</div>
				</div>

			</div><!-- Main wrapper closing -->
			<script src="js/plugin.js"></script>
			<script src="js/fg-script.js"></script>       
		</body>
</html>