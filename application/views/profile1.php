<?php
//code to show the value of userid and username which is passed when page is called
//echo $userid; 
//echo $username;
if ($this->session->userdata('username')) {
    $name = $this->session->userdata('username');
} else {
    header('location:');
    header('Location:' . base_url() . 'tracker/login');
}
?>
<?php
foreach ($uprofile->result() as $row) {
//echo $userid;
//echo $userid;
    //  echo $row->name;
    $name = $row->name;
    //  echo $row->password;
    // echo $row->emailid;
    // echo $row->country;
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Profile Settings</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="css/global.css">
        <link rel="stylesheet" type="text/css" href="css/fg-main-style.css">



        <link href="<?php echo base_url(); ?>css/global.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>css/fg-main-style.min.css" rel="stylesheet" type="text/css"/>


    </head>
    
    <body>
        <div id="main-wrapper" class="container-fluid" style="">

            <!-- Header Section -->
            <div class="row">
                <div class="main-header" style="position: relative;">  
                    <div class="col-xs-3 col-sm-3 col-md-3">
                        <a href="#" class="logo"><img src="<?php echo base_url(); ?>images/logo.png" alt="Formget Fugo"></a>
                    </div>
                    <div class="col-xs-9 col-sm-9 col-md-9">
                        <div>
                            <div class="user-nav">
                                <span class="wel-name">Welcome <?php echo $name; ?></span>
                                <div id="user-img">
                                        <!--<img src="#" alt="Profile Picture">
                                        <span class="arrow-down"></span>-->
                                    <div id="fg-dropdown">
                                        <div class="fg-dropdown-inner">
                                            <ul>
                                                <li><a href="dashboard.html" class="first">Dashboard</a></li>
                                                <li><a href="#">Account Settings</a></li>
                                                <li><a href="#">Become Affiliate</a></li>
                                                <li><a href="#" class="last">Log Out</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="fg-clear"></div>
                    <ul class="main-tab" style="position: absolute;right: 0px;bottom: -1px;">
                        <li><a href="<?php echo base_url() . 'tracker/dashboard' ?>">Dashboard</a></li>
                        <!--<li><a href="single-extension-page.html">Active Extensions</a></li>-->
                        <li id="nomargin"><a href="<?php echo base_url() . 'tracker/logout' ?>">logout</a></li>
                    </ul>
                    <div class="fg-clear"></div>
                </div>
            </div>

            <!-- Middle Section -->
            <div class="row  main-container blue-bg" style="padding-left:0px;">
                
                <div class="col-md-2 nopadding" >
                    <div id="sidebar">
                        </div>
                    </div>

                <div class="col-md-10 nopadding">
                   <div id="right-section-wrapper">
                        <div id='profilcontainer'>
                        <div class="top-section-heading">
                            <h2>Profile Settings</h2>
                        </div>
                        <div class="clearfix"></div>
                        <div class="fg-parent-box">
                            <div class="fg-box first last">
                                <p class="fg-box-header rl-pad">Profile Settings</p>
                                <div class="fg-inner-box rl-pad">
                                    <form action="<?php echo base_url() . 'tracker/accountupdate/' . $userid . '/' . $name?>" method="post">
                                        <div class="fg-row">
                                            <label class="block fg-label">Full Name</label>
                                            <input type="text" class="fg-input fg-fw text" name="name" value="<?php echo $row->name; ?>">
                                        </div>
                                        <!--		<div class="fg-row">
                                                        <label class="block fg-label">UserName</label>
                                                        <input type="text" class="fg-input fg-fw text" name="uname" value="<?php // echo $row->username;    ?>">
                                                </div>-->
                                        <div class="fg-row">
                                            <label class="block fg-label">password</label>
                                            <input type="password" class="fg-input fg-fw text" name="upass" value="<?php echo $row->password; ?>">
                                        </div>
                                        <div class="fg-row">
                                            <label class="block fg-label">Your Email</label>
                                            <input type="text" class="fg-input fg-fw text disable" disabled name="emailid" value="<?php echo $row->emailid; ?>">
                                        </div>
                                        <div class="fg-row">
                                            <label class="block fg-label">Country</label>
                                            <input type="text" class="fg-input fg-fw text" name="country" value="<?php echo $row->country; ?>">
                                        </div>

                                        <div class="fg-row last">
                                            <input type="submit" class="fg-btn green medium inline" value="Save Settings">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div><!-- fg-parent-box End-->
                        <div>
                    </div><!-- right-section-wrapper End -->
                </div>
            </div>
</div>
                </div>
            <!-- Footer Section -->
            <div class="row">
                <div class="main-footer">
                    <div class="link-footer">
                        <ul>
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Features</a></li>
                            <li><a href="#">Faqs</a></li> 
                            <li><a href="#">Blog</a></li>
                            <li><a href="#">Affiliates</a></li>
                            <li><a href="#">About Us</a></li>
                            <li><a href="#">Terms &amp; Conditions and License</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="#">Contact Us</a></li>
                        </ul>
                    </div>
                    <div class="copyright-footer">
                        <p>2014 &copy; FormGet.com All rights reserved.</p>
                    </div>
                </div>
            </div>
 </div><!-- Main wrapper closing -->
     
        <script src="js/plugin.js"></script>
        <script src="js/fg-script.js"></script>       
    </body>
</html>